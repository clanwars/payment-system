## [5.1.3](https://gitlab.com/clanwars/payment-system/compare/5.1.2...5.1.3) (2024-10-12)


### Bug Fixes

* **dependencies:** update pcsc libs and identitymodel lib ([0d1a614](https://gitlab.com/clanwars/payment-system/commit/0d1a61400a1c777607212e39ef753826d3b39526))

## [5.1.2](https://gitlab.com/clanwars/payment-system/compare/5.1.1...5.1.2) (2024-10-12)


### Bug Fixes

* **container:** don't copy with chown ([7e020f3](https://gitlab.com/clanwars/payment-system/commit/7e020f3c6f2717d7caa0cbf37e03de24b6bf83b8)), closes [#173](https://gitlab.com/clanwars/payment-system/issues/173)

## [5.1.1](https://gitlab.com/clanwars/payment-system/compare/5.1.0...5.1.1) (2024-08-07)


### Bug Fixes

* **deps:** update dotnet monorepo ([6004e1e](https://gitlab.com/clanwars/payment-system/commit/6004e1e878d2a2bdf3ed2b834fb07ddd25a83c13))

# [5.1.0](https://gitlab.com/clanwars/payment-system/compare/5.0.1...5.1.0) (2024-05-30)


### Features

* **dotnet:** upgrade libs and project to dotnet8 ([70c0ff8](https://gitlab.com/clanwars/payment-system/commit/70c0ff8e8ebd68f79db8cedc2b40618c8ae6f4d6))

## [5.0.1](https://gitlab.com/clanwars/payment-system/compare/5.0.0...5.0.1) (2023-10-19)


### Bug Fixes

* **admin-ui:** fixes transactions list where username was not displayed anymore ([b6fb854](https://gitlab.com/clanwars/payment-system/commit/b6fb854bac8ec74989793cb7f10daafb51f1b267))
* **admin-ui:** fixes transactions list where username was not displayed anymore ([e13e606](https://gitlab.com/clanwars/payment-system/commit/e13e6060f0d533c0915d8281f029fceed2ca1754))

# [5.0.0](https://gitlab.com/clanwars/payment-system/compare/4.3.2...5.0.0) (2023-10-18)


### Bug Fixes

* **deps:** updates project to dotnet 7 ([4de73cc](https://gitlab.com/clanwars/payment-system/commit/4de73cc63f5659f047b5a319a48114993d777c21))
* **IdentityModel:** downgrades System.IdentityModel.Tokens.Jwt to latest working version ([4d999ea](https://gitlab.com/clanwars/payment-system/commit/4d999ea622c5c72f6439a45679f839bf34b967da))


### BREAKING CHANGES

* **deps:** dotnet 7 required for NFC client now

## [4.3.2](https://gitlab.com/clanwars/payment-system/compare/4.3.1...4.3.2) (2022-10-08)


### Bug Fixes

* **discount:** fixes a bug with non recalculation of discount of current cart entries ([66d860a](https://gitlab.com/clanwars/payment-system/commit/66d860a33ef6686c44efd305759ad0e57761bdb7))

## [4.3.1](https://gitlab.com/clanwars/payment-system/compare/4.3.0...4.3.1) (2022-10-07)


### Bug Fixes

* **cashdesk:** fixes discount ([5c39340](https://gitlab.com/clanwars/payment-system/commit/5c39340c45d4d5cb901e3788a8d7f7cc44116350))

# [4.3.0](https://gitlab.com/clanwars/payment-system/compare/4.2.0...4.3.0) (2022-09-27)


### Features

* **lib:** publish lib as nuget package ([2d3c84e](https://gitlab.com/clanwars/payment-system/commit/2d3c84e96bec29bfb19dff3c34fbd93747522f01))

# [4.2.0](https://gitlab.com/clanwars/payment-system/compare/4.1.2...4.2.0) (2022-09-27)


### Features

* **api:** adds point of sale endpoint serving all current cards ([e2c94ab](https://gitlab.com/clanwars/payment-system/commit/e2c94ab891a56edabfc7d637152aec3d4cd87105))

## [4.1.2](https://gitlab.com/clanwars/payment-system/compare/4.1.1...4.1.2) (2022-09-21)


### Bug Fixes

* **accounts:** splits username column into email, first name and last name ([87b67df](https://gitlab.com/clanwars/payment-system/commit/87b67dfcf9bd1af12c18cde401603bc39d8bca65)), closes [#165](https://gitlab.com/clanwars/payment-system/issues/165)

## [4.1.1](https://gitlab.com/clanwars/payment-system/compare/4.1.0...4.1.1) (2022-09-16)


### Bug Fixes

* **deps:** update dependency @microsoft/signalr to v6 ([147942e](https://gitlab.com/clanwars/payment-system/commit/147942e218d15c4f9938389836bfcda98c909dcc))

# [4.1.0](https://gitlab.com/clanwars/payment-system/compare/4.0.7...4.1.0) (2022-09-13)


### Features

* **db:** add new initialmigration ([8837667](https://gitlab.com/clanwars/payment-system/commit/88376677f526c804a09a74008405f5b332b53a7f))

## [4.0.7](https://gitlab.com/clanwars/payment-system/compare/4.0.6...4.0.7) (2022-09-09)


### Bug Fixes

* **frontends:** fixes redirects after login/logout ([28ef29c](https://gitlab.com/clanwars/payment-system/commit/28ef29ce1a7dc61bf4d8dfadf806d385fe98153f))

## [4.0.6](https://gitlab.com/clanwars/payment-system/compare/4.0.5...4.0.6) (2022-09-09)


### Bug Fixes

* **oauth:** fetch dynamic OAuth2 config from backend instead of built it into frontend container ([0043f99](https://gitlab.com/clanwars/payment-system/commit/0043f998a1d4c07c55bd164cf261564390a16016)), closes [#163](https://gitlab.com/clanwars/payment-system/issues/163)

## [4.0.5](https://gitlab.com/clanwars/payment-system/compare/4.0.4...4.0.5) (2022-09-06)


### Bug Fixes

* **image:** add dotnet environment env ([a6447f8](https://gitlab.com/clanwars/payment-system/commit/a6447f8a2caf7468549c69dcadac7ac79e4d40b7))

## [4.0.4](https://gitlab.com/clanwars/payment-system/compare/4.0.3...4.0.4) (2022-09-04)


### Bug Fixes

* **frontends:** configure OAuth2 IDP via environment.ts file ([d43e288](https://gitlab.com/clanwars/payment-system/commit/d43e2888492302e953c4ba5d1492fa52e22c5575))

## [4.0.3](https://gitlab.com/clanwars/payment-system/compare/4.0.2...4.0.3) (2022-09-04)


### Bug Fixes

* **frontend-spas:** adds localized redirects to corresponding SPAs in production ([6f5c055](https://gitlab.com/clanwars/payment-system/commit/6f5c055fbd9c1ca7a1814679abf2ff5696f06b87)), closes [#161](https://gitlab.com/clanwars/payment-system/issues/161)

## [4.0.2](https://gitlab.com/clanwars/payment-system/compare/4.0.1...4.0.2) (2022-09-03)


### Bug Fixes

* **image:** copy correct files ([e9f9210](https://gitlab.com/clanwars/payment-system/commit/e9f92109fca089277e6f8da0a6ed396094941678))

## [4.0.1](https://gitlab.com/clanwars/payment-system/compare/4.0.0...4.0.1) (2022-09-03)


### Bug Fixes

* **image:** copy correct build files ([50dd74a](https://gitlab.com/clanwars/payment-system/commit/50dd74a9f0008cf6998376c18bc81edc1aa08fe6))

# [4.0.0](https://gitlab.com/clanwars/payment-system/compare/3.0.3...4.0.0) (2022-09-02)


### Features

* **auth:** use keycloak instead of own user authentication ([10d6cf4](https://gitlab.com/clanwars/payment-system/commit/10d6cf4aa88c05c42f2ca0b42ab116d2c460e7f4)), closes [#142](https://gitlab.com/clanwars/payment-system/issues/142)
* **ci:** add semantic release job ([baabceb](https://gitlab.com/clanwars/payment-system/commit/baabceb500ba3bdfcd3358ea1789c00d95d2ec9f))


### BREAKING CHANGES

* **auth:** use OAuth2 (tested with keycloak) for authentication

# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [3.0.2](https://gitlab.com/homescreen.rocks/payment-system/compare/3.0.1...3.0.2) (2021-10-06)


### Bug Fixes

* **cashdesk:** fixes lockscreen unlock which was not possible if a pin was defined ([1f2a95d](https://gitlab.com/homescreen.rocks/payment-system/commit/1f2a95d1ceffa775dbb2fafa28df165db84f8cff))

### [3.0.1](https://gitlab.com/homescreen.rocks/payment-system/compare/3.0.0...3.0.1) (2021-10-06)


### Bug Fixes

* **wizard:** improve layout and configuration page ([f453d3f](https://gitlab.com/homescreen.rocks/payment-system/commit/f453d3fb93093b44e65b1955be8eb7817325f4ee)), closes [#138](https://gitlab.com/homescreen.rocks/payment-system/issues/138)

## [3.0.0](https://gitlab.com/homescreen.rocks/payment-system/compare/2.2.1...3.0.0) (2021-10-04)


### Features

* **cashdesk:** use browsers NavigatorOnLine to check for network connection ([75022a8](https://gitlab.com/homescreen.rocks/payment-system/commit/75022a824c87fcbec59233278ca80618a6527d52)), closes [#133](https://gitlab.com/homescreen.rocks/payment-system/issues/133)
* **db:** upgrade mariadb to 10.6 ([b86fdf0](https://gitlab.com/homescreen.rocks/payment-system/commit/b86fdf0c04b85f6e052692b73e30044e04e99ed9))
* **i18n:** adds more german translations to cashdesk ([7422e1a](https://gitlab.com/homescreen.rocks/payment-system/commit/7422e1ad07652eae5e6888514f1ba49d439584c9)), closes [#125](https://gitlab.com/homescreen.rocks/payment-system/issues/125)
* **ui:** i18n ([407ca5c](https://gitlab.com/homescreen.rocks/payment-system/commit/407ca5cabb6a4218054f2b10a943a7f1729e9446)), closes [#125](https://gitlab.com/homescreen.rocks/payment-system/issues/125)


### Bug Fixes

* **adminui:** make it possible to delete cardId assigned to a user ([8a0378d](https://gitlab.com/homescreen.rocks/payment-system/commit/8a0378dedc8c30267df6ecbc7aaa43861b57b649)), closes [#150](https://gitlab.com/homescreen.rocks/payment-system/issues/150)
* **deps:** pin dependency zone.js to 0.11.4 ([fdd274d](https://gitlab.com/homescreen.rocks/payment-system/commit/fdd274d37048c40acbd95f502a9ddba699fd7580))
* **deps:** update dependency ngx-image-cropper to v4 ([f36bf85](https://gitlab.com/homescreen.rocks/payment-system/commit/f36bf8585f38c209ffa39ffd17fd96588a85017f))
* **deps:** update dependency rxjs to v7 ([ea12c5b](https://gitlab.com/homescreen.rocks/payment-system/commit/ea12c5b6e6eafa44d2443264a82a099814dbeddc))
* **ui:** migrated to @danielmoncada/angular-datetime-picker as old datetimepicker is not maintained ([bfd03f4](https://gitlab.com/homescreen.rocks/payment-system/commit/bfd03f42d5d36cb203b527305fb31f702cb931eb))
* **ui:** several small fixes ([949668d](https://gitlab.com/homescreen.rocks/payment-system/commit/949668d52a2dd6f33c0ac51b64b9821091198e05))

### [2.2.1](https://gitlab.com/homescreen.rocks/payment-system/compare/2.2.0...2.2.1) (2021-02-27)


### Features

* **cashdesk:** make use of the configured PoS ID instead of using a hardcoded one ([0c37ea7](https://gitlab.com/homescreen.rocks/payment-system/commit/0c37ea77a1941729af980d7bbdfb7b016a224411)), closes [#148](https://gitlab.com/homescreen.rocks/payment-system/issues/148)


### Bug Fixes

* **cashdesk:** checkout now throws a large error when transaction could not be completed ([a53173e](https://gitlab.com/homescreen.rocks/payment-system/commit/a53173e9d02e57ce0a0af558c7fe165848b9d76a))
* **sieve:** include fixed dll to filter correctly ([3dacd9a](https://gitlab.com/homescreen.rocks/payment-system/commit/3dacd9a72dbd08246d84a814d6ca005e9782df5e))

## [2.2.0](https://gitlab.com/homescreen.rocks/payment-system/compare/2.1.0...2.2.0) (2021-02-08)


### Features

* **deployment:** adds /health check route ([208014e](https://gitlab.com/homescreen.rocks/payment-system/commit/208014e43b24689d142eadc5addd440073cbc451)), closes [#145](https://gitlab.com/homescreen.rocks/payment-system/issues/145)

## [2.1.0](https://gitlab.com/homescreen.rocks/payment-system/compare/2.0.4...2.1.0) (2021-02-07)


### Bug Fixes

* **adminui:** pos: go to overview after creating a new PoS ([7afa03b](https://gitlab.com/homescreen.rocks/payment-system/commit/7afa03bd197fe0c2066a1883cf43ef25e4231822)), closes [#136](https://gitlab.com/homescreen.rocks/payment-system/issues/136)
* **build:** fixes missing frontend package ng-packagr ([def0f37](https://gitlab.com/homescreen.rocks/payment-system/commit/def0f379e82dab0ea5092aeae0a384f0068d4824))

### [2.0.4](https://gitlab.com/homescreen.rocks/payment-system/compare/2.0.3...2.0.4) (2020-10-09)


### Bug Fixes

* **admin:** fixes an error in the user registration wizard ([997eaa1](https://gitlab.com/homescreen.rocks/payment-system/commit/997eaa1aee989ba1f5951f76f1653c75c8dde718))

### [2.0.3](https://gitlab.com/homescreen.rocks/payment-system/compare/2.0.2...2.0.3) (2020-10-06)


### Bug Fixes

* **kestrel:** removes integrated ssl termination ([2508edd](https://gitlab.com/homescreen.rocks/payment-system/commit/2508eddd7d2119e8d37d5b84599065f50ed72cfd)), closes [#132](https://gitlab.com/homescreen.rocks/payment-system/issues/132)

### [2.0.2](https://gitlab.com/homescreen.rocks/payment-system/compare/2.0.1...2.0.2) (2020-09-19)


### Bug Fixes

* **product-types:** do not show locked product types on cashdesk ([6f7ca50](https://gitlab.com/homescreen.rocks/payment-system/commit/6f7ca5080009e9143823c2b19fa313b4b50d9632))

### [2.0.1](https://gitlab.com/homescreen.rocks/payment-system/compare/2.0.0...2.0.1) (2020-09-19)


### Bug Fixes

* **kestrel:** do not use httpsredirection for usage with external reverse proxy with ssl termination ([9ffb4d6](https://gitlab.com/homescreen.rocks/payment-system/commit/9ffb4d6ba49e2505f81ca675fcc1ff69f87337b3))

## [2.0.0](https://gitlab.com/homescreen.rocks/payment-system/compare/2.0.0-beta.1...2.0.0) (2020-09-17)


### Features

* **backend:** adds validation localization of error messages ([6f0ba9d](https://gitlab.com/homescreen.rocks/payment-system/commit/6f0ba9df2ad7c39efb239b74cc485f8791a7981c)), closes [#129](https://gitlab.com/homescreen.rocks/payment-system/issues/129)
* **monitoring:** improve monitoring page with grouped product informaton ([3721eaf](https://gitlab.com/homescreen.rocks/payment-system/commit/3721eafabcac7697fd6c3c963d2d4aa479a537ac))
* **posselection:** add configuration to select pos for wizard ([4ddb026](https://gitlab.com/homescreen.rocks/payment-system/commit/4ddb026ffa3ff6b759d33c2c7e1b077ff7c94529))
* **toasts:** adds notifications for all backend activities ([5dcb842](https://gitlab.com/homescreen.rocks/payment-system/commit/5dcb842b03811da0472f0dcb487cf76a9c12d0f0)), closes [#105](https://gitlab.com/homescreen.rocks/payment-system/issues/105)


### Bug Fixes

* **backend:** adds neutral cultures (de,en) to supported cultures ([d126a4a](https://gitlab.com/homescreen.rocks/payment-system/commit/d126a4a4d6832a4ec8020db78e8b97ff6ba01a22))
* **deps:** update angular monorepo to v10.0.3 ([934ffd2](https://gitlab.com/homescreen.rocks/payment-system/commit/934ffd2390cba3e48a7b23ce8a46bd733b3db5e8))
* **deps:** update dependency @microsoft/signalr to v3.1.7 ([9200a40](https://gitlab.com/homescreen.rocks/payment-system/commit/9200a402c22aef7defe91e6a0c83739b596a6c10))
* **deps:** update dependency @ng-bootstrap/ng-bootstrap to v7 ([f3e78fe](https://gitlab.com/homescreen.rocks/payment-system/commit/f3e78fe1eba2d7059f57f7ad1c8da0c066f08fb5))
* **deps:** update dependency bootstrap to v4.5.2 ([d6d4956](https://gitlab.com/homescreen.rocks/payment-system/commit/d6d495630c55208880d99bae9066a6763d5acdf6))
* **deps:** update dependency bootswatch to v4.5.2 ([2dd1c11](https://gitlab.com/homescreen.rocks/payment-system/commit/2dd1c1178aed8a321ab8eed3e0bd3235f2d934aa))
* **deps:** update dependency ngx-image-cropper to v3.2.1 ([5a9a1df](https://gitlab.com/homescreen.rocks/payment-system/commit/5a9a1df66b1d5e31cab30539616dee14784ce92d))
* **deps:** update dependency rxjs to v6.6.2 ([c2bfcea](https://gitlab.com/homescreen.rocks/payment-system/commit/c2bfcea96b9b4b3de967bae9fe94e96072ccee42))
* **frontend:** payment-lib update to angular 10 compatibility ([3f46630](https://gitlab.com/homescreen.rocks/payment-system/commit/3f46630ad7969c4655f3feb12e39621059c99ae2))
* **frontend:** reverst using const enums ([9ce4ad7](https://gitlab.com/homescreen.rocks/payment-system/commit/9ce4ad71917bb12a0be4960e089efedfd2411796))
* **pos:** updating pos to an existing pos name does throw an validation error now instead of 500er ([00da1af](https://gitlab.com/homescreen.rocks/payment-system/commit/00da1af39579878913c6b70a1e76df4fcef91075)), closes [#128](https://gitlab.com/homescreen.rocks/payment-system/issues/128)

## [2.0.0-beta.1](https://gitlab.com/homescreen.rocks/payment-system/compare/2.0.0-beta.0...2.0.0-beta.1) (2020-03-29)

## [2.0.0-beta.0](https://gitlab.com/homescreen.rocks/payment-system/compare/1.0.5...2.0.0-beta.0) (2020-03-29)


### Features

* **i18n:** adds internationalization for cashdesk project ([9a2edd5](https://gitlab.com/homescreen.rocks/payment-system/commit/9a2edd59bece8be30badca6753098c00fa6383aa)), closes [#120](https://gitlab.com/homescreen.rocks/payment-system/issues/120)
* **i18n:** adds some more transalations for cashdesk ([b1ccea3](https://gitlab.com/homescreen.rocks/payment-system/commit/b1ccea396aaa6fcee433345908c301d414607eb9)), closes [#125](https://gitlab.com/homescreen.rocks/payment-system/issues/125)
* **product-selector:** use new product type selector instead of touch swiping ([ef21380](https://gitlab.com/homescreen.rocks/payment-system/commit/ef213804f6fb140d3d288c05f6c497e449d3696d)), closes [#118](https://gitlab.com/homescreen.rocks/payment-system/issues/118)
* **product-type:** adds new property `image` to product type ([2e89e6c](https://gitlab.com/homescreen.rocks/payment-system/commit/2e89e6c4fc6ddf2947e4c744cac10545291b86cb)), closes [#118](https://gitlab.com/homescreen.rocks/payment-system/issues/118) [#118](https://gitlab.com/homescreen.rocks/payment-system/issues/118)
* **webserver:** adds redirections to localized angular apps based on users browser ([22d2ec0](https://gitlab.com/homescreen.rocks/payment-system/commit/22d2ec0c3223ae9d64a325fd78c7e709623091d4))


### Bug Fixes

* **admin:** fixes nebular input field states ([65ee159](https://gitlab.com/homescreen.rocks/payment-system/commit/65ee1593914bd251367064e039de6f70cbd3e388))
* **angular:** removed unused entryModules settings as they are not needed in angular 9 ([9a4a1f9](https://gitlab.com/homescreen.rocks/payment-system/commit/9a4a1f9449bb10609e1500a998b455225b4d32a9))
* **automapper:** adds missing mapping profiles ([4534cc9](https://gitlab.com/homescreen.rocks/payment-system/commit/4534cc954d7dcb24d7eac43d57a8033466b9d71a)), closes [#54](https://gitlab.com/homescreen.rocks/payment-system/issues/54)
* **backend:** fixes an issue with filtering points of sale from the previous commit ([c6de05d](https://gitlab.com/homescreen.rocks/payment-system/commit/c6de05d44d818529f613453b87e59e40c105ec33))
* **build:** adds angular/localize dependency ([219722b](https://gitlab.com/homescreen.rocks/payment-system/commit/219722b218aaa0058e067c8b0ae14437a50293b7))
* **customer-panel:** fixes clickable panel if no card is present ([e697f99](https://gitlab.com/homescreen.rocks/payment-system/commit/e697f992e32400b869aa125fc0b8b6a4d70eb874))
* **customer-panel:** fixes grouping in transaction history ([9a4fe36](https://gitlab.com/homescreen.rocks/payment-system/commit/9a4fe36835a6dc24e81fe8d7d3b5e080b607c385)), closes [#123](https://gitlab.com/homescreen.rocks/payment-system/issues/123)
* **customerpane:** fixes a pipe which was blocking the customer panel from showing up ([7d1acbe](https://gitlab.com/homescreen.rocks/payment-system/commit/7d1acbee6ee81dce932422e269a99e6ad30ee019)), closes [#121](https://gitlab.com/homescreen.rocks/payment-system/issues/121)
* **db:** adds an on-delete-restrict attribute to PoS-Transactions relationship ([f7be5a1](https://gitlab.com/homescreen.rocks/payment-system/commit/f7be5a1a639d7e3d43ee162d4cdd730a01fa964a)), closes [#124](https://gitlab.com/homescreen.rocks/payment-system/issues/124)
* **deps:** update angular monorepo to v9.0.1 ([6342534](https://gitlab.com/homescreen.rocks/payment-system/commit/6342534f3cc292c206395b2a2b6f991dd6bb091f))
* **deps:** update dependency @fortawesome/angular-fontawesome to v0.6.0 ([117bd8d](https://gitlab.com/homescreen.rocks/payment-system/commit/117bd8da49f084148023112c8a70d933b50c53e6))
* **deps:** update dependency @ng-bootstrap/ng-bootstrap to v5.2.2 ([f4da4b0](https://gitlab.com/homescreen.rocks/payment-system/commit/f4da4b07fa76f4df0f0e6c81affb6a1ff5ed8805))
* **deps:** update font awesome ([eafa7da](https://gitlab.com/homescreen.rocks/payment-system/commit/eafa7da04863ddc4aa7a90f8da5ffe40c7581256))
* **frontend:** removed unused import ([48fb90d](https://gitlab.com/homescreen.rocks/payment-system/commit/48fb90d698244f6b96a237c1ed9601c7e520b957))
* **hammerjs:** removed unused hammerjs config service ([d57cbe4](https://gitlab.com/homescreen.rocks/payment-system/commit/d57cbe4f2fb2a4674fc820ca856c2b57a3634bb0))
* **product-selector:** fixes discount modal on long/touch press onto product ([bd952a8](https://gitlab.com/homescreen.rocks/payment-system/commit/bd952a8f3680622c0d550816ed8b75893bc8295a)), closes [#119](https://gitlab.com/homescreen.rocks/payment-system/issues/119)
* **product-types:** make pt selector as wide as product cards ([6104487](https://gitlab.com/homescreen.rocks/payment-system/commit/61044872cf36e6d8a0460aa9aaaa1537ff124f37))
* **time-ago-texts:** refactored the time ago texts with a new internationalizable component ([c4f9b81](https://gitlab.com/homescreen.rocks/payment-system/commit/c4f9b8170e7b8e7a9316b82f9f898950188fc390)), closes [#122](https://gitlab.com/homescreen.rocks/payment-system/issues/122)

### [1.0.5](https://gitlab.com/homescreen.rocks/payment-system/compare/1.0.4...1.0.5) (2019-11-02)


### Bug Fixes

* **REST:** fixes issue with base paths for frontends ([c2b4be9](https://gitlab.com/homescreen.rocks/payment-system/commit/c2b4be9))

### [1.0.4](https://gitlab.com/homescreen.rocks/payment-system/compare/1.0.3...1.0.4) (2019-11-02)


### Bug Fixes

* **cashdesk:** fixes issue with zero prices in Cart ([6345793](https://gitlab.com/homescreen.rocks/payment-system/commit/6345793))

### [1.0.3](https://gitlab.com/homescreen.rocks/payment-system/compare/1.0.2...1.0.3) (2019-11-02)


### Bug Fixes

* **rest:** refactores images to URLs instead of full payload on every request ([93ebd0f](https://gitlab.com/homescreen.rocks/payment-system/commit/93ebd0f))

### [1.0.2](https://gitlab.com/homescreen.rocks/payment-system/compare/1.0.1...1.0.2) (2019-11-01)


### Bug Fixes

* **Cashdesk:** show description of no product image exists ([d6cc585](https://gitlab.com/homescreen.rocks/payment-system/commit/d6cc58522ee420398d8a51b71767f00e0e20f247))

### [1.0.1](https://gitlab.com/homescreen.rocks/payment-system/compare/1.0.0...1.0.1) (2019-11-01)


### Bug Fixes

* **Cashdesk:** fix initial loading of all products ([35cd047](https://gitlab.com/homescreen.rocks/payment-system/commit/35cd0476495de9b826033335a6ae44447a6a81f6))

## [1.0.0](https://gitlab.com/homescreen.rocks/payment-system/compare/1.0.0-rc.5...1.0.0) (2019-10-31)


### Bug Fixes

* **TouchEvents:** Fixes scrolling and swipe events for safari and chrome ([b4e1fac](https://gitlab.com/homescreen.rocks/payment-system/commit/b4e1facfec71109392f02f081bb44356f5e3bfe5)), closes [#80](https://gitlab.com/homescreen.rocks/payment-system/issues/80)

## [1.0.0-rc.5](https://gitlab.com/homescreen.rocks/payment-system/compare/1.0.0-rc.4...1.0.0-rc.5) (2019-10-31)


### Features

* **cashdesk:** adds warnings if NFC client is not ready ([9d8f3a9](https://gitlab.com/homescreen.rocks/payment-system/commit/9d8f3a92b0f54bec99ecc9c3d26114104f7c6c6e)), closes [#97](https://gitlab.com/homescreen.rocks/payment-system/issues/97)
* **REST:** adds lock routes for products and product types ([849f513](https://gitlab.com/homescreen.rocks/payment-system/commit/849f513c0e7b9d7907744a05e3443a539f72e98f))
* **wizard:** polished the wizard stepper ([8aee933](https://gitlab.com/homescreen.rocks/payment-system/commit/8aee9332399f2e1d29e6e10c4aaa08f00a87d5a6)), closes [#112](https://gitlab.com/homescreen.rocks/payment-system/issues/112)


### Bug Fixes

* **AccountList:** navigate to account list after generating new user ([0c2acab](https://gitlab.com/homescreen.rocks/payment-system/commit/0c2acabab9b7900dccfda904442a2c066a7bf79e)), closes [#116](https://gitlab.com/homescreen.rocks/payment-system/issues/116)
* **adminui:** fixes AOT type mismatch error ([855f50e](https://gitlab.com/homescreen.rocks/payment-system/commit/855f50efcc5a85094a498ce48a26b288869eb8bd)), closes [#83](https://gitlab.com/homescreen.rocks/payment-system/issues/83)
* **cashdesk:** reload cardInfo after cancelling a transaction ([bb90785](https://gitlab.com/homescreen.rocks/payment-system/commit/bb90785d3543037f0c4a2e21e30b7f38b89531bd)), closes [#111](https://gitlab.com/homescreen.rocks/payment-system/issues/111)
* **cashdesk:** reload products on every unlock ([8a201ee](https://gitlab.com/homescreen.rocks/payment-system/commit/8a201eedb25b71e8e2c7ce4641d3d8b36d8dd0a1)), closes [#114](https://gitlab.com/homescreen.rocks/payment-system/issues/114)
* **transactionresponses:** fixes AOT warnings ([88cae6f](https://gitlab.com/homescreen.rocks/payment-system/commit/88cae6f2864eb08340818e032119ca142ef0e2ba))

## [1.0.0-rc.4](https://gitlab.com/homescreen.rocks/payment-system/compare/1.0.0-rc.3...1.0.0-rc.4) (2019-10-18)


### Bug Fixes

* **cashdesk:** adds more color to checkout button to get more visual focus ([3f79064](https://gitlab.com/homescreen.rocks/payment-system/commit/3f79064)), closes [#106](https://gitlab.com/homescreen.rocks/payment-system/issues/106)
* **cashdesk:** fixes charging sidebar ([fca13e0](https://gitlab.com/homescreen.rocks/payment-system/commit/fca13e0)), closes [#77](https://gitlab.com/homescreen.rocks/payment-system/issues/77)
* **cashdesk:** fixes sorting of products by `order` property ([fa45b50](https://gitlab.com/homescreen.rocks/payment-system/commit/fa45b50)), closes [#109](https://gitlab.com/homescreen.rocks/payment-system/issues/109)
* **Cashdesk:** chown member discount on products ([6e19404](https://gitlab.com/homescreen.rocks/payment-system/commit/6e19404))
* **Cashdesk:** update styling of card ([d1fb028](https://gitlab.com/homescreen.rocks/payment-system/commit/d1fb028)), closes [#107](https://gitlab.com/homescreen.rocks/payment-system/issues/107)
* **Cashdesk:** updates discount logic in products and cart ([cab4904](https://gitlab.com/homescreen.rocks/payment-system/commit/cab4904)), closes [#103](https://gitlab.com/homescreen.rocks/payment-system/issues/103)
* **fix:** fix ([1a1127f](https://gitlab.com/homescreen.rocks/payment-system/commit/1a1127f))
* **webserver:** fixes delivery of .webmanifest files by Kestrel ([5179d16](https://gitlab.com/homescreen.rocks/payment-system/commit/5179d16)), closes [#110](https://gitlab.com/homescreen.rocks/payment-system/issues/110)
* **wizard:** refactors card assignment step ([22d7dea](https://gitlab.com/homescreen.rocks/payment-system/commit/22d7dea)), closes [#101](https://gitlab.com/homescreen.rocks/payment-system/issues/101)
* **wizard:** some minor code improvements in PoS handling in Wizard ([1786440](https://gitlab.com/homescreen.rocks/payment-system/commit/1786440))


### Features

* **favicon:** adds favicon ([c4e042a](https://gitlab.com/homescreen.rocks/payment-system/commit/c4e042a)), closes [#74](https://gitlab.com/homescreen.rocks/payment-system/issues/74)
* **wizard:** adds charging accounts in step 3 ([88b6e97](https://gitlab.com/homescreen.rocks/payment-system/commit/88b6e97)), closes [#91](https://gitlab.com/homescreen.rocks/payment-system/issues/91)

## [1.0.0-rc.3](https://gitlab.com/homescreen.rocks/payment-system/compare/1.0.0-rc.2...1.0.0-rc.3) (2019-10-11)


### Bug Fixes

* **AdminUi:** update code to latest version ([216591e](https://gitlab.com/homescreen.rocks/payment-system/commit/216591e))
* **Checkout:** reset checkout sum ([2150309](https://gitlab.com/homescreen.rocks/payment-system/commit/2150309))
* **fix:** fix ([cc6da1c](https://gitlab.com/homescreen.rocks/payment-system/commit/cc6da1c))
* **OrderList:** disable cancelled checkboxes in orders overview ([76f61f5](https://gitlab.com/homescreen.rocks/payment-system/commit/76f61f5)), closes [#95](https://gitlab.com/homescreen.rocks/payment-system/issues/95)
* **UserForm:** setting a new password works again ([41a68e1](https://gitlab.com/homescreen.rocks/payment-system/commit/41a68e1)), closes [#92](https://gitlab.com/homescreen.rocks/payment-system/issues/92)


### Features

* **AccountList:** disable locked checkboxes in account overview ([f8d9249](https://gitlab.com/homescreen.rocks/payment-system/commit/f8d9249))
* **Monitoring:** implementing a monitoringscreen to see all transactions ([08430fa](https://gitlab.com/homescreen.rocks/payment-system/commit/08430fa))
* **producttype:** adds `order` property to ProductTypes ([ee214b7](https://gitlab.com/homescreen.rocks/payment-system/commit/ee214b7)), closes [#85](https://gitlab.com/homescreen.rocks/payment-system/issues/85)
* **registrationwizard:** adds an wizard feature for creating and configuration accounts ([31674cb](https://gitlab.com/homescreen.rocks/payment-system/commit/31674cb)), closes [#67](https://gitlab.com/homescreen.rocks/payment-system/issues/67)

## [1.0.0-rc.2](https://gitlab.com/homescreen.rocks/payment-system/compare/1.0.0-rc.1...1.0.0-rc.2) (2019-10-06)


### Bug Fixes

* **ClientEmulator:** nfcclient loops through newcard and removecard ([b55683f](https://gitlab.com/homescreen.rocks/payment-system/commit/b55683f))
* **Docker:** build backend in ci ([01a8aa8](https://gitlab.com/homescreen.rocks/payment-system/commit/01a8aa8)), closes [#69](https://gitlab.com/homescreen.rocks/payment-system/issues/69)
* **Docker:** fix image name ([a1b08a1](https://gitlab.com/homescreen.rocks/payment-system/commit/a1b08a1))
* **Migration:** add ENV toggle for migration execution ([0c0e57c](https://gitlab.com/homescreen.rocks/payment-system/commit/0c0e57c)), closes [#68](https://gitlab.com/homescreen.rocks/payment-system/issues/68)


### Features

* **Accounts:** adds a configurable overdraft facility per account ([0653fd6](https://gitlab.com/homescreen.rocks/payment-system/commit/0653fd6)), closes [#76](https://gitlab.com/homescreen.rocks/payment-system/issues/76)
* **Charge:** allow negative amounts for cash out purpose ([0b24d9d](https://gitlab.com/homescreen.rocks/payment-system/commit/0b24d9d))
* **Product:** adds `order` property to Product ([b9fca0b](https://gitlab.com/homescreen.rocks/payment-system/commit/b9fca0b)), closes [#72](https://gitlab.com/homescreen.rocks/payment-system/issues/72)
* **Roles:** adds `member` role with predefined discount of 10% ([804f49b](https://gitlab.com/homescreen.rocks/payment-system/commit/804f49b)), closes [#48](https://gitlab.com/homescreen.rocks/payment-system/issues/48)
* **Transactions:** adds possibility to cancel a Transaction ([29dc4f5](https://gitlab.com/homescreen.rocks/payment-system/commit/29dc4f5)), closes [#71](https://gitlab.com/homescreen.rocks/payment-system/issues/71)

## [1.0.0-rc.1](https://gitlab.com/homescreen.rocks/payment-system/compare/1.0.0-rc.0...1.0.0-rc.1) (2019-09-26)


### Bug Fixes

* **Migrations:** remove ef image an integrate migrate into startup ([308ebde](https://gitlab.com/homescreen.rocks/payment-system/commit/308ebde)), closes [#61](https://gitlab.com/homescreen.rocks/payment-system/issues/61)



## [1.0.0-rc.0](https://gitlab.com/homescreen.rocks/payment-system/compare/1.0.0-beta.4...1.0.0-rc.0) (2019-09-17)


### Bug Fixes

* **Account:** validates cardId for usage in other accounts ([c0f3637](https://gitlab.com/homescreen.rocks/payment-system/commit/c0f3637)), closes [#60](https://gitlab.com/homescreen.rocks/payment-system/issues/60)
* **Charge:** do not allow to charge 0 EUR ([925a731](https://gitlab.com/homescreen.rocks/payment-system/commit/925a731))
* **DB:** fixes connection string to connect to MariaDB ([29d53db](https://gitlab.com/homescreen.rocks/payment-system/commit/29d53db))
* **DockerFiles:** fixing alignments ([114998e](https://gitlab.com/homescreen.rocks/payment-system/commit/114998e))
* **Filter:** also allow filtering for account Id ([c3e1112](https://gitlab.com/homescreen.rocks/payment-system/commit/c3e1112))
* **Filter:** change account filter from id to name ([f4a291a](https://gitlab.com/homescreen.rocks/payment-system/commit/f4a291a))
* **Filter:** fix pagination ([8dcbf77](https://gitlab.com/homescreen.rocks/payment-system/commit/8dcbf77))
* **Filter:** update filter/sort configuration ([23c2dd7](https://gitlab.com/homescreen.rocks/payment-system/commit/23c2dd7))

## [1.0.0-beta.4](https://gitlab.com/homescreen.rocks/payment-system/compare/1.0.0-beta.3...1.0.0-beta.4) (2019-07-08)


### Bug Fixes

* **NfcClient:** remove console inputs ([239eb69](https://gitlab.com/homescreen.rocks/payment-system/commit/239eb69)), closes [#56](https://gitlab.com/homescreen.rocks/payment-system/issues/56)


### Features

* **Data:** add seeding script ([dd3808d](https://gitlab.com/homescreen.rocks/payment-system/commit/dd3808d)), closes [#28](https://gitlab.com/homescreen.rocks/payment-system/issues/28)



## [1.0.0-beta.3](https://gitlab.com/homescreen.rocks/payment-system/compare/1.0.0-beta.2...1.0.0-beta.3) (2019-06-19)


### Bug Fixes

* **Transactions:** do not send product images in TransactionResponse ([c52a989](https://gitlab.com/homescreen.rocks/payment-system/commit/c52a989)), closes [#55](https://gitlab.com/homescreen.rocks/payment-system/issues/55)



## [1.0.0-beta.2](https://gitlab.com/homescreen.rocks/payment-system/compare/1.0.0-beta.1...1.0.0-beta.2) (2019-06-19)


### Bug Fixes

* **DockerComposer:** add complete docker-compose.yml file ([59384d4](https://gitlab.com/homescreen.rocks/payment-system/commit/59384d4))
* **Transactions:** allow CashDesk user to list Transactions of all users ([a52d106](https://gitlab.com/homescreen.rocks/payment-system/commit/a52d106))



## [1.0.0-beta.1](https://gitlab.com/homescreen.rocks/payment-system/compare/1.0.0-beta.0...1.0.0-beta.1) (2019-06-18)


### Bug Fixes

* **Mapping:** fixes Automapper to use an unbugged version (6.1.0 instead of 6.1.1) ([27ab3b8](https://gitlab.com/homescreen.rocks/payment-system/commit/27ab3b8)), closes [#54](https://gitlab.com/homescreen.rocks/payment-system/issues/54)



## 1.0.0-beta.0 (2019-06-18)


### Bug Fixes

* **Accounts:** fixes an unhandles Exception when placing a card not associated with any account ([5fd6639](https://gitlab.com/homescreen.rocks/payment-backend/commit/5fd6639))
* **Accounts:** returns full AccountInformationen on both, update and creation ([13315ad](https://gitlab.com/homescreen.rocks/payment-backend/commit/13315ad))
* **API:** working with eager loading ([fcfaac9](https://gitlab.com/homescreen.rocks/payment-backend/commit/fcfaac9))
* **Core:** fix DbContext service lifetime ([9431bdf](https://gitlab.com/homescreen.rocks/payment-backend/commit/9431bdf))
* **Core:** fix error disconnecting NFC client ([6fac6b1](https://gitlab.com/homescreen.rocks/payment-backend/commit/6fac6b1))
* **Core:** optimize code ([6e1eb01](https://gitlab.com/homescreen.rocks/payment-backend/commit/6e1eb01))
* **Fixes:** several small bugs are fixed with this commit ([00ab277](https://gitlab.com/homescreen.rocks/payment-backend/commit/00ab277))
* **HubConnection:** do not allow to join a PoS if this is not configured ([46db45f](https://gitlab.com/homescreen.rocks/payment-backend/commit/46db45f)), closes [#41](https://gitlab.com/homescreen.rocks/payment-backend/issues/41)
* **HubConnections:** fixes disconnecting of cash-desk-clients ([db3c3ab](https://gitlab.com/homescreen.rocks/payment-backend/commit/db3c3ab))
* **HubConnections:** fixes error reporting on duplicate connections to PoS ([12587bc](https://gitlab.com/homescreen.rocks/payment-backend/commit/12587bc))
* **JwtAuthorizationTokenService:** improves token refreshing handling ([907d8a7](https://gitlab.com/homescreen.rocks/payment-backend/commit/907d8a7))
* **Kestrel:** fixes serving of static files by backend ([846a222](https://gitlab.com/homescreen.rocks/payment-backend/commit/846a222))
* **Kestrel:** serve angular SPA's from within /admin and /cashdesk ([03ca64a](https://gitlab.com/homescreen.rocks/payment-backend/commit/03ca64a))
* **Mapper:** fixes several small mapper issues ([471bd12](https://gitlab.com/homescreen.rocks/payment-backend/commit/471bd12))
* **NewCard:** catches an error with unknown cards ([a7f0c28](https://gitlab.com/homescreen.rocks/payment-backend/commit/a7f0c28))
* **NfcClient:** fix build, use dotnet publish ([d7e1a5b](https://gitlab.com/homescreen.rocks/payment-backend/commit/d7e1a5b))
* **NfcClient:** fix namespace ([8df4d14](https://gitlab.com/homescreen.rocks/payment-backend/commit/8df4d14))
* **NfcClient:** fix no-card startup issue ([b3224ae](https://gitlab.com/homescreen.rocks/payment-backend/commit/b3224ae))
* **NfcClient:** make reader selection configurable ([91c8905](https://gitlab.com/homescreen.rocks/payment-backend/commit/91c8905))
* **NfcClient:** use UID of NFC card ([7e2d644](https://gitlab.com/homescreen.rocks/payment-backend/commit/7e2d644))
* **Pagination:** fixes usage of page size default value ([03bc8de](https://gitlab.com/homescreen.rocks/payment-backend/commit/03bc8de)), closes [#36](https://gitlab.com/homescreen.rocks/payment-backend/issues/36)
* **PaymentHub:** fixes correct handling of aborted clients and frees PoS for new connection ([ebaa72b](https://gitlab.com/homescreen.rocks/payment-backend/commit/ebaa72b))
* **PointOfSaleRepo:** fixes an issue combining NfcClient and CashDeskClient to a single PoS ([d2b56d5](https://gitlab.com/homescreen.rocks/payment-backend/commit/d2b56d5))
* **ProductType:** fixes updating product types ([76b39a6](https://gitlab.com/homescreen.rocks/payment-backend/commit/76b39a6))
* **ProductTypes:** fixes creation/update of productTypes ([62be4d5](https://gitlab.com/homescreen.rocks/payment-backend/commit/62be4d5)), closes [#39](https://gitlab.com/homescreen.rocks/payment-backend/issues/39)
* **Readme:** some useless fix ([39c50e9](https://gitlab.com/homescreen.rocks/payment-backend/commit/39c50e9))
* **SignalR:** fix some signalr service errors ([af88b00](https://gitlab.com/homescreen.rocks/payment-backend/commit/af88b00))
* **Transactions:** refactors CreateTransaction messages ([015d31a](https://gitlab.com/homescreen.rocks/payment-backend/commit/015d31a))
* **Transactions:** return full Product as TransactionEntry ([13e5f17](https://gitlab.com/homescreen.rocks/payment-backend/commit/13e5f17))


### Build System

* **Dependencies:** updated all dependencies ([495f4c6](https://gitlab.com/homescreen.rocks/payment-backend/commit/495f4c6))
* **Docker:** fix ([8f86f03](https://gitlab.com/homescreen.rocks/payment-backend/commit/8f86f03))
* **Migrations:** adds Card migration ([dd06afa](https://gitlab.com/homescreen.rocks/payment-backend/commit/dd06afa))
* **Nfc-Client:** fix ([f277492](https://gitlab.com/homescreen.rocks/payment-backend/commit/f277492))
* **Nfc-Client:** fix ([66d5324](https://gitlab.com/homescreen.rocks/payment-backend/commit/66d5324))


### Features

* **Account:** adds LOCKED state to accounts ([9171bf5](https://gitlab.com/homescreen.rocks/payment-backend/commit/9171bf5)), closes [#16](https://gitlab.com/homescreen.rocks/payment-backend/issues/16)
* **API:** adding database connection ([6e96859](https://gitlab.com/homescreen.rocks/payment-backend/commit/6e96859))
* **API:** adds REST endpoint for creating Accounts ([5c96054](https://gitlab.com/homescreen.rocks/payment-backend/commit/5c96054)), closes [#14](https://gitlab.com/homescreen.rocks/payment-backend/issues/14)
* **API:** basic product type routes ([4513ba1](https://gitlab.com/homescreen.rocks/payment-backend/commit/4513ba1))
* **API:** dotnet new webapi ([1612227](https://gitlab.com/homescreen.rocks/payment-backend/commit/1612227))
* **Authentication:** adds JwtAuthentication services and REST login route ([8d26d9b](https://gitlab.com/homescreen.rocks/payment-backend/commit/8d26d9b)), closes [#17](https://gitlab.com/homescreen.rocks/payment-backend/issues/17)
* **Authentication:** adds password check ([83594a1](https://gitlab.com/homescreen.rocks/payment-backend/commit/83594a1)), closes [#33](https://gitlab.com/homescreen.rocks/payment-backend/issues/33)
* **Authorization:** adds account roles to backend ([e9b2d24](https://gitlab.com/homescreen.rocks/payment-backend/commit/e9b2d24))
* **Charge:** adds charge implementation ([46aac99](https://gitlab.com/homescreen.rocks/payment-backend/commit/46aac99))
* **Core:** add entities and models ([e8c3a32](https://gitlab.com/homescreen.rocks/payment-backend/commit/e8c3a32))
* **DB:** first try to use foreingnt keys in entityFramework ([adb2158](https://gitlab.com/homescreen.rocks/payment-backend/commit/adb2158))
* **Debug:** adds a simple nfc-service-emulator to solution ([7fa6147](https://gitlab.com/homescreen.rocks/payment-backend/commit/7fa6147))
* **Emulator:** implement token authentication ([e596912](https://gitlab.com/homescreen.rocks/payment-backend/commit/e596912)), closes [#22](https://gitlab.com/homescreen.rocks/payment-backend/issues/22)
* **EntityFramework:** try and try and try and .. ([d3d8640](https://gitlab.com/homescreen.rocks/payment-backend/commit/d3d8640))
* **Hub:** CashDesk: send current card after joinen a PoS with existing NfcClient ([8761af9](https://gitlab.com/homescreen.rocks/payment-backend/commit/8761af9))
* **JoinPos:** CashDesk can now join and will ne anounced to nfc (if present) ([9eb6ac9](https://gitlab.com/homescreen.rocks/payment-backend/commit/9eb6ac9))
* **Logger:** adds ILogger instead of Console.WriteLine ([48a2fa3](https://gitlab.com/homescreen.rocks/payment-backend/commit/48a2fa3)), closes [#11](https://gitlab.com/homescreen.rocks/payment-backend/issues/11)
* **MSC:** adds put card to reader use-case ([f333739](https://gitlab.com/homescreen.rocks/payment-backend/commit/f333739))
* **MSC:** adds RemovedCard message handling ([adba18c](https://gitlab.com/homescreen.rocks/payment-backend/commit/adba18c))
* **PaymentHub:** adds hello message to /payment-hub ([a89d253](https://gitlab.com/homescreen.rocks/payment-backend/commit/a89d253))
* **PoS:** adds nfc and cashDesk satates to REST model ([c8635af](https://gitlab.com/homescreen.rocks/payment-backend/commit/c8635af)), closes [#37](https://gitlab.com/homescreen.rocks/payment-backend/issues/37)
* **PoS:** fully refactored PoS handling ([1b5e07c](https://gitlab.com/homescreen.rocks/payment-backend/commit/1b5e07c)), closes [#23](https://gitlab.com/homescreen.rocks/payment-backend/issues/23)
* **Product:** adds product image ([642c962](https://gitlab.com/homescreen.rocks/payment-backend/commit/642c962)), closes [#40](https://gitlab.com/homescreen.rocks/payment-backend/issues/40)
* **Purchase:** adds purchase implementation ([9c5c897](https://gitlab.com/homescreen.rocks/payment-backend/commit/9c5c897)), closes [#12](https://gitlab.com/homescreen.rocks/payment-backend/issues/12)
* **REST:** add create/update for product ([60f4c72](https://gitlab.com/homescreen.rocks/payment-backend/commit/60f4c72))
* **REST:** add product type routes ([b625299](https://gitlab.com/homescreen.rocks/payment-backend/commit/b625299)), closes [#26](https://gitlab.com/homescreen.rocks/payment-backend/issues/26)
* **REST:** add Products routes ([e5ade62](https://gitlab.com/homescreen.rocks/payment-backend/commit/e5ade62))
* **REST:** adds API for getting accounts /accounts[/{id}] ([07e1021](https://gitlab.com/homescreen.rocks/payment-backend/commit/07e1021)), closes [#15](https://gitlab.com/homescreen.rocks/payment-backend/issues/15)
* **REST:** adds roles property to Accounts roles ([b7d378c](https://gitlab.com/homescreen.rocks/payment-backend/commit/b7d378c))
* **REST:** adds Roles route ([672776e](https://gitlab.com/homescreen.rocks/payment-backend/commit/672776e)), closes [#52](https://gitlab.com/homescreen.rocks/payment-backend/issues/52)
* **REST:** implement filter posibilities for all entities ([986f6c2](https://gitlab.com/homescreen.rocks/payment-backend/commit/986f6c2)), closes [#29](https://gitlab.com/homescreen.rocks/payment-backend/issues/29)
* **Routing:** add GUID contstraints for routes containing IDs ([8cbcba6](https://gitlab.com/homescreen.rocks/payment-backend/commit/8cbcba6))
* **Services:** adding AccountRepository, adding cardId to Account Class ([0eed7f3](https://gitlab.com/homescreen.rocks/payment-backend/commit/0eed7f3))
* **SignalR:** adds connection handling for GeneralSubscribers ([d843763](https://gitlab.com/homescreen.rocks/payment-backend/commit/d843763)), closes [#43](https://gitlab.com/homescreen.rocks/payment-backend/issues/43)
* **SignalR:** send updated NewCard to PoS clients after successful transaction ([624d042](https://gitlab.com/homescreen.rocks/payment-backend/commit/624d042))
* **Transaction:** implementing a comment property for transactions ([eeaf930](https://gitlab.com/homescreen.rocks/payment-backend/commit/eeaf930)), closes [#19](https://gitlab.com/homescreen.rocks/payment-backend/issues/19)
* **TransactionRepo:** implementing functions ([8f93fdb](https://gitlab.com/homescreen.rocks/payment-backend/commit/8f93fdb))
* **Transactions:** add transactions ([e37cb28](https://gitlab.com/homescreen.rocks/payment-backend/commit/e37cb28))
* **Transactions:** adds API endpoint to list all transactions of an user ([3839020](https://gitlab.com/homescreen.rocks/payment-backend/commit/3839020)), closes [#3](https://gitlab.com/homescreen.rocks/payment-backend/issues/3)
* **Validations:** adds validation for unique key fields on creation and updating entities ([d74792a](https://gitlab.com/homescreen.rocks/payment-backend/commit/d74792a)), closes [#34](https://gitlab.com/homescreen.rocks/payment-backend/issues/34)
* **Websocket:** adds registration call for NFC clients ([2563338](https://gitlab.com/homescreen.rocks/payment-backend/commit/2563338))
