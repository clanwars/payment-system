# Payment Frontends
This project mainly consists of two UIs:
* Cashdesk
* AdminUi

## Development

Build the common payment-lib:
* `yarn run build:payment-lib`

Use the yarn jobs to serve developments servers for cashdesk/adminUi:
* `yarn run start:admin`
* `yarn run start:cashdesk`

## Translation
* I used POEditor for online translation: https://poeditor.com/join/project?hash=vLQQGa4ODM
