import {Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from '@angular/core';
import {FormBuilder, ValidationErrors, Validators} from '@angular/forms';
import {AccountInfo} from 'payment-lib';

@Component({
  selector: 'app-account-form',
  templateUrl: './account-form.component.html',
  styleUrls: ['./account-form.component.scss']
})
export class AccountFormComponent implements OnChanges {
  accountForm = this.fb.group({
    id: [null],
    email: ['', Validators.required],
    firstName: ['', Validators.required],
    lastName: ['', Validators.required],
    cardId: [],
    balance: [{value: 0, disabled: true}],
    overdraft: [0, Validators.compose([Validators.min(0), Validators.required])],
    discount: [0, Validators.compose([Validators.min(0), Validators.max(100), Validators.required])],
    locked: [false, Validators.required],
    roles: this.fb.array([])
  });
  @Input() public account: AccountInfo | null;
  @Input() public validationErrors: ValidationErrors;
  @Output() public save = new EventEmitter<AccountInfo>();

  constructor(private fb: FormBuilder) {
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (this.account) {
      this.accountForm.patchValue(this.account);
      this.accountForm.get('discount').setValue(this.accountForm.get('discount').value * 100);
    }

    if (changes.validationErrors && changes.validationErrors.currentValue !== changes.validationErrors.previousValue) {
      for (const key in this.validationErrors) {
        if (this.validationErrors.hasOwnProperty(key)) {
          const lowerCaseKey = key.charAt(0).toLowerCase() + key.slice(1);
          const field = this.accountForm.get(lowerCaseKey);
          field.setErrors({backendErrors: this.validationErrors[key]});
        }
      }
    }
  }

  saveAccount(): void {
    this.accountForm.get('discount').setValue(this.accountForm.get('discount').value / 100);
    if (this.accountForm.get('cardId').value === '') {
      this.accountForm.get('cardId').setValue(null);
    }
    this.save.emit(this.accountForm.getRawValue());
  }
}
