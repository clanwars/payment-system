import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AccountsDetailsComponent} from './accounts/accounts-details/accounts-details.component';
import {AccountsListComponent} from './accounts/accounts-list/accounts-list.component';
import {ProductsDetailsComponent} from './products/products-details/products-details.component';
import {ProductsListComponent} from './products/products-list/products-list.component';
import {ProductTypesListComponent} from './product-types/product-types-list/product-types-list.component';
import {ProductTypesDetailsComponent} from './product-types/product-types-details/product-types-details.component';
import {PointsOfSaleDetailsComponent} from './points-of-sale/points-of-sale-details/points-of-sale-details.component';
import {TransactionDetailsComponent} from './transactions/transaction-details/transaction-details.component';
import {TransactionListComponent} from './transactions/transaction-list/transaction-list.component';
import {GetAccountResolverService} from './resolver/get-account-resolver.service';
import {ProductResolverService} from './resolver/product-resolver.service';
import {ProductTypeResolverService} from './resolver/product-type-resolver.service';
import {AuthGuard} from './services/auth-guard.service';
import {WizardPageComponent} from './registration-wizard/wizard-page/wizard-page.component';
import {PointOfSaleResolverService} from './resolver/point-of-sale-resolver.service';
import {TransactionResolverService} from './resolver/transaction-resolver.service';
import {PointsOfSaleListComponent} from './points-of-sale/points-of-sale-list/points-of-sale-list.component';
import {PointsOfSaleSelectedGuard} from './services/points-of-sale-selected.guard';
import {ConfigurationComponent} from './configuration/configuration.component';

const routes: Routes = [
  {
    path: 'registration-wizard',
    component: WizardPageComponent,
    canActivate: [AuthGuard, PointsOfSaleSelectedGuard]
  },
  {
    path: 'accounts',
    component: AccountsListComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'accounts/:id',
    component: AccountsDetailsComponent,
    resolve: {
      account: GetAccountResolverService
    },
    canActivate: [AuthGuard]
  },
  {
    path: 'transactions',
    component: TransactionListComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'transactions/:id',
    component: TransactionDetailsComponent,
    resolve: {transaction: TransactionResolverService},
    canActivate: [AuthGuard]
  },
  {
    path: 'products',
    component: ProductsListComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'products/:id',
    component: ProductsDetailsComponent,
    resolve: {product: ProductResolverService},
    canActivate: [AuthGuard]
  },
  {
    path: 'product-types',
    component: ProductTypesListComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'product-types/:id',
    component: ProductTypesDetailsComponent,
    resolve: {productType: ProductTypeResolverService},
    canActivate: [AuthGuard]
  },
  {
    path: 'points-of-sale',
    component: PointsOfSaleListComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'points-of-sale/:id',
    component: PointsOfSaleDetailsComponent,
    resolve: {pointOfSale: PointOfSaleResolverService},
    canActivate: [AuthGuard]
  },
  {
    path: 'configuration',
    component: ConfigurationComponent,
    canActivate: [AuthGuard]
  },
  {
    path: '',
    redirectTo: 'accounts',
    pathMatch: 'full'
  },
  {
    path: 'auth',
    loadChildren: () => import('./auth/auth.module').then(m => m.AuthModule),
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' })],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
