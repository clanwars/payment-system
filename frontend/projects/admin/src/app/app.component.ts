import {Component} from '@angular/core';
import {delay, tap} from 'rxjs/operators';
import {AuthService} from 'payment-lib';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'payment-admin-ui';
  public isAuthenticated: boolean;

  constructor(private authService: AuthService) {
    this.authService.isLoggedIn$.pipe(
      delay(50),
      tap(authState => this.isAuthenticated = authState)
    ).subscribe();
  }
}
