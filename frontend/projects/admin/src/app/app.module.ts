import {BrowserModule} from '@angular/platform-browser';
import {DEFAULT_CURRENCY_CODE, NgModule} from '@angular/core';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {NavigationComponent} from './layout/navigation/navigation.component';
import {AccountsDetailsComponent} from './accounts/accounts-details/accounts-details.component';
import {AccountsListComponent} from './accounts/accounts-list/accounts-list.component';
import {HttpClientModule} from '@angular/common/http';
import {NgbModalModule, NgbPaginationModule, NgbPopoverModule, NgbTooltipModule} from '@ng-bootstrap/ng-bootstrap';
import {ReactiveFormsModule} from '@angular/forms';
import {ConfirmDialogComponent} from './layout/confirm-dialog/confirm-dialog.component';
import {ProductsDetailsComponent} from './products/products-details/products-details.component';
import {ProductsListComponent} from './products/products-list/products-list.component';
import {ProductTypesDetailsComponent} from './product-types/product-types-details/product-types-details.component';
import {ProductTypesListComponent} from './product-types/product-types-list/product-types-list.component';
import {PointsOfSaleListComponent} from './points-of-sale/points-of-sale-list/points-of-sale-list.component';
import {PointsOfSaleDetailsComponent} from './points-of-sale/points-of-sale-details/points-of-sale-details.component';
import {TransactionListComponent} from './transactions/transaction-list/transaction-list.component';
import {TransactionDetailsComponent} from './transactions/transaction-details/transaction-details.component';
import {RouterModule} from '@angular/router';
import {ConcatProductsPipe} from './pipes/concat-products.pipe';
import {AuthGuard} from './services/auth-guard.service';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatSortModule} from '@angular/material/sort';
import {MatTableModule} from '@angular/material/table';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {ImageCropperModule} from 'ngx-image-cropper';
import {WizardPageComponent} from './registration-wizard/wizard-page/wizard-page.component';
import {WizardStepperComponent} from './registration-wizard/wizard-stepper/wizard-stepper.component';
import {AccountFormComponent} from './accounts/account-form/account-form.component';
import {EventSubscriberService} from './services/event-subscriber.service';
import {environment} from '../environments/environment';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatStepperModule} from '@angular/material/stepper';
import {MatCardModule} from '@angular/material/card';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {PosFooterComponent} from './layout/pos-footer/pos-footer.component';
import {PointsOfSaleStatusbarComponent} from './points-of-sale/points-of-sale-statusbar/points-of-sale-statusbar.component';
import {PointsOfSaleStatusbarItemComponent} from './points-of-sale/points-of-sale-statusbar-item/points-of-sale-statusbar-item.component';
import {ConfigurationComponent} from './configuration/configuration.component';
import {ToastrModule} from 'ngx-toastr';
import {OAuthModule} from 'angular-oauth2-oidc';
import {LibModule} from 'payment-lib';

@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent,
    AccountsDetailsComponent,
    AccountsListComponent,
    ProductsDetailsComponent,
    ProductsListComponent,
    ProductTypesDetailsComponent,
    ProductTypesListComponent,
    ConfirmDialogComponent,
    PointsOfSaleListComponent,
    PointsOfSaleDetailsComponent,
    TransactionListComponent,
    TransactionDetailsComponent,
    ConcatProductsPipe,
    WizardPageComponent,
    WizardStepperComponent,
    AccountFormComponent,
    AccountFormComponent,
    PosFooterComponent,
    PointsOfSaleStatusbarComponent,
    PointsOfSaleStatusbarItemComponent,
    ConfigurationComponent,
  ],
  imports: [
    RouterModule,
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    ReactiveFormsModule,
    NgbPaginationModule,
    NgbModalModule,
    NgbTooltipModule,
    NgbPopoverModule,
    ToastrModule.forRoot({}),
    OAuthModule.forRoot({
      resourceServer: {
        allowedUrls: ['/api/'],
        sendAccessToken: true
      }
    }),
    MatTableModule,
    MatPaginatorModule,
    BrowserAnimationsModule,
    MatSortModule,
    MatProgressSpinnerModule,
    MatFormFieldModule,
    MatInputModule,
    ImageCropperModule,
    MatCheckboxModule,
    MatSidenavModule,
    MatExpansionModule,
    MatToolbarModule,
    MatStepperModule,
    MatCardModule,
    FontAwesomeModule,
    LibModule,
  ],
  providers: [
    AuthGuard,
    EventSubscriberService,
    // todo: setting DEFAULT_CURRENCY_CODE is deprecated and will be removed in angular 10
    // we can remove this line in angular 10 as the currency will be taken from the current locale then
    // https://angular.io/api/core/DEFAULT_CURRENCY_CODE
    {provide: DEFAULT_CURRENCY_CODE, useValue: environment.defaultCurrency},
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
