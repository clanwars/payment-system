import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree} from '@angular/router';
import {from, Observable, of, switchMap} from 'rxjs';
import {AuthService} from 'payment-lib';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class RedirectIfLoggedInGuard implements CanActivate {

  constructor(private authService: AuthService, private router: Router) {
  }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return this.authService.isLoggedIn$.pipe(
      map(isLoggedIn => !isLoggedIn),
      switchMap(isNotLoggedIn => {
        if (isNotLoggedIn) {
          return of(true);
        } else {
          return from(this.router.navigate(['/']));
        }
      })
    );
  }

}
