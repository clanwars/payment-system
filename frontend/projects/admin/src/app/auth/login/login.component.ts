import {Component} from '@angular/core';
import {AuthService} from 'payment-lib';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {

  user: string;
  password: string;
  submitted: boolean;
  rememberMe: boolean;
  errors: string[];

  constructor(private authService: AuthService, private router: Router) {
  }

  login(): void {
    this.authService.login();
  }
}
