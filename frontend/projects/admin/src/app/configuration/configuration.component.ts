import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, Validators} from '@angular/forms';
import {AuthService, Filter, PointOfSaleFilter, PointOfSaleInfo, PointsOfSaleService} from 'payment-lib';
import {Observable, Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

@Component({
  selector: 'app-configuration',
  templateUrl: './configuration.component.html',
  styleUrls: ['./configuration.component.scss']
})
export class ConfigurationComponent implements OnInit, OnDestroy {

  posConfigurationForm = this.fb.group({
    pos: [null, Validators.compose([
      Validators.required
    ])],
  });

  isCashDesk: boolean;
  poses$: Observable<PointOfSaleInfo[]>;
  destroy$ = new Subject<void>();
  private currentPos: PointOfSaleInfo;

  constructor(private authService: AuthService, private fb: FormBuilder, private posService: PointsOfSaleService) {
  }

  ngOnInit(): void {
    this.isCashDesk = this.authService.hasRoleCashDesk();

    this.poses$ = this.posService.getAllPointsOfSaleFiltered(new Filter(PointOfSaleFilter));

    this.currentPos = JSON.parse(localStorage.getItem('currentPos'));
    if (this.currentPos != null) {
      this.posConfigurationForm.get('pos').patchValue(this.currentPos);
    }

    this.posConfigurationForm.valueChanges.pipe(
      takeUntil(this.destroy$)
    ).subscribe(() => {
      this.currentPos = this.posConfigurationForm.get('pos').value;
      localStorage.setItem('currentPos', JSON.stringify(this.currentPos));
    });
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  posCompare(optionOne, optionTwo): boolean {
    if (optionOne === null || optionTwo === null) {
      return false;
    }

    return optionOne.id === optionTwo.id;
  }
}
