import {HttpErrorResponse} from '@angular/common/http';
import {FormGroup, ValidationErrors} from '@angular/forms';
import {StatusCodes} from 'http-status-codes';

export class BackendErrorHandler {

  /***
   * Pushed backend validation errors to form properties
   * @param form FormGroup
   */
  public static pushBackendErrorsToForm = (form: FormGroup) => {
    return (err: HttpErrorResponse) => {
      if (err.status === StatusCodes.BAD_REQUEST) {
        console.log(err.error);
        for (const key in err.error.errors as ValidationErrors) {
          if (err.error.errors.hasOwnProperty(key)) {
            const lowerCaseKey = key.charAt(0).toLowerCase() + key.slice(1);
            const field = form.get(lowerCaseKey);
            if (field === null) {
              console.warn('A form field with the name ' + key + ' does not exist!');
            } else {
              field.setErrors({backendErrors: err.error.errors[key]});
            }
          }
        }
      }
    };
  }

}
