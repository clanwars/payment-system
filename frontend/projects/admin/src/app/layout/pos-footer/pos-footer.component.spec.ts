import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { PosFooterComponent } from './pos-footer.component';

describe('PosFooterComponent', () => {
  let component: PosFooterComponent;
  let fixture: ComponentFixture<PosFooterComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ PosFooterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PosFooterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
