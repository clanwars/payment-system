import {AfterViewInit, Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Filter, PagedDataSource, PointOfSaleFilter, PointOfSaleInfo, PointsOfSaleService, RelationalOperator} from 'payment-lib';
import {merge, Observable, Subject} from 'rxjs';
import {debounceTime, map, switchMap, takeUntil, tap} from 'rxjs/operators';
import {EventSubscriberService} from '../../services/event-subscriber.service';
import {MatPaginator, PageEvent} from '@angular/material/paginator';
import {MatSort, Sort} from '@angular/material/sort';
import {FormControl} from '@angular/forms';
import {faEdit, faPlusSquare} from '@fortawesome/free-regular-svg-icons';

@Component({
  selector: 'app-points-of-sale-list',
  templateUrl: './points-of-sale-list.component.html',
  styleUrls: ['./points-of-sale-list.component.scss']
})
export class PointsOfSaleListComponent implements OnInit, AfterViewInit, OnDestroy {
  faPlus = faPlusSquare;
  faEdit = faEdit;

  public currentResult: PointOfSaleInfo[];
  private destroy = new Subject<void>();

  columnsToDisplay = ['id', 'name', 'nfcClient', 'cashDesk', 'ready', 'actions'];
  filterRow = ['id-filter', 'name-filter', 'nfcClient-filter', 'cashDesk-filter', 'ready-filter', 'actions-filter'];

  dataSource: PagedDataSource<PointOfSaleInfo, PointOfSaleFilter>;

  allFilters$: Observable<[keyof PointOfSaleFilter, RelationalOperator, string | number | boolean]>;

  nameFilterValue = new FormControl('');
  nameFilterValue$: Observable<['name', RelationalOperator, string]>;

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(
    private pointsOfSaleService: PointsOfSaleService,
    private eventSubscriberService: EventSubscriberService
  ) {
  }

  ngOnInit() {
    this.dataSource = new PagedDataSource(this.pointsOfSaleService.fetch, PointOfSaleFilter);
    this.dataSource.loadData();

    this.pointsOfSaleService.getAllPointsOfSaleFiltered(new Filter(PointOfSaleFilter)).subscribe(posses => {
      this.currentResult = posses;
    });

    this.eventSubscriberService.pointOfSaleEvents.pipe(
      takeUntil(this.destroy),
      switchMap(_ => {
        return this.pointsOfSaleService.getAllPointsOfSaleFiltered(new Filter(PointOfSaleFilter));
      })
    ).subscribe(posses => {
      this.currentResult = posses;
    });
  }

  ngAfterViewInit() {
    // reset the paginator after sorting
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

    this.nameFilterValue$ = this.nameFilterValue.valueChanges.pipe(
      debounceTime(300),
      map(v => ['name', RelationalOperator.CONTAINS_CI, v])
    );

    this.allFilters$ = merge(this.nameFilterValue$);

    merge(this.sort.sortChange, this.paginator.page, this.allFilters$)
      .pipe(
        tap((change: [keyof PointOfSaleFilter, RelationalOperator, string | boolean] | Sort | PageEvent) => {
            this.loadPoSInfosPage(change);
          }
        )
      )
      .subscribe();
  }

  loadPoSInfosPage<K extends keyof PointOfSaleFilter>(change: [K, RelationalOperator, string | boolean] | Sort | PageEvent) {
    if (change instanceof Array) {
      const key = change[0] as K;
      const operator = change[1];
      const value = change[2];
      if (value === '') {
        this.dataSource.filter.clearFilter(key);
      } else {
        // @ts-ignore
        this.dataSource.filter.setFilter(key, operator, value);
      }
    }
    this.dataSource.filter.clearSort();
    // @ts-ignore
    this.dataSource.filter.setSort(this.sort.active, this.sort._direction);
    this.dataSource.filter.setPage(this.paginator.pageIndex + 1);
    this.dataSource.filter.setPageSize(this.paginator.pageSize);
    this.dataSource.loadData();
  }

  ngOnDestroy(): void {
    this.destroy.next();
    this.destroy.complete();
  }
}
