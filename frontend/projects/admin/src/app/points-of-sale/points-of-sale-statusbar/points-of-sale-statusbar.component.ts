import {Component, OnDestroy, OnInit} from '@angular/core';
import {Filter, PointOfSaleFilter, PointOfSaleInfo, PointsOfSaleService} from 'payment-lib';
import {Subject} from 'rxjs';
import {EventSubscriberService} from '../../services/event-subscriber.service';
import {switchMap, takeUntil} from 'rxjs/operators';

@Component({
  selector: 'app-points-of-sale-statusbar',
  templateUrl: './points-of-sale-statusbar.component.html',
  styleUrls: ['./points-of-sale-statusbar.component.scss']
})
export class PointsOfSaleStatusbarComponent implements OnInit, OnDestroy {
  public currentResult: PointOfSaleInfo[];
  private destroy = new Subject<void>();

  constructor(
    private pointsOfSaleService: PointsOfSaleService,
    private eventSubscriberService: EventSubscriberService
  ) {
  }

  ngOnInit() {
    this.pointsOfSaleService.getAllPointsOfSaleFiltered(new Filter(PointOfSaleFilter)).subscribe(posses => {
      this.currentResult = posses;
    });

    this.eventSubscriberService.pointOfSaleEvents.pipe(
      takeUntil(this.destroy),
      switchMap(_ => {
        return this.pointsOfSaleService.getAllPointsOfSaleFiltered(new Filter(PointOfSaleFilter));
      })
    ).subscribe(posses => {
      this.currentResult = posses;
    });
  }

  ngOnDestroy(): void {
    this.destroy.next();
    this.destroy.complete();
  }
}
