import {Component, EventEmitter, OnInit} from '@angular/core';
import {FormBuilder, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {CreateProductType, ProductType, ProductTypesService, UpdateProductType} from 'payment-lib';
import {Observable} from 'rxjs';
import {BackendErrorHandler} from '../../helper/backend-error.handler';
import {ImageCroppedEvent} from 'ngx-image-cropper';
import {ToastrService} from 'ngx-toastr';
import {catchError, tap} from 'rxjs/operators';

@Component({
  selector: 'app-product-types-details',
  templateUrl: './product-types-details.component.html',
  styleUrls: ['./product-types-details.component.scss']
})
export class ProductTypesDetailsComponent implements OnInit {
  public productType: ProductType;
  productTypeForm = this.fb.group({
    id: [null],
    order: [0, Validators.required],
    name: ['', Validators.required],
    locked: [false, Validators.required],
    image: []
  });

  public onImageUploadEvent: EventEmitter<any> = new EventEmitter<any>();
  public croppedImage: string | null | undefined;

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private productTypesService: ProductTypesService,
    private toastr: ToastrService
  ) {
  }

  ngOnInit() {
    this.productType = this.route.snapshot.data.productType;
    if (this.productType !== null) {
      this.productTypeForm.patchValue(this.productType);
    }
  }

  save(): void {
    const formValue = this.productTypeForm.getRawValue() as ProductType;
    let response$: Observable<ProductType>;

    if (formValue.id === null) {
      const createMessage: CreateProductType = {
        name: formValue.name,
        locked: formValue.locked,
        order: formValue.order,
        image: formValue.image
      };
      response$ = this.productTypesService.createProductType(createMessage);
    } else {
      const updateMessage: UpdateProductType = {
        name: formValue.name,
        locked: formValue.locked,
        order: formValue.order,
        image: formValue.image?.startsWith('data:') ? formValue.image : null
      };
      response$ = this.productTypesService.updateProductType(formValue.id, updateMessage);
    }

    response$.pipe(
      tap(() => {
        this.toastr.success('Product type saved.', 'Success');
      }),
      catchError((err) => {
        this.toastr.error('Couldnt save product type!', 'Error');
        throw err;
      })
    ).subscribe(
      (pt) => {
        this.router.navigate(['/product-types', pt.id]);
      },
      BackendErrorHandler.pushBackendErrorsToForm(this.productTypeForm),
    );
  }

  imageCropped($event: ImageCroppedEvent) {
    this.croppedImage = $event.base64;
    this.productTypeForm.get('image').setValue($event.base64);
  }
}
