import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {ConfirmDialogService} from '../../layout/confirm-dialog/confirm-dialog.service';
import {MatPaginator, PageEvent} from '@angular/material/paginator';
import {MatSort, Sort} from '@angular/material/sort';
import {debounceTime, map, tap} from 'rxjs/operators';
import {merge, Observable} from 'rxjs';
import {FormControl} from '@angular/forms';
import {PagedDataSource, Product, ProductFilter, ProductsService, RelationalOperator, SortDirection} from 'payment-lib';
import {faPlusSquare, faEdit} from '@fortawesome/free-regular-svg-icons';

@Component({
  selector: 'app-products-list',
  templateUrl: './products-list.component.html',
  styleUrls: ['./products-list.component.scss']
})
export class ProductsListComponent implements OnInit, AfterViewInit {
  faPlus = faPlusSquare;
  faEdit = faEdit;

  columnsToDisplay = ['id', 'name', 'price', 'description', 'order', 'productType', 'locked', 'actions'];
  filterRow = ['id-filter', 'name-filter', 'price-filter', 'description-filter', 'order-filter',
    'productType-filter', 'locked-filter', 'actions-filter'];
  dataSource: PagedDataSource<Product, ProductFilter>;

  allFilters$: Observable<[keyof ProductFilter, RelationalOperator, string | boolean]>;

  nameFilterValue = new FormControl('');
  nameFilterValue$: Observable<['name', RelationalOperator, string]>;
  priceFilterValue = new FormControl('');
  priceFilterValue$: Observable<['price', RelationalOperator, string]>;
  descriptionFilterValue = new FormControl('');
  descriptionFilterValue$: Observable<['description', RelationalOperator, string]>;
  productTypeFilterValue = new FormControl('');
  productTypeFilterValue$: Observable<['productType.Name', RelationalOperator, string]>;

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(private productsService: ProductsService, private confirmDialog: ConfirmDialogService) {
  }

  ngOnInit() {
    this.dataSource = new PagedDataSource(this.productsService.fetch, ProductFilter);
    this.dataSource.filter.setSort('name', SortDirection.ASC);
    this.dataSource.loadData();
  }

  ngAfterViewInit() {
    // reset the paginator after sorting
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

    this.nameFilterValue$ = this.nameFilterValue.valueChanges.pipe(
      debounceTime(300),
      map(v => ['name', RelationalOperator.CONTAINS_CI, v])
    );

    this.priceFilterValue$ = this.priceFilterValue.valueChanges.pipe(
      debounceTime(300),
      map(v => ['price', RelationalOperator.EQUALS, v])
    );

    this.descriptionFilterValue$ = this.descriptionFilterValue.valueChanges.pipe(
      debounceTime(300),
      map(v => ['description', RelationalOperator.CONTAINS_CI, v])
    );

    this.productTypeFilterValue$ = this.productTypeFilterValue.valueChanges.pipe(
      debounceTime(300),
      map(v => ['productType.Name', RelationalOperator.CONTAINS_CI, v])
    );

    this.allFilters$ = merge(this.nameFilterValue$, this.priceFilterValue$, this.descriptionFilterValue$, this.productTypeFilterValue$);

    merge(this.sort.sortChange, this.paginator.page, this.allFilters$)
      .pipe(
        tap((change: [keyof ProductFilter, RelationalOperator, string] | Sort | PageEvent) => this.loadProductPage(change))
      )
      .subscribe();
  }

  loadProductPage<K extends keyof ProductFilter>(change: [K, RelationalOperator, string] | Sort | PageEvent) {
    if (change instanceof Array) {
      const key = change[0] as K;
      const operator = change[1];
      const value = change[2];
      if (value === '') {
        this.dataSource.filter.clearFilter(key);
      } else {
        // @ts-ignore
        this.dataSource.filter.setFilter(key, operator, value);
      }
    }
    this.dataSource.filter.clearSort();
    // @ts-ignore
    this.dataSource.filter.setSort(this.sort.active, this.sort._direction);
    this.dataSource.filter.setPage(this.paginator.pageIndex + 1);
    this.dataSource.filter.setPageSize(this.paginator.pageSize);
    this.dataSource.loadData();
  }
}
