import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  OnDestroy,
  OnInit,
  ViewChild
} from '@angular/core';
import {interval, Observable, Subject} from 'rxjs';
import {debounce, distinctUntilChanged, filter, map, switchMap, takeUntil, tap} from 'rxjs/operators';
import {FormControl, ValidationErrors, Validators} from '@angular/forms';
import {
  AccountFilter,
  AccountInfo,
  AccountsService,
  CardInfo,
  CreateChargeTransaction,
  Filter,
  PagedResult,
  PointsOfSaleService,
  RelationalOperator,
  SortDirection,
  TransactionsService
} from 'payment-lib';
import {NfcReaderService} from '../../services/nfc-reader.service';
import {StatusCodes} from 'http-status-codes';
import {MatStepper} from '@angular/material/stepper';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-wizard-stepper',
  templateUrl: './wizard-stepper.component.html',
  changeDetection: ChangeDetectionStrategy.Default,
  styleUrls: ['./wizard-stepper.component.scss'],
  providers: [NfcReaderService]
})
export class WizardStepperComponent implements OnInit, OnDestroy, AfterViewInit {
  @ViewChild('stepper') stepper: MatStepper;
  searchUser = new Subject<KeyboardEvent>();
  userResults$: Observable<PagedResult<AccountInfo>>;
  posConnected: Observable<boolean>;
  account: AccountInfo;
  selectedIndex = 0;
  accountFormValidationErrors: ValidationErrors;
  cardIdInputControl = new FormControl('', Validators.compose([Validators.required, Validators.minLength(1)]));
  searchUserInputControl = new FormControl();
  chargeAmountInputControl = new FormControl(0, Validators.compose([Validators.min(0), Validators.max(100)]));

  private currentCard: CardInfo | null;
  private destroy$ = new Subject<void>();
  private userResults: PagedResult<AccountInfo>;

  constructor(private accountService: AccountsService,
              private nfcReaderService: NfcReaderService,
              private transactionService: TransactionsService,
              private posService: PointsOfSaleService,
              private toastr: ToastrService
  ) {
  }

  ngOnInit() {
    this.nfcReaderService.currentCard$.subscribe(cc => this.currentCard = cc);

    this.posConnected = this.posService.currentPos$.pipe(
      tap(async pos => {
        if (pos !== null) {
          await this.nfcReaderService.startHubConnection(pos.id);
        }
      }),
      switchMap(pos => this.nfcReaderService.connectionState$),
      takeUntil(this.destroy$)
    );

    this.searchUser.asObservable().pipe(
      takeUntil(this.destroy$),
      map(event => event.key),
      filter(key => key === 'Enter')
    ).subscribe(() => {
      if (this.userResults && this.userResults.rowCount === 1) {
        this.setAccount(this.userResults.results[0]);
      }
    });

    this.userResults$ = this.searchUser.asObservable().pipe(
      map(event => (event.target as HTMLInputElement).value),
      debounce(() => interval(300)),
      distinctUntilChanged(),
      filter(input => input !== ''),
      tap(console.log),
      switchMap(search => {
        const accFilter = new Filter(AccountFilter);
        accFilter.setPageSize(10);
        accFilter.setFilter('email', RelationalOperator.CONTAINS_CI, search);
        accFilter.setSort('email', SortDirection.ASC);
        return this.accountService.getFilteredAccounts(accFilter);
      }),
      tap(res => this.userResults = res)
    );
  }

  ngOnDestroy(): void {
    this.destroy$.next();
  }

  setAccount(account: AccountInfo) {
    this.account = account;
    this.selectedIndex = 1;
  }

  useCurrentCard() {
    if (this.currentCard === null) {
      this.cardIdInputControl.setValue('NO_CARD_ON_READER');
      this.cardIdInputControl.markAsPristine();
    } else if (this.currentCard.account !== null && this.currentCard.account.id !== this.account.id) {
      this.cardIdInputControl.setValue('CARD_ALREADY_USED');
      this.cardIdInputControl.markAsPristine();
    } else {
      this.cardIdInputControl.setValue(this.currentCard.id);
      this.cardIdInputControl.markAsDirty();
    }
  }

  chargeAccount(amount: number) {
    if (!this.account || !this.account.id) {
      return;
    }
    if (amount === 0) {
      this.selectedIndex++;
      return;
    }
    this.posService.currentPos$.pipe(
      switchMap(pos => {
        const charge: CreateChargeTransaction = {
          accountId: this.account.id,
          amount,
          comment: 'Charged account via wizard interface.',
          pointOfSaleId: pos.id
        };
        return this.transactionService.chargeAccount(charge);
      })
    ).subscribe(result => {
      this.account = result.account;
      this.selectedIndex++;
    }, error => {
      this.toastr.error('Couldn\'t charge account!', 'Error'),
      this.chargeAmountInputControl.setValue(0);
    });
  }

  storeNewAccount(accountInfo: AccountInfo) {
    this.accountService.save(accountInfo).subscribe(acc => {
        this.account = acc;
        this.selectedIndex = 1;
      },
      err => {
        if (err.status === StatusCodes.BAD_REQUEST) {
          console.log(err.error);
          this.accountFormValidationErrors = err.error.errors;
        }
      });
  }

  saveCard(cardId: string) {
    this.account.cardId = cardId;
    this.accountService.save(this.account).subscribe(result => {
      this.account = result;
      this.selectedIndex = 2;
    }, error => {
      this.toastr.error('Card couldn\'t be saved.', 'Error'),
      this.cardIdInputControl.setValue('ERROR_SAVING_CARD!!!');
    });
  }

  previousPage() {
    if (this.selectedIndex > 0) {
      this.selectedIndex--;
    }
  }

  restartWizard() {
    this.account = undefined;
    this.userResults.results = undefined;
    this.searchUserInputControl.setValue('');
    this.cardIdInputControl.setValue('');
    this.cardIdInputControl.markAsPristine();
    this.chargeAmountInputControl.setValue(0);
    this.selectedIndex = 0;
  }

  ngAfterViewInit(): void {
    this.stepper._getIndicatorType = () => 'number';
  }
}
