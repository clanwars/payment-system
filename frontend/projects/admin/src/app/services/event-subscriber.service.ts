import {Injectable} from '@angular/core';
import * as signalR from '@microsoft/signalr';
import {HubConnection} from '@microsoft/signalr';
import {Observable, Subject, takeWhile, tap} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {AuthService, SubscriptionEventClasses, SubscriptionNotification, SubscriptionNotificationPointOfSaleInfo} from 'payment-lib';

@Injectable({
  providedIn: 'root'
})
export class EventSubscriberService {
  private connection: HubConnection = null;
  private pointOfSaleSubject: Subject<SubscriptionNotificationPointOfSaleInfo> = new Subject<SubscriptionNotificationPointOfSaleInfo>();

  constructor(
    private http: HttpClient,
    private authService: AuthService
  ) {
    this.createSignalrClient();
  }

  private createSignalrClient() {
    this.authService.currentToken$
      .pipe(
        takeWhile(token => token === null, true),
        tap(console.warn)
      )
      .subscribe(async (token) => {
          if (token !== null && this.authService.hasRolePaymentAdmin()) {
            if (this.connection !== null) {
              await this.connection.stop();
              this.connection = null;
            }
            this.connection = new signalR.HubConnectionBuilder()
              .withUrl('/payment-hub?access_token=' + token)
              .build();
            this.connection.onclose(err => {
              // setTimeout(() => {
              //   console.warn('Signalr connection was lost. Trying to reestablish connection after 5 seconds...');
              //   this.createSignalrClient();
              // }, 5000);
            });
            this.startHubConnection();
          } else if (this.connection !== null) {
            this.connection.stop().then(() => this.connection = null);
          }
        }
      );
  }

  get pointOfSaleEvents(): Observable<SubscriptionNotificationPointOfSaleInfo> {
    return this.pointOfSaleSubject.asObservable();
  }

  private startHubConnection() {
    this.connection.on('NewEvent', (ev: SubscriptionNotification) => {
      console.log(ev);
      if (ev.eventClass === SubscriptionEventClasses.PointOfSaleInfo) {
        this.pointOfSaleSubject.next(ev);
      }
    });

    this.connection.start().then(value => {
      this.connection.invoke('SubscribeEvents').catch(err => console.error(err));
    }).catch(err => {
      // TODO: handle loginError
    });
  }
}
