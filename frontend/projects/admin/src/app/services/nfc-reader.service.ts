import {Injectable, OnDestroy} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {AuthService, CardInfo, uuid} from 'payment-lib';
import {BehaviorSubject, takeWhile, tap} from 'rxjs';
import * as signalR from '@microsoft/signalr';
import {HubConnection, HubConnectionState} from '@microsoft/signalr';

@Injectable()
export class NfcReaderService implements OnDestroy {
  private connection: HubConnection = null;
  private currentCardSubject: BehaviorSubject<CardInfo | null> = new BehaviorSubject<CardInfo | null>(null);
  currentCard$ = this.currentCardSubject.asObservable();
  private connectionStateSubject: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  connectionState$ = this.connectionStateSubject.asObservable();

  constructor(
    private http: HttpClient,
    private authService: AuthService
  ) {
    console.log('initializing nfcReaderService');
    this.authService.currentToken$.pipe(
      takeWhile(token => token === null, true),
      tap(console.warn)
    ).subscribe(async (token) => {
        if (token !== null) {
          if (this.authService.hasRolePaymentAdmin()) {
            if (this.connection !== null && this.connection.state === HubConnectionState.Connected) {
              await this.connection.stop();
              this.connectionStateSubject.next(false);
            }
            this.connection = new signalR.HubConnectionBuilder()
              .withUrl('/payment-hub?access_token=' + token)
              .build();

            this.connection.onclose(err => {
              // TODO: handle loginError
            });

            this.connection.on('NewCard', (ev: CardInfo) => {
              this.currentCardSubject.next(ev);
            });

            this.connection.on('RemovedCard', () => {
              this.currentCardSubject.next(null);
            });
          }
        } else if (this.connection !== null && this.connection.state === HubConnectionState.Connected) {
          await this.connection.stop();
          this.connectionStateSubject.next(false);
          this.connection = null;
        }
      }
    );
  }

  public async startHubConnection(posId: uuid) {
    if (this.connection === null) {
      return;
    } else if (this.connection.state === HubConnectionState.Connected) {
      await this.connection.stop();
    }
    this.connection.start().then(value => {
      this.connection.invoke('JoinPointOfSale', {
        type: 'CashDesk',
        posId
      }).then(_ => {
        this.connectionStateSubject.next(true);
      }).catch(err => {
        // TODO
        // if (this.toastService !== undefined) {
        //   this.toastService.danger(err, 'PoS Service Error', {duration: 15000});
        // }
        console.error(err);
      });
    });
  }

  async ngOnDestroy() {
    console.log('NFC reader service is about to be destroyed.');
    if (this.connection !== null && this.connection.state === HubConnectionState.Connected) {
      this.connectionStateSubject.next(false);
      await this.connection.stop();
      this.connection = null;
    }
  }
}
