import { TestBed } from '@angular/core/testing';

import { PointsOfSaleSelectedGuard } from './points-of-sale-selected.guard';

describe('PointsOfSaleSelectedGuard', () => {
  let guard: PointsOfSaleSelectedGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(PointsOfSaleSelectedGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
