import {Component, OnInit} from '@angular/core';
import {FormBuilder} from '@angular/forms';
import {ActivatedRoute} from '@angular/router';
import {PurchaseTransactionResponse, TransactionResponse} from 'payment-lib';

@Component({
  selector: 'app-orders-details',
  templateUrl: './transaction-details.component.html',
  styleUrls: ['./transaction-details.component.scss']
})
export class TransactionDetailsComponent implements OnInit {
  transactionResponse: TransactionResponse;

  constructor(private fb: FormBuilder, private route: ActivatedRoute) {
  }

  ngOnInit() {
    if (this.route.snapshot.data.pointOfSale === null) {
      this.transactionResponse = null;
      return;
    } else {
      this.transactionResponse = this.route.snapshot.data.transaction;
    }
  }

  asPurchaseTransactionResponse(transactionResponse: TransactionResponse): PurchaseTransactionResponse {
    return transactionResponse as PurchaseTransactionResponse;
  }
}
