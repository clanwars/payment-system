import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {ConfirmDialogService} from '../../layout/confirm-dialog/confirm-dialog.service';
import {merge, Observable} from 'rxjs';
import {MatPaginator, PageEvent} from '@angular/material/paginator';
import {MatSort, Sort} from '@angular/material/sort';
import {debounceTime, map, tap} from 'rxjs/operators';
import {FormControl} from '@angular/forms';
import {
  PagedDataSource,
  RelationalOperator,
  SortDirection,
  TransactionResponse,
  TransactionsFilter,
  TransactionsService
} from 'payment-lib';
import {faInfo} from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-orders-list',
  templateUrl: './transaction-list.component.html',
  styleUrls: ['./transaction-list.component.scss']
})
export class TransactionListComponent implements OnInit, AfterViewInit {
  faInfo = faInfo;

  columnsToDisplay = ['id', 'timestamp', 'username', 'sum', 'products', 'cancelled', 'actions'];
  filterRow = ['id-filter', 'timestamp-filter', 'username-filter', 'sum-filter', 'products-filter',
    'cancelled-filter', 'actions-filter'];

  dataSource: PagedDataSource<TransactionResponse, TransactionsFilter>;

  allFilters$: Observable<[keyof TransactionsFilter, RelationalOperator, string]>;
  usernameFilterValue = new FormControl('');

  usernameFilterValue$: Observable<['Account.Email', RelationalOperator, string]>;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(private transactionsService: TransactionsService, private confirmDialog: ConfirmDialogService) {
  }

  ngOnInit() {
    this.dataSource = new PagedDataSource(this.transactionsService.fetch, TransactionsFilter);
    this.dataSource.filter.setSort('timestamp', SortDirection.DESC);
    this.dataSource.loadData();
  }

  ngAfterViewInit() {
    // reset the paginator after sorting
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

    this.usernameFilterValue$ = this.usernameFilterValue.valueChanges.pipe(
      debounceTime(300),
      map(v => ['Account.Email', RelationalOperator.CONTAINS_CI, v])
    );

    this.allFilters$ = merge(this.usernameFilterValue$);

    merge(this.sort.sortChange, this.paginator.page, this.allFilters$)
      .pipe(
        tap((change: [keyof TransactionsFilter, RelationalOperator, string] | Sort | PageEvent) => this.loadTransactionPage(change))
      )
      .subscribe();
  }

  loadTransactionPage<K extends keyof TransactionsFilter>(change: [K, RelationalOperator, string] | Sort | PageEvent) {
    if (change instanceof Array) {
      const key = change[0] as K;
      const operator = change[1];
      const value = change[2];
      if (value === '') {
        this.dataSource.filter.clearFilter(key);
      } else {
        // @ts-ignore
        this.dataSource.filter.setFilter(key, operator, value);
      }
    }
    this.dataSource.filter.clearSort();
    // @ts-ignore
    this.dataSource.filter.setSort(this.sort.active, this.sort._direction);
    this.dataSource.filter.setPage(this.paginator.pageIndex + 1);
    this.dataSource.filter.setPageSize(this.paginator.pageSize);
    this.dataSource.loadData();
  }
}
