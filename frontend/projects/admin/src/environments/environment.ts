// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  defaultCurrency: 'EUR', // The ISO 4217 currency code, such as USD for the US dollar and EUR for the euro.
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related loginError stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an loginError is thrown.
 */
// import 'zone.js/plugins/zone-loginError';  // Included with Angular CLI.
