import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { CartEntryComponent } from './cart-entry.component';

describe('CartEntryComponent', () => {
  let component: CartEntryComponent;
  let fixture: ComponentFixture<CartEntryComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ CartEntryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CartEntryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
