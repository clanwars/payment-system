import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ChargeButtonComponent } from './charge-button.component';

describe('ChargeButtonComponent', () => {
  let component: ChargeButtonComponent;
  let fixture: ComponentFixture<ChargeButtonComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ChargeButtonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChargeButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
