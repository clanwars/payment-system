import {Component, OnInit, TemplateRef} from '@angular/core';
import {Observable} from 'rxjs';
import {NgbModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {CardInfo, SignalrClientService} from 'payment-lib';
import {ChargeService} from '../../services/charge-service/charge.service';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-charge-button',
  templateUrl: './charge-button.component.html',
  styleUrls: ['./charge-button.component.scss']
})
export class ChargeButtonComponent implements OnInit {

  currentCard: CardInfo | null;
  public currentCard$ = this.eventSubscriber.currentCard;
  public totalChargeAmount$: Observable<number> = this.chargeService.currentChargeAmount;

  public modalInstance: NgbModalRef;
  comments: string;

  constructor(private eventSubscriber: SignalrClientService,
              private chargeService: ChargeService,
              private modal: NgbModal,
              private toasts: ToastrService) {
  }

  ngOnInit() {
    this.currentCard$.subscribe(c => this.currentCard = c);
  }

  checkout(comment: string) {
    this.comments = null;
    this.chargeService.checkout(comment).subscribe(res => {
      this.toasts.success('Checkout successful.', 'Nice!', {timeOut: 10000});
    });
  }

  public async showChargeCheckoutConfirmationModal(modalRef: TemplateRef<any>) {
    this.modalInstance = this.modal.open(modalRef, {
      size: 'lg'
    });

    this.modalInstance.result.then((comments: string) => {
      this.checkout(comments);
    }, () => {
      this.toasts.warning('Checkout canceled!', 'Okay', {timeOut: 5000});
    });
  }

}
