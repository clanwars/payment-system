import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {ChargeSelectorComponent} from './charge-selector.component';

describe('ChargeSelectorComponent', () => {
  let component: ChargeSelectorComponent;
  let fixture: ComponentFixture<ChargeSelectorComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ChargeSelectorComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChargeSelectorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
