import {Component, Input, OnInit} from '@angular/core';
import {ChargeService} from '../../services/charge-service/charge.service';

@Component({
  selector: 'app-charge-selector',
  templateUrl: './charge-selector.component.html',
  styleUrls: ['./charge-selector.component.scss']
})
export class ChargeSelectorComponent implements OnInit {
  @Input() amount: number;

  public fontAwesomeCurrencySign: string;

  constructor(private chargeService: ChargeService) {
  }

  ngOnInit() {
  }

  changeAmount() {
    this.chargeService.changeChargeAmount(this.amount);
  }
}
