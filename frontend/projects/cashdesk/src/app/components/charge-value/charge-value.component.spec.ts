import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ChargeValueComponent } from './charge-value.component';

describe('ChargeValueComponent', () => {
  let component: ChargeValueComponent;
  let fixture: ComponentFixture<ChargeValueComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ChargeValueComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChargeValueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
