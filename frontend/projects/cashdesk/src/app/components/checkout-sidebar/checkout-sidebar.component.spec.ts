import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { CheckoutSidebarComponent } from './checkout-sidebar.component';

describe('CheckoutSidebarComponent', () => {
  let component: CheckoutSidebarComponent;
  let fixture: ComponentFixture<CheckoutSidebarComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ CheckoutSidebarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CheckoutSidebarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
