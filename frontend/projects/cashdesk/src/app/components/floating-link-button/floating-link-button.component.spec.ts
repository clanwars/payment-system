import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {FloatingLinkButtonComponent} from './floating-link-button.component';

describe('FloatingButtonComponent', () => {
  let component: FloatingLinkButtonComponent;
  let fixture: ComponentFixture<FloatingLinkButtonComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [FloatingLinkButtonComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FloatingLinkButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
