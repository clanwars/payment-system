import {Component, Input, OnInit} from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-floating-link-button',
  templateUrl: './floating-link-button.component.html',
  styleUrls: ['./floating-link-button.component.scss']
})

export class FloatingLinkButtonComponent implements OnInit {
  @Input() urlString: string;

  constructor(private router: Router) {
  }

  ngOnInit() {
  }

  public navigateTo(): void {
    this.router.navigate(['/' + this.urlString]);
  }
}
