import {Component, OnDestroy, OnInit, TemplateRef} from '@angular/core';
import {NgbModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {from, Subject} from 'rxjs';
import {map, switchMap, takeUntil, toArray} from 'rxjs/operators';
import {
  Filter,
  Product,
  ProductFilter,
  ProductsService,
  ProductType,
  ProductTypeFilter,
  ProductTypesService,
  RelationalOperator,
  SignalrClientService
} from 'payment-lib';
import {CartService} from '../../services/cart-service/cart.service';
import {ProductTypeSelectorService} from '../../services/product-type-selector.service';

@Component({
  selector: 'app-product-selector',
  templateUrl: './product-selector.component.html',
  styleUrls: ['./product-selector.component.scss']
})
export class ProductSelectorComponent implements OnInit, OnDestroy {
  productTypes: Array<[ProductType, Product[]]> = [];
  modalProduct: Product;
  discountPrice: number;
  modalPrice: number;
  currentIndex = 0;
  isFinalPrize = false;
  modalInstance: NgbModalRef;
  currentDiscount = 0;

  private onDestroy$ = new Subject<void>();

  constructor(private productsService: ProductsService,
              private cartService: CartService,
              private modal: NgbModal,
              private eventSubscriber: SignalrClientService,
              private productTypesService: ProductTypesService,
              private productTypeSelectorService: ProductTypeSelectorService
  ) {
  }

  ngOnInit() {
    const ptFilter = new Filter(ProductTypeFilter);
    ptFilter.setFilter('locked', RelationalOperator.EQUALS, false);
    this.productTypesService.getAllProductTypesFiltered(ptFilter).pipe(
      takeUntil(this.onDestroy$),
      switchMap(pts => from(pts)),
      map(pt => {
        return [pt, []];
      }),
      toArray(),
    ).subscribe((pts: Array<[ProductType, Product[]]>) => {
      pts.forEach((pt) => {
        const filter = new Filter(ProductFilter);
        filter.setFilter('productType.Name', RelationalOperator.EQUALS, pt[0].name);
        this.productsService.getAllProductsFiltered(filter).subscribe(ps => {
          pt[1].push(...ps);
        });
      });
      this.productTypes = pts;
    });

    this.eventSubscriber.currentDiscount.pipe(
      takeUntil(this.onDestroy$)
    ).subscribe(cd => this.currentDiscount = cd);

    this.productTypeSelectorService.currentProductType$.pipe(
      takeUntil(this.onDestroy$)
    ).subscribe(cpt => {
      this.currentIndex = this.productTypes.findIndex((pt => pt[0].id === cpt.id));
    });
  }

  addToCart(product: Product) {
    const productClone = Object.assign({}, product);
    productClone.price = productClone.price - productClone.price * this.currentDiscount;
    this.cartService.addToCart(productClone);
  }

  public openDiscountModal(modalRef: TemplateRef<any>, product: Product) {
    this.modalProduct = Object.assign({}, product);
    this.discountPrice = this.modalProduct.price;
    this.modalPrice = this.modalProduct.price;
    this.modalInstance = this.modal.open(modalRef, {
      centered: true,
    });
    this.modalInstance.result.then(() => {
      this.modalProduct.price = this.discountPrice;
      this.cartService.addToCart(this.modalProduct, true);
      this.isFinalPrize = false;
      setTimeout(() => {
        this.modalProduct = undefined;
      }, 500);
    }, () => {
      setTimeout(() => {
        this.modalProduct = undefined;
      }, 500);
    });
  }

  public setDiscountToSpecificAmount(amount: number) {
    this.isFinalPrize = true;
    this.discountPrice = amount;
    setTimeout(mi => mi.close(), 500, this.modalInstance);
  }

  ngOnDestroy(): void {
    this.onDestroy$.next();
    this.onDestroy$.complete();
  }
}
