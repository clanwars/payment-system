import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ProductTypeCardComponent } from './product-type-card.component';

describe('ProductTypeCardComponent', () => {
  let component: ProductTypeCardComponent;
  let fixture: ComponentFixture<ProductTypeCardComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductTypeCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductTypeCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
