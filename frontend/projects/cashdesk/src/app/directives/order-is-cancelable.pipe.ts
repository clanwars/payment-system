import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'orderIsCancelable'
})
export class OrderIsCancelablePipe implements PipeTransform {

  /**
   * @param value A string which can be parsed with new Date(...)
   */
  transform(value: string): boolean {
    const now = new Date();
    return (now.getTime() - (new Date(value)).getTime()) < 10 * 60 * 1000;
  }
}
