import {AfterViewChecked, Directive, ElementRef, HostListener, Input} from '@angular/core';

@Directive({
  selector: '[appUseWindowHeight]'
})
export class UseWindowHeightDirective implements AfterViewChecked {

  @Input() appUseWindowHeight: number;

  constructor(private el: ElementRef) {
  }

  ngAfterViewChecked() {
    this.resize();
  }

  @HostListener('window:resize')
  onResize() {
    this.resize();
  }

  resize() {
    const htmlElement: HTMLElement = this.el.nativeElement;
    htmlElement.style.height = `${window.innerHeight - this.appUseWindowHeight}px`;
  }
}
