import {inject, TestBed} from '@angular/core/testing';

import {IsUnlockedGuard} from './is-unlocked.guard';

describe('IsUnlockedGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [IsUnlockedGuard]
    });
  });

  it('should ...', inject([IsUnlockedGuard], (guard: IsUnlockedGuard) => {
    expect(guard).toBeTruthy();
  }));
});
