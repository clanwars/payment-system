import {inject, TestBed} from '@angular/core/testing';

import {LogoutGard} from './logout-gard.service';

describe('LogoutGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LogoutGard]
    });
  });

  it('should ...', inject([LogoutGard], (guard: LogoutGard) => {
    expect(guard).toBeTruthy();
  }));
});
