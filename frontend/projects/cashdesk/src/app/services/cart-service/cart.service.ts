import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable, of} from 'rxjs';
import {map, tap} from 'rxjs/operators';
import {CardInfo, CreatePurchaseTransaction, Product, PurchaseEntry, SignalrClientService, TransactionsService} from 'payment-lib';
import {ConfigurationService} from '../configuration.service';

@Injectable({
  providedIn: 'root'
})
export class CartService {
  public lastChangedItem: BehaviorSubject<Product> = new BehaviorSubject(null);

  private currentCard: CardInfo;
  private cartEntries: Product[] = [];
  private currentCartSubject: BehaviorSubject<Product[]> = new BehaviorSubject<Product[]>([]);

  constructor(private configService: ConfigurationService,
              private cashdeskHubService: SignalrClientService,
              private transactionService: TransactionsService) {
    this.cashdeskHubService.currentCard.subscribe(c => this.currentCard = c);

    this.cashdeskHubService.currentDiscount.subscribe(discount => {
      for (const p of this.cartEntries) {
        if (!p.priceFixed) {
          const x = Math.floor(p.defaultPrice * 100);
          const d = Math.floor(x * discount);
          p.price = (x - d) / 100;
        }
      }
      this.currentCartSubject.next(this.cartEntries);
    });
  }

  get currentCart(): Observable<Product[]> {
    return this.currentCartSubject.asObservable();
  }

  get totalCheckoutSum(): Observable<number> {
    return this.currentCart.pipe(
      map(products => {
        return products.length === 0 ? 0 : products.map(p => p.price).reduce((acc: number, currentValue: number) => acc + currentValue);
      })
    );
  }

  public addToCart(entry: Product, priceFixed = false) {
    entry.priceFixed = priceFixed;

    this.lastChangedItem.next(entry);
    this.cartEntries.push(entry);
    this.currentCartSubject.next(this.cartEntries);
  }

  public removeFromCart(index: number) {
    this.lastChangedItem.next(this.cartEntries[index]);
    this.cartEntries.splice(index, 1);
    this.currentCartSubject.next(this.cartEntries);
  }

  public clearCart() {
    this.lastChangedItem.next(null);
    this.cartEntries.length = 0;
    this.currentCartSubject.next(this.cartEntries);
  }

  public checkout(comment: string): Observable<boolean> {
    if (!this.currentCard
      || this.currentCard.account.locked) {
      return of(false);
    }
    const createPurchaseTransaction: CreatePurchaseTransaction = {
      accountId: this.currentCard.account.id,
      comment,
      pointOfSaleId: this.configService.pointOfSaleId,
      entries: this.cartEntries.map<PurchaseEntry>(ce => {
        return {productId: ce.id, price: ce.price};
      })
    };
    return this.transactionService.purchase(createPurchaseTransaction).pipe(
      tap(cardInfo => {
        this.currentCard = cardInfo;
        this.clearCart();
      }),
      map(cardInfo => {
        return !!cardInfo;
      })
    );
  }
}
