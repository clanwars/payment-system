import {Injectable, OnDestroy} from '@angular/core';
import {Subject} from 'rxjs';
import {uuid} from 'payment-lib';

@Injectable({
  providedIn: 'root'
})
export class ConfigurationService implements OnDestroy {
  private onDestroy$ = new Subject<void>();

  constructor() {
  }

  get pointOfSaleId(): string | null {
    return localStorage.getItem('posId');
  }

  set pointOfSaleId(posId: uuid) {
    localStorage.setItem('posId', posId);
  }

  ngOnDestroy(): void {
    this.onDestroy$.next();
    this.onDestroy$.complete();
  }
}
