import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ApplicationOfflineScreenComponent } from './application-offline-screen.component';

describe('ApplicationOfflineScreenComponent', () => {
  let component: ApplicationOfflineScreenComponent;
  let fixture: ComponentFixture<ApplicationOfflineScreenComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ApplicationOfflineScreenComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ApplicationOfflineScreenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
