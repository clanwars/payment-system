import {Component, OnDestroy, OnInit} from '@angular/core';
import {OnlineStatusService} from '../../../../../payment-lib/src/lib/services/online-status/online-status.service';
import {Subject} from 'rxjs';
import {filter, takeUntil, tap} from 'rxjs/operators';
import {Router} from '@angular/router';

@Component({
  selector: 'app-application-offline-screen',
  templateUrl: './application-offline-screen.component.html',
  styleUrls: ['./application-offline-screen.component.scss']
})
export class ApplicationOfflineScreenComponent implements OnInit, OnDestroy {
  private onDestroy$ = new Subject<void>();

  constructor(
    private router: Router,
    private onlineStatus: OnlineStatusService
  ) {
  }

  ngOnInit(): void {
    this.onlineStatus.browserIsOnline$.pipe(
      takeUntil(this.onDestroy$),
      filter(onlineStatus => onlineStatus),
      tap(() => this.router.navigate(['/']))
    ).subscribe();
  }

  ngOnDestroy(): void {
    this.onDestroy$.next();
    this.onDestroy$.complete();
  }

}
