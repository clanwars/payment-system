import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { CashdeskscreenComponent } from './cashdeskscreen.component';

describe('CashdeskscreenComponent', () => {
  let component: CashdeskscreenComponent;
  let fixture: ComponentFixture<CashdeskscreenComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ CashdeskscreenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CashdeskscreenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
