import {Component, OnDestroy, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {Observable, Subject, tap} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {Filter, ProductType, ProductTypeFilter, ProductTypesService, RelationalOperator, SignalrClientService} from 'payment-lib';
import {ProductTypeSelectorService} from '../../services/product-type-selector.service';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-cashdeskscreen',
  templateUrl: './cashdeskscreen.component.html',
  styleUrls: ['./cashdeskscreen.component.scss']
})
export class CashdeskscreenComponent implements OnInit, OnDestroy {
  productTypes$: Observable<ProductType[]>;
  currentProductType$: Observable<ProductType>;

  private onDestroy$ = new Subject<void>();

  constructor(
    private router: Router,
    private hub: SignalrClientService,
    private productTypesService: ProductTypesService,
    private productTypeSelectorService: ProductTypeSelectorService,
    private toastr: ToastrService
  ) {
  }

  ngOnInit() {
    const ptFilter = new Filter(ProductTypeFilter);
    ptFilter.setFilter('locked', RelationalOperator.EQUALS, false);
    this.productTypes$ = this.productTypesService.getAllProductTypesFiltered(ptFilter);
    this.currentProductType$ = this.productTypeSelectorService.currentProductType$;

    this.hub.connectionState$.pipe(
      takeUntil(this.onDestroy$),
      tap(state => {
        if (state === false) {
          this.toastr.error('Couldnt join Point of Sale! Maybe PoS ID is not configured properly or PoS is already in use.', 'Error');
          this.router.navigate(['/']);
        }
      })
    ).subscribe();
  }

  selectProductType(productType: ProductType) {
    this.productTypeSelectorService.jump(productType);
  }

  ngOnDestroy(): void {
    this.onDestroy$.next();
    this.onDestroy$.complete();
  }
}
