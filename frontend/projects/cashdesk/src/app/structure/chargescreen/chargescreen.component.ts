import {Component, OnInit} from '@angular/core';
import {CardInfo, SignalrClientService} from 'payment-lib';
import {map} from 'rxjs/operators';

@Component({
  selector: 'app-chargescreen',
  templateUrl: './chargescreen.component.html',
  styleUrls: ['./chargescreen.component.scss']
})
export class ChargescreenComponent implements OnInit {
  public currentCard$ = this.eventSubscriber.currentCard;

  public currentAccountBalance$ = this.currentCard$.pipe(
    map((c: CardInfo) => {
      if (c === null) {
        return 0;
      }
      if (c.account === null) {
        return 0;
      }
      return c.account.balance;
    })
  );

  constructor(private eventSubscriber: SignalrClientService) {
  }

  ngOnInit() {
  }
}
