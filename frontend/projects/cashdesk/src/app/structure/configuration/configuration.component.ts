import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {uuid} from 'payment-lib';
import {ConfigurationService} from '../../services/configuration.service';

@Component({
  selector: 'app-configuration',
  templateUrl: './configuration.component.html',
  styleUrls: ['./configuration.component.scss']
})
export class ConfigurationComponent implements OnInit {
  posId: uuid;

  constructor(private configurationService: ConfigurationService, private router: Router) {
  }

  ngOnInit() {
    this.posId = this.configurationService.pointOfSaleId;
  }

  save() {
    this.configurationService.pointOfSaleId = this.posId;
    this.router.navigate(['/cashdesk']);
  }
}
