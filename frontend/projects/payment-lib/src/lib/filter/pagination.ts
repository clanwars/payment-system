export class Pagination {
  public page = 1;
  public pageSize = 15;

  constructor(page: number, pageSize: number) {
    this.page = page;
    this.pageSize = pageSize;
  }
}
