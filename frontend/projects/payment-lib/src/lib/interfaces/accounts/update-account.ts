export interface UpdateAccount {
  email: string;
  firstName: string;
  lastName: string;
  cardId: string;
  locked: boolean;
  overdraft: number;
  discount: number;
}
