import {AccountInfo} from '../accounts/account-info';

export interface CardInfo {
  id: string;
  account: AccountInfo | null;
}
