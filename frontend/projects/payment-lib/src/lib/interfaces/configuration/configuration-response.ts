import {ConfigurationAuthResponse} from './configuration-auth-response';

export interface ConfigurationResponse {
  auth: ConfigurationAuthResponse;
}
