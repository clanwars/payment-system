export interface LoginResponse {
  token: string | null;
}
