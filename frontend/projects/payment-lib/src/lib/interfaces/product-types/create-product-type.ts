export interface CreateProductType {
  name: string;
  order: number;
  locked: boolean;
  image: string;
}
