export interface UpdateProductType {
  name: string;
  order: number;
  locked: boolean;
  image: string;
}
