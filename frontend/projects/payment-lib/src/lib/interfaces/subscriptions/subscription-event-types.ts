export enum SubscriptionEventTypes {
  PosAdded = 'PosAdded',
  PosConfigured = 'PosConfigured',
  PosDeviceLeft = 'PosDeviceLeft',
  PosDeviceJoined = 'PosDeviceJoined',
}
