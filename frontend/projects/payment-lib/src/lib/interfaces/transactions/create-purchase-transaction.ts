import {PurchaseEntry} from './purchase-entry';
import {uuid} from '../../types/uuid';

export interface CreatePurchaseTransaction {
  entries: Array<PurchaseEntry>;
  accountId: uuid;
  pointOfSaleId: uuid;
  comment: string;
}
