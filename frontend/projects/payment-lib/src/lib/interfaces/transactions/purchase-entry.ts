import {uuid} from '../../types/uuid';

export interface PurchaseEntry {
  productId: uuid;
  price: number;
}
