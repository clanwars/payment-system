import {Product} from '../products/product';
import {uuid} from '../../types/uuid';

export interface PurchaseTransactionEntry {
  id: uuid;
  productId: uuid;
  product: Product;
  price: number;
  purchaseTransactionId: uuid;
}
