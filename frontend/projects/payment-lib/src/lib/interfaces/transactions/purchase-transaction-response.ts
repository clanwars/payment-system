import {AccountInfo} from '../accounts/account-info';
import {PointOfSaleInfo} from '../point-of-sales/point-of-sale-info';
import {PurchaseTransactionEntry} from './purchase-transaction-entry';
import {Product} from '../products/product';
import {uuid} from '../../types/uuid';

export interface PurchaseTransactionResponse {
  id: uuid;
  account: AccountInfo;
  pointOfSale: PointOfSaleInfo;
  sum: number;
  timestamp: string;
  cancelled: string | null;
  comment: string;
  entries: PurchaseTransactionEntry[];
  entriesGrouped?: { product: Product, price: number, amount: number }[];
  transactionType: 'PurchaseTransaction';
}
