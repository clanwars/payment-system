import {Inject, LOCALE_ID, Pipe, PipeTransform} from '@angular/core';
import {TimeAgoRanges} from './time-ago-ranges';
import {formatDate} from '@angular/common';

@Pipe({
  name: 'timeAgo'
})
export class TimeAgoPipe implements PipeTransform {

  constructor(@Inject(LOCALE_ID) private locale: string) {
  }

  transform(value: Date | string): string {
    const date = new Date(value);
    const now = new Date();

    let range = TimeAgoRanges.longer;

    const diffInSeconds = (now.getTime() - date.getTime()) / 1000;
    if (diffInSeconds < 60) {
      range = TimeAgoRanges.seconds;
    } else if (diffInSeconds < 60 * 60) {
      range = TimeAgoRanges.minutes;
    } else if (diffInSeconds < 60 * 60 * 24) {
      range = TimeAgoRanges.hours;
    } else if (diffInSeconds < 60 * 60 * 24 * 7) {
      range = TimeAgoRanges.days;
    }

    let output: string;

    switch (range) {
      case TimeAgoRanges.seconds:
        // @ts-ignore
        output = $localize`:@@pl.timeAgo.seconds:some seconds ago`;
        break;
      case TimeAgoRanges.minutes:
        // @ts-ignore
        output = $localize`:@@pl.timeAgo.minutes:some minutes ago`;
        break;
      case TimeAgoRanges.hours:
        // @ts-ignore
        output = $localize`:@@pl.timeAgo.hours:some hours ago`;
        break;
      case TimeAgoRanges.days:
        // @ts-ignore
        output = $localize`:@@pl.timeAgo.days:some days ago`;
        break;
      default:
        output = formatDate(date, 'short', this.locale);
    }

    return output;
  }

}
