import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {AccountFilter} from './account-filter';
import {UpdateAccount} from '../../interfaces/accounts/update-account';
import {CreateAccount} from '../../interfaces/accounts/create-account';
import {Filter} from '../../filter/filter';
import {PagedResult} from '../paged-result';
import {AbstractApiService} from '../abstract-api.service';
import {uuid} from '../../types/uuid';
import {AccountInfo} from '../../interfaces/accounts/account-info';
import {RelationalOperator} from '../../filter/relational-operator';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class AccountsService extends AbstractApiService<AccountInfo> {
  baseUrl = '/api/v1/Accounts';

  constructor(public http: HttpClient) {
    super();
  }

  save(accountInfo: AccountInfo): Observable<AccountInfo> {
    let $acc: Observable<AccountInfo>;
    if (accountInfo.id) {
      const account: UpdateAccount = {
        email: accountInfo.email,
        firstName: accountInfo.firstName,
        lastName: accountInfo.lastName,
        cardId: accountInfo.cardId,
        locked: accountInfo.locked,
        overdraft: accountInfo.overdraft,
        discount: accountInfo.discount,
      };
      $acc = this.updateAccount(accountInfo.id, account);
    } else {
      const account: CreateAccount = {
        email: accountInfo.email,
        firstName: accountInfo.firstName,
        lastName: accountInfo.lastName,
        cardId: accountInfo.cardId,
        overdraft: accountInfo.overdraft,
        discount: accountInfo.discount,
      };
      $acc = this.createAccount(account);
    }
    return $acc;
  }

  getAccountInfoById(id: uuid): Observable<AccountInfo | null> {
    const filter = new Filter(AccountFilter, 1, 1);
    filter.setFilter('id', RelationalOperator.EQUALS, id);
    return this.getFiltered(this.baseUrl, filter).pipe(
      map(r => {
        if (r.rowCount !== 1) {
          throw new Error('AccountInfo with id ' + id + ' not found');
        } else {
          return r;
        }
      }),
      map(pagedResult => pagedResult.results.shift())
    );
  }

  // +----------------------------------------------------------------------------+
  // |                    START Backend REST APIs                                 |
  // +----------------------------------------------------------------------------+

  public fetch = (filter: Filter<AccountFilter>) => this.getFilteredAccounts(filter);

  getFilteredAccounts(filter: Filter<AccountFilter>): Observable<PagedResult<AccountInfo>> {
    return this.getFiltered(this.baseUrl, filter);
  }

  createAccount(createMessage: CreateAccount): Observable<AccountInfo> {
    return this.postGeneric<CreateAccount, AccountInfo>(this.baseUrl, createMessage);
  }

  updateAccount(accountId: uuid, updateMessage: UpdateAccount): Observable<AccountInfo> {
    return this.putGeneric<UpdateAccount, AccountInfo>(this.baseUrl + '/' + accountId, updateMessage);
  }

  deleteAccount(accountId: uuid): Observable<null> {
    return this.deleteGeneric<null>(this.baseUrl + '/' + accountId);
  }

  lockAccount(accountId: uuid): Observable<null> {
    return this.patchGeneric(this.baseUrl + '/' + accountId + '/lock');
  }

  unlockAccount(accountId: uuid): Observable<null> {
    return this.patchGeneric(this.baseUrl + '/' + accountId + '/unlock');
  }

  // +----------------------------------------------------------------------------+
  // |                    END Backend REST APIs                                 |
  // +----------------------------------------------------------------------------+
}
