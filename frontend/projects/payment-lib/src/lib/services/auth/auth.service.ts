import {Injectable, OnDestroy} from '@angular/core';
import {BehaviorSubject, distinctUntilChanged, Observable, of, ReplaySubject, Subject, takeUntil, tap} from 'rxjs';
import {AuthConfig, OAuthService} from 'angular-oauth2-oidc';
import {switchMap} from 'rxjs/operators';
import {JwtHelperService} from '@auth0/angular-jwt';
import {HttpClient} from '@angular/common/http';
import {ConfigurationResponse} from '../../interfaces/configuration/configuration-response';

@Injectable({
  providedIn: 'root'
})
export class AuthService implements OnDestroy {
  private isLoggedIn = false;
  private currentLoginStateSubject: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  private initialLoginSequenceFinishedSubject: ReplaySubject<boolean> = new ReplaySubject<boolean>(1);
  private destroy$ = new Subject();

  private authCodeFlowConfig: AuthConfig = {
    // Url of the Identity Provider
    issuer: '', // is set in constructor after injecting lib config
    requireHttps: false,
    // URL of the SPA to redirect the user to after login
    redirectUri: '',
    postLogoutRedirectUri: '',
    redirectUriAsPostLogoutRedirectUriFallback: false,
    // The SPA's id. The SPA is registerd with this id at the auth-server
    clientId: '',
    responseType: 'code',
    // Important: Request offline_access to get a refresh token
    scope: 'openid profile email offline_access',
    showDebugInformation: true,
  };

  private tokenChanged: BehaviorSubject<string | null> = new BehaviorSubject<string | null>(null);
  public currentToken$ = this.tokenChanged.pipe(distinctUntilChanged());

  constructor(
    private http: HttpClient,
    private oauthService: OAuthService
  ) {
    this.http.get<ConfigurationResponse>('/api/v1/.configuration/frontend').pipe(
      takeUntil(this.destroy$),
      tap((c) => this.startOauthService(c.auth.idpUrl, c.auth.clientId))
    ).subscribe();
  }

  private startOauthService(oAuth2IdpUrl: string, clientId: string) {
    this.authCodeFlowConfig.issuer = oAuth2IdpUrl;
    this.authCodeFlowConfig.clientId = clientId;
    this.authCodeFlowConfig.redirectUri = window.location.origin + window.location.pathname;
    this.authCodeFlowConfig.postLogoutRedirectUri = window.location.origin + window.location.pathname;
    this.oauthService.configure(this.authCodeFlowConfig);
    this.oauthService.events.subscribe(ev => {
      console.log(ev.type);
      if (ev.type === 'token_received' || ev.type === 'discovery_document_loaded') {
        this.tokenChanged.next(this.oauthService.getAccessToken());
      }
    });

    this.oauthService.loadDiscoveryDocumentAndTryLogin().then(() => {
      if (this.oauthService.hasValidAccessToken()) {
        console.log(this.oauthService.getIdentityClaims());
        console.log(this.oauthService.getGrantedScopes());
        console.log(this.oauthService.getAccessToken());
        this.setLoginState(true);
      } else {
        this.setLoginState(false);
      }
      this.initialLoginSequenceFinishedSubject.next(true);
    });
  }

  get isLoggedIn$(): Observable<boolean> {
    return this.initialLoginSequenceFinishedSubject.asObservable().pipe(
      switchMap(() => this.currentLoginStateSubject.asObservable())
    );
  }

  private isAuthenticated(): boolean {
    return (
      this.oauthService.hasValidAccessToken()
    );
  }

  ngOnDestroy(): void {
    this.destroy$.next(null);
    this.destroy$.complete();
  }

  currentRoles(): string[] {
    const helper = new JwtHelperService();
    const decodedToken = helper.decodeToken(this.oauthService.getAccessToken());
    return decodedToken.roles;
  }

  login() {
    this.authCodeFlowConfig.redirectUri = window.location.origin + window.location.pathname;
    this.oauthService.initLoginFlow();
  }

  logout() {
    this.authCodeFlowConfig.postLogoutRedirectUri = window.location.origin + window.location.pathname;
    return of(this.oauthService.revokeTokenAndLogout());
  }

  private setLoginState(loginState: boolean) {
    if (loginState) {
      this.oauthService.setupAutomaticSilentRefresh();
    } else {
      this.oauthService.stopAutomaticRefresh();
    }
    this.isLoggedIn = loginState;
    this.currentLoginStateSubject.next(this.isLoggedIn);
  }

  hasRoleCashDesk() {
    return this.currentRoles().includes('CashDesk');
  }

  hasRolePaymentAdmin() {
    return this.currentRoles().includes('Admin');
  }
}
