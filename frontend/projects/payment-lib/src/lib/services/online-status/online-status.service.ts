import {Injectable} from '@angular/core';
import {fromEvent, merge, of} from 'rxjs';
import {map, shareReplay} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class OnlineStatusService {
  public browserIsOnline$ = merge(
    of(null),
    fromEvent(window, 'online'),
    fromEvent(window, 'offline')
  ).pipe(
    map(() => navigator.onLine),
    shareReplay(1)
  );

  constructor() {
  }
}
