import {AbstractFilter} from '../../filter/abstract-filter';
import {RelationalOperator} from '../../filter/relational-operator';
import {uuid} from '../../types/uuid';

export class ProductFilter extends AbstractFilter {
  public id: Map<RelationalOperator, uuid | uuid[]> = new Map<RelationalOperator, uuid | uuid[]>();
  public name: Map<RelationalOperator, string> = new Map<RelationalOperator, string>();
  public price: Map<RelationalOperator, number | number[]> = new Map<RelationalOperator, number | number[]>();
  public description: Map<RelationalOperator, string> = new Map<RelationalOperator, string>();
  public order: Map<RelationalOperator, number | number[]> = new Map<RelationalOperator, number | number[]>();
  public locked: Map<RelationalOperator, boolean> = new Map<RelationalOperator, boolean>();
  public 'productType.Name': Map<RelationalOperator, string> = new Map<RelationalOperator, string>();
  public 'productType.Locked': Map<RelationalOperator, boolean> = new Map<RelationalOperator, boolean>();
}
