import {Injectable} from '@angular/core';
import * as signalR from '@microsoft/signalr';
import {HubConnection} from '@microsoft/signalr';
import {BehaviorSubject, Observable, Subject} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {map, take, tap} from 'rxjs/operators';
import {JoinPointOfSale} from '../../interfaces/point-of-sales/join-point-of-sale';
import {uuid} from '../../types/uuid';
import {CardInfo} from '../../interfaces/cards/card-info';
import {PosClientType} from '../../interfaces/point-of-sales/pos-client-type';
import {PointOfSaleInfo} from '../../interfaces/point-of-sales/point-of-sale-info';
import {PointsOfSaleService} from '../point-of-sale/points-of-sale.service';
import {AuthService} from '../auth/auth.service';

@Injectable({
  providedIn: 'root'
})
export class SignalrClientService {
  private connectionChanged = new Subject<boolean>();
  connectionState$ = this.connectionChanged.asObservable();

  private connection: HubConnection = null;
  private currentCardSubject: BehaviorSubject<CardInfo | null> = new BehaviorSubject<CardInfo | null>(null);
  // private nfcClientError: ToastMessage = {
  //   header: 'Card reader loginError',
  //   autoHide: null,
  //   body: 'There is no NFC Card Reader connected to this Point of Sale!',
  //   classname: 'bg-warning text-dark'
  // };

  constructor(
    private http: HttpClient,
    private authService: AuthService,
    private pointsOfSaleService: PointsOfSaleService
  ) {
    authService.currentToken$.subscribe(async t => {
      if (t === null) {
        await this.connection.stop();
        this.connection = null;
      }
      if (this.connection === null) {
        this.startConnection(t);
      }
    });
  }

  get currentCard(): Observable<CardInfo | null> {
    return this.currentCardSubject.asObservable();
  }

  get currentDiscount(): Observable<number> {
    return this.currentCardSubject.asObservable().pipe(
      map(cc => {
        if (cc !== null && cc.account !== null) {
          return cc.account.discount;
        } else {
          return 0;
        }
      })
    );
  }

  public joinPos(pos: uuid, posClientType: PosClientType) {
    this.connection.invoke('JoinPointOfSale', {
      type: posClientType,
      posId: pos
    }).catch(err => console.error(err));
  }

  private startConnection(token: string) {
    this.connection = new signalR.HubConnectionBuilder()
      .withUrl('/payment-hub?access_token=' + token)
      .withAutomaticReconnect()
      .build();

    this.connection.start().then().catch(err => {
      this.connectionChanged.next(false);
    });

    this.connection.onreconnecting(err => {
      this.connectionChanged.next(false);
    });

    this.connection.onreconnected(err => {
      this.connectionChanged.next(true);
    });

    this.connection.onclose(err => {
      this.connectionChanged.next(false);
    });

    this.connection.on('JoinRequired', () => {
      this.pointsOfSaleService.currentPos$.pipe(
        take(1),
        tap(posInfo => {
          if (posInfo === null) {
            this.connection.stop();
          } else {
            this.joinPos(posInfo.id, PosClientType.CashDesk);
          }
        })
      ).subscribe();
    });

    this.connection.on('NewCard', (ev: CardInfo) => {
      this.currentCardSubject.next(ev);
    });

    this.connection.on('RemovedCard', () => {
      this.currentCardSubject.next(null);
    });

    this.connection.on('Welcome', (newPosInfo: PointOfSaleInfo) => {
      this.currentCardSubject.next(null);
      this.connectionChanged.next(true);
      // if (newPosInfo.hasNfcClient === false) {
      //   this.toastService.show(this.nfcClientError);
      // } else {
      //   this.toastService.remove(this.nfcClientError);
      // }
    });

    this.connection.on('NewCardReader', (newPosInfo: PointOfSaleInfo) => {
      this.currentCardSubject.next(null);
      if (newPosInfo.hasNfcClient === true) {
        // this.toastService.remove(this.nfcClientError);
      }
    });

    this.connection.on('DeviceLeftPos', (deviceLeftMessage) => {
      this.currentCardSubject.next(null);
      if (deviceLeftMessage.type === 'NfcReader') {
        // this.toastService.show(this.nfcClientError);
      }
    });
  }
}
