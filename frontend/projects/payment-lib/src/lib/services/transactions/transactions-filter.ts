import {AbstractFilter} from '../../filter/abstract-filter';
import {RelationalOperator} from '../../filter/relational-operator';

export class TransactionsFilter extends AbstractFilter {
  public id: Map<RelationalOperator, string | string[]> = new Map<RelationalOperator, string | string[]>();
  public 'Account.Email': Map<RelationalOperator, string> = new Map<RelationalOperator, string>();
  public 'Account.Id': Map<RelationalOperator, string> = new Map<RelationalOperator, string>();
  public timestamp: Map<RelationalOperator, Date> = new Map<RelationalOperator, Date>();
  public sum: Map<RelationalOperator, number> = new Map<RelationalOperator, number>();
  public 'PointOfSale.Name': Map<RelationalOperator, string> = new Map<RelationalOperator, string>();
  public 'isCancelled': Map<RelationalOperator, boolean> = new Map<RelationalOperator, boolean>();
}
