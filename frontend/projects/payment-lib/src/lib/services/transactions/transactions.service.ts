import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {EMPTY, Observable} from 'rxjs';
import {concatMap, expand, map, toArray} from 'rxjs/operators';
import {PagedResult} from '../paged-result';
import {AbstractApiService} from '../abstract-api.service';
import {Filter} from '../../filter/filter';
import {RelationalOperator} from '../../filter/relational-operator';
import {TransactionsFilter} from './transactions-filter';
import {TransactionResponse} from '../../interfaces/transactions/transaction-response';
import {CardInfo} from '../../interfaces/cards/card-info';
import {CreateChargeTransaction} from '../../interfaces/transactions/create-charge-transaction';
import {CreatePurchaseTransaction} from '../../interfaces/transactions/create-purchase-transaction';
import {uuid} from '../../types/uuid';

@Injectable({
  providedIn: 'root'
})
export class TransactionsService extends AbstractApiService<TransactionResponse> {
  baseUrl = '/api/v1/Transactions';

  constructor(public http: HttpClient) {
    super();
  }

  getFilteredTransactions(filter: Filter<TransactionsFilter>): Observable<PagedResult<TransactionResponse>> {
    return this.getFiltered(this.baseUrl, filter);
  }

  public fetch = (filter: Filter<TransactionsFilter>) => this.getFilteredTransactions(filter);

  getTransactionById(id: uuid): Observable<TransactionResponse | null> {
    const filter = new Filter(TransactionsFilter, 1, 1);
    filter.setFilter('id', RelationalOperator.EQUALS, id);
    return this.getFiltered(this.baseUrl, filter).pipe(
      map(r => {
        if (r.rowCount !== 1) {
          throw new Error('Transaction with id ' + id + ' not found');
        } else {
          return r;
        }
      }),
      map(pagedResult => pagedResult.results.shift())
    );
  }

  chargeAccount(chargeTransaction: CreateChargeTransaction): Observable<CardInfo> {
    return this.postGeneric<CreateChargeTransaction, CardInfo>(this.baseUrl + '/Charge', chargeTransaction);
  }

  purchase(purchaseTransaction: CreatePurchaseTransaction): Observable<CardInfo> {
    return this.postGeneric<CreatePurchaseTransaction, CardInfo>(this.baseUrl + '/Purchase', purchaseTransaction);
  }

  cancelTransaction(trans: TransactionResponse) {
    return this.deleteGeneric(this.baseUrl + '/' + trans.id);
  }

  getAllTransactionsFiltered(filter: Filter<TransactionsFilter>): Observable<TransactionResponse[]> {
    filter.setPage(1);
    return this.getFilteredTransactions(filter).pipe(
      expand(page => {
        if (page.currentPage < page.pageCount) {
          filter.setPage(page.currentPage + 1);
          return this.getFilteredTransactions(filter);
        } else {
          return EMPTY;
        }
      }),
      concatMap(responses => responses.results),
      toArray()
    );
  }
}
