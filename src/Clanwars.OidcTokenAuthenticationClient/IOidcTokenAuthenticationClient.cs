namespace Clanwars.OidcTokenAuthenticationClient;

public interface IOidcTokenAuthenticationClient
{
    Task AddAuthorizationHeaderToHttpRequestAsync(HttpRequestMessage request);
    Task<string> GetCurrentJwtTokenAsync();
}
