namespace Clanwars.OidcTokenAuthenticationClient;

public class OidcOptions
{
    public Uri TokenEndpoint { get; set; } = default!;
    public string ClientId { get; set; } = default!;
    public string ClientSecret { get; set; } = default!;
}
