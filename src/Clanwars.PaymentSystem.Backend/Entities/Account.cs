using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Sieve.Attributes;

namespace Clanwars.PaymentSystem.Backend.Entities;

[Table("Accounts")]
public class Account
{
    [Key]
    [Sieve(CanFilter = true, CanSort = true)]
    public Guid Id { get; set; }

    [Required]
    [EmailAddress]
    [Sieve(CanFilter = true, CanSort = true)]
    public string Email { get; set; }

    [Required]
    [Sieve(CanFilter = true, CanSort = true)]
    public string FirstName { get; set; }

    [Required]
    [Sieve(CanFilter = true, CanSort = true)]
    public string LastName { get; set; }

    [Sieve(CanFilter = true, CanSort = true)]
    public string CardId { get; set; }

    [Required]
    [Sieve(CanFilter = true, CanSort = true)]
    public decimal Balance { get; set; }

    [Sieve(CanFilter = true, CanSort = true)]
    public bool Locked { get; set; }

    [Sieve(CanFilter = true, CanSort = true)]
    [Required]
    public int Overdraft { get; set; }

    [Required]
    public float Discount { get; set; }
}
