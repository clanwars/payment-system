using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Sieve.Attributes;

namespace Clanwars.PaymentSystem.Backend.Entities;

[Table("PointsOfSale")]
public class PointOfSale
{
    [Key]
    [Sieve(CanFilter = true, CanSort = true)]
    public Guid Id { get; set; }

    [Required]
    [MinLength(3)]
    [Sieve(CanFilter = true, CanSort = true)]
    public string Name { get; set; }

    public IList<Transaction> Transactions { get; set; }
}
