using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Sieve.Attributes;

namespace Clanwars.PaymentSystem.Backend.Entities;

[Table("Products")]
public class Product
{
    [Key]
    [Sieve(CanFilter = true, CanSort = true)]
    public Guid Id { get; set; }

    [Required]
    [Sieve(CanFilter = true, CanSort = true)]
    public string Name { get; set; }

    [Required]
    [Sieve(CanFilter = true, CanSort = true)]
    public decimal Price { get; set; }

    [Sieve(CanFilter = true, CanSort = true)]
    public string Description { get; set; }

    public ProductType ProductType { get; set; }

    [Required]
    public Guid ProductTypeId { get; set; }

    public string Image { get; set; }

    [Sieve(CanFilter = true, CanSort = true)]
    public int Order { get; set; }
        
    [Sieve(CanFilter = true, CanSort = true)]
    public bool Locked { get; set; }
}