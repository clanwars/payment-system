using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Sieve.Attributes;

namespace Clanwars.PaymentSystem.Backend.Entities;

[Table("ProductTypes")]
public class ProductType
{
    [Key]
    [Sieve(CanFilter = true, CanSort = true)]
    public Guid Id { get; set; }

    [Required]
    [Sieve(CanFilter = true, CanSort = true)]
    public string Name { get; set; }

    [Sieve(CanFilter = true, CanSort = true)]
    public int Order { get; set; }

    public string Image { get; set; }

    [Sieve(CanFilter = true, CanSort = true)]
    public bool Locked { get; set; }
}