using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Clanwars.PaymentSystem.Backend.Entities;

[Table("PurchaseTransactions")]
public class PurchaseTransaction : Transaction
{
    public List<PurchaseTransactionEntry> Entries { get; set; }
}