using System;

namespace Clanwars.PaymentSystem.Backend.Exceptions;

public class CashDeskClientAlreadyRegisteredForPointOfSaleException : Exception
{
    public CashDeskClientAlreadyRegisteredForPointOfSaleException()
    {
    }

    public CashDeskClientAlreadyRegisteredForPointOfSaleException(string message) : base(message)
    {
    }

    public CashDeskClientAlreadyRegisteredForPointOfSaleException(string message, Exception inner) : base(message, inner)
    {
    }
}
