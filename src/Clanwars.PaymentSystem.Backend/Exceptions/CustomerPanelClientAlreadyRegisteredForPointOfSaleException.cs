using System;

namespace Clanwars.PaymentSystem.Backend.Exceptions;

public class CustomerPanelClientAlreadyRegisteredForPointOfSaleException : Exception
{
    public CustomerPanelClientAlreadyRegisteredForPointOfSaleException()
    {
    }

    public CustomerPanelClientAlreadyRegisteredForPointOfSaleException(string message) : base(message)
    {
    }

    public CustomerPanelClientAlreadyRegisteredForPointOfSaleException(string message, Exception inner) : base(message, inner)
    {
    }
}
