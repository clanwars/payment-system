using System;

namespace Clanwars.PaymentSystem.Backend.Exceptions;

public class NfcClientAlreadyRegisteredForPointOfSaleException : Exception
{
    public NfcClientAlreadyRegisteredForPointOfSaleException()
    {
    }

    public NfcClientAlreadyRegisteredForPointOfSaleException(string message) : base(message)
    {
    }

    public NfcClientAlreadyRegisteredForPointOfSaleException(string message, Exception inner) : base(message, inner)
    {
    }
}
