using System.Net;
using Clanwars.PaymentSystem.Lib;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace Clanwars.PaymentSystem.Backend.Filters;

public class PagedResultResponseFilter : IActionFilter
{
    public void OnActionExecuting(ActionExecutingContext context)
    {
        // Method intentionally left empty.
    }

    public void OnActionExecuted(ActionExecutedContext context)
    {
        if (context.HttpContext.Request.Method != WebRequestMethods.Http.Get ||
            context.Result is not OkObjectResult okObjectResult ||
            okObjectResult.Value is null ||
            !okObjectResult.Value.GetType().IsGenericType ||
            okObjectResult.Value.GetType().GetGenericTypeDefinition() != typeof(PagedResult<>)
           )
        {
            return;
        }

        var pr = okObjectResult.Value;
        var prType = pr.GetType();

        var totalCount = prType.GetProperty(nameof(PagedResult<object>.TotalCount))!.GetValue(pr, null)!;
        var currentPage = prType.GetProperty(nameof(PagedResult<object>.CurrentPage))!.GetValue(pr, null)!;
        var pageCount = prType.GetProperty(nameof(PagedResult<object>.PageCount))!.GetValue(pr, null)!;
        var pageSize = prType.GetProperty(nameof(PagedResult<object>.PageSize))!.GetValue(pr, null)!;
        var results = prType.GetProperty(nameof(PagedResult<object>.Results))!.GetValue(pr, null)!;

        context.HttpContext.Response.Headers.Add("X-Total-Count", totalCount.ToString());
        context.HttpContext.Response.Headers.Add("X-Current-Page", currentPage.ToString());
        context.HttpContext.Response.Headers.Add("X-Page-Count", pageCount.ToString());
        context.HttpContext.Response.Headers.Add("X-Page-Size", pageSize.ToString());
        okObjectResult.Value = results;
    }
}
