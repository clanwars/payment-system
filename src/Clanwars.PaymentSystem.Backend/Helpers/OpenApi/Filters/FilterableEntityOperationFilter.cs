using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Clanwars.PaymentSystem.Backend.Helpers.OpenApi.Attributes;
using Microsoft.OpenApi.Models;
using Sieve.Attributes;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace Clanwars.PaymentSystem.Backend.Helpers.OpenApi.Filters;

public class FilterableEntityOperationFilter : IOperationFilter
{
    public void Apply(OpenApiOperation operation, OperationFilterContext context)
    {
        var filterableEntityAttribute = context
            .MethodInfo
            .GetCustomAttributes(true)
            .OfType<FilterableEntityAttribute>()
            .FirstOrDefault();

        if (filterableEntityAttribute == null)
        {
            return;
        }

        var filterParameter = operation.Parameters.FirstOrDefault(p => p.Name == "Filters");
        if (filterParameter == null)
        {
            return;
        }

        var filterParamDescriptionPrefix = (filterParameter.Description ?? "") + " Possible filters: ";

        var sortParameter = operation.Parameters.FirstOrDefault(p => p.Name == "Sorts");
        var sortParamDescriptionPrefix = (sortParameter!.Description ?? "") + " Possible sorts: ";

        var types = AppDomain.CurrentDomain.GetAssemblies()
            .SelectMany(s => s.GetTypes())
            .Where(p => filterableEntityAttribute.EntityType.IsAssignableFrom(p))
            .ToList();

        var filterParamList = new List<string>();
        var sortParamList = new List<string>();

        types.ForEach(t =>
        {
            t.GetProperties().ToList().ForEach(prop =>
            {
                var sieveAttr = prop.GetCustomAttributes(true).OfType<SieveAttribute>().ToList();
                sieveAttr.ForEach(s =>
                {
                    if (s.CanFilter)
                    {
                        filterParamList.Add(prop.Name);
                    }

                    if (s.CanSort)
                    {
                        sortParamList.Add(prop.Name);
                    }
                });
            });
        });

        if (filterableEntityAttribute.EntityType.IsDefined(typeof(AdditionalFilterableEntitiesAttribute), true))
        {
            var attribute = (AdditionalFilterableEntitiesAttribute) filterableEntityAttribute.EntityType.GetCustomAttribute(typeof(AdditionalFilterableEntitiesAttribute))!;
            filterParamList.AddRange(attribute.AdditionFilterableEntities);
        }

        if (filterableEntityAttribute.EntityType.IsDefined(typeof(AdditionalSortableEntitiesAttribute), true))
        {
            var attribute = (AdditionalSortableEntitiesAttribute) filterableEntityAttribute.EntityType.GetCustomAttribute(typeof(AdditionalSortableEntitiesAttribute))!;
            sortParamList.AddRange(attribute.AdditionSortableEntities);
        }

        filterParameter.Description = filterParamDescriptionPrefix + string.Join(", ", filterParamList);
        sortParameter.Description = sortParamDescriptionPrefix + string.Join(", ", sortParamList);
    }
}
