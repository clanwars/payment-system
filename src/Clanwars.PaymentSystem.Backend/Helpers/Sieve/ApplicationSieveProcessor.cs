using Clanwars.PaymentSystem.Backend.Entities;
using Microsoft.Extensions.Options;
using Sieve.Models;
using Sieve.Services;

namespace Clanwars.PaymentSystem.Backend.Helpers.Sieve;

public class ApplicationSieveProcessor : SieveProcessor
{
    public ApplicationSieveProcessor(IOptions<SieveOptions> options, ISieveCustomSortMethods customSortMethods,
        ISieveCustomFilterMethods customFilterMethods)
        : base(options, customSortMethods, customFilterMethods)
    {
    }

    protected override SievePropertyMapper MapProperties(SievePropertyMapper mapper)
    {
        mapper.Property<Product>(p => p.ProductType.Name)
            .CanFilter()
            .CanSort();

        mapper.Property<Product>(p => p.ProductType.Locked)
            .CanFilter()
            .CanSort();

        mapper.Property<Transaction>(t => t.PointOfSale.Name)
            .CanFilter()
            .CanSort();

        mapper.Property<Transaction>(t => t.Account.Id)
            .CanFilter()
            .CanSort();

        mapper.Property<Transaction>(t => t.Account.Email)
            .CanFilter()
            .CanSort();

        mapper.Property<Transaction>(t => t.Account.FirstName)
            .CanFilter()
            .CanSort();

        mapper.Property<Transaction>(t => t.Account.LastName)
            .CanFilter()
            .CanSort();

        return mapper;
    }
}
