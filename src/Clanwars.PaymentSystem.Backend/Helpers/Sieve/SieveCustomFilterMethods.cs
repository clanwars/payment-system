using System.Linq;
using Clanwars.PaymentSystem.Backend.Entities;
using Sieve.Services;

namespace Clanwars.PaymentSystem.Backend.Helpers.Sieve;

public class SieveCustomFilterMethods : ISieveCustomFilterMethods
{
    public IQueryable<Transaction> IsCancelled(IQueryable<Transaction> source, string op, string[] values)
    {
        return values[0] == "true" ? source.Where(t => t.Cancelled != null) : source.Where(t => t.Cancelled == null);
    }
}
