using System.Threading.Tasks;
using Clanwars.PaymentSystem.Lib.Messages.Account;
using Clanwars.PaymentSystem.Lib.Messages.Hub;
using Clanwars.PaymentSystem.Lib.Messages.PointOfSale;
using Microsoft.AspNetCore.Mvc;

namespace Clanwars.PaymentSystem.Backend.Hubs;

public interface IPaymentHub
{
    Task NewCardReader(PointOfSaleInfo pointOfSale);
    Task NewCashDesk(PointOfSaleInfo pointOfSale);
    Task NewCustomerPanel(PointOfSaleInfo pointOfSale);
    Task NewEventSubscriber();
    Task JoinRequired(string message);
    Task Welcome(PointOfSaleInfo pointOfSale);
    Task Error(ProblemDetails problemDetails);
    Task DeviceLeftPos(PointOfSaleDevice pointOfSaleDeviceMessage);
    Task NewCard(CardInfo cardInfo);
    Task RemovedCard();
    Task GetCard();
    Task Error(string message);
    Task NewEvent(SubscriptionNotification notificationPointOfSaleInfo);
}