using System;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Clanwars.PaymentSystem.Backend.Exceptions;
using Clanwars.PaymentSystem.Backend.Repositories;
using Clanwars.PaymentSystem.Backend.Services;
using Clanwars.PaymentSystem.Lib.Enums;
using Clanwars.PaymentSystem.Lib.Messages.Account;
using Clanwars.PaymentSystem.Lib.Messages.Hub;
using Clanwars.PaymentSystem.Lib.Messages.PointOfSale;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;
using Sieve.Models;

namespace Clanwars.PaymentSystem.Backend.Hubs;

[Authorize]
public class PaymentHub : Hub<IPaymentHub>
{
    private readonly ILogger<PaymentHub> _logger;
    private readonly IMapper _mapper;
    private readonly IAccountRepository _accountRepository;
    private readonly IConnectionService _connectionService;
    private readonly IPointOfSaleService _pointOfSaleService;

    public PaymentHub(
        IAccountRepository accountRepository,
        IMapper mapper,
        ILogger<PaymentHub> logger,
        IConnectionService connectionService,
        IPointOfSaleService pointOfSaleService
    )
    {
        _accountRepository = accountRepository;
        _logger = logger;
        _connectionService = connectionService;
        _pointOfSaleService = pointOfSaleService;
        _mapper = mapper;
    }

    [Authorize(Roles = "NfcClient,CashDesk")]
    public async Task JoinPointOfSale(PointOfSaleDevice device)
    {
        _logger.LogInformation("Client tries to connect to PoS {PosId} as {PosType}", device.PosId, device.Type);
        var pos = await _pointOfSaleService.GetPointOfSaleByIdAsync(device.PosId);
        if (pos == null)
        {
            _logger.LogError("The PointOfSale {DevicePosId} a client tries to connect to is unknown!", device.PosId);
            var e = new ProblemDetails
            {
                Type = "signalr/join-point-of-sale",
                Title = "PoS unknown",
                Detail = "The Point of Sale you are trying to join is unknown!",
                Status = StatusCodes.Status400BadRequest
            };
            await Clients.Caller.Error(e);
            return;
        }

        try
        {
            if (!_connectionService.SetConnection(device.PosId, device.Type, Context.ConnectionId))
            {
                _logger.LogError("A client wants to join as {DeviceType} but is already joined!", device.Type);
                return;
            }

            await Groups.AddToGroupAsync(Context.ConnectionId, device.PosId.ToString());

            var newPosInfo = await _pointOfSaleService.GetPointOfSaleByIdAsync(device.PosId);

            await Clients.Caller.Welcome(newPosInfo);

            switch (device.Type)
            {
                case PosClientType.NfcReader:
                    await Clients.OthersInGroup(device.PosId.ToString()).NewCardReader(newPosInfo);
                    break;
                case PosClientType.CashDesk:
                    await Clients.OthersInGroup(device.PosId.ToString()).NewCashDesk(newPosInfo);
                    var nfcConnection = _connectionService.GetConnection(device.PosId, PosClientType.NfcReader);
                    if (nfcConnection != null)
                    {
                        await Clients.Client(nfcConnection).GetCard();
                    }

                    break;
                case PosClientType.CustomerPanel:
                    await Clients.OthersInGroup(device.PosId.ToString()).NewCustomerPanel(newPosInfo);
                    break;
                default:
                    _logger.LogError("A client wants to connect as {DeviceType} which is not allowed", device.Type);
                    return;
            }

            await Clients.Group("EventSubscriber").NewEvent(
                new SubscriptionNotificationPointOfSaleInfo
                {
                    EventClass = SubscriptionEventClasses.PointOfSaleInfo,
                    EventType = SubscriptionEventTypes.PosDeviceJoined,
                    Content = newPosInfo
                });

            await LogPosStatesAsync();
        }
        catch (NfcClientAlreadyRegisteredForPointOfSaleException e)
        {
            _logger.LogError(e, "Another NFC client is already registered for this PointOfSale");
            throw new HubException("Another NFC client is already registered for this PointOfSale.", e);
        }
        catch (CashDeskClientAlreadyRegisteredForPointOfSaleException e)
        {
            _logger.LogError(e, "Another CashDesk client is already registered for this PointOfSale");
            throw new HubException("Another CashDesk client is already registered for this PointOfSale.", e);
        }
    }

    [Authorize(Roles = "Admin")]
    public async Task SubscribeEvents()
    {
        _logger.LogInformation("Admin is connecting as EventSubscriber");

        await Groups.AddToGroupAsync(Context.ConnectionId, "EventSubscriber");
        await Clients.Group("EventSubscriber").NewEventSubscriber();
    }

    [Authorize(Roles = "NfcClient")]
    public async Task NewCard(Card card)
    {
        var account = await _accountRepository.GetAccountInformationByCardIdAsync(card.Id);
        var cardInfo = new CardInfo
        {
            Id = card.Id,
        };
        if (account != null)
        {
            var accountInfo = _mapper.Map<AccountInfo>(account);
            cardInfo.Account = accountInfo;
        }

        _logger.LogInformation("New card placed: {CardInfo}", cardInfo);

        await Clients.Group(_connectionService.GetPointOfSaleId(Context.ConnectionId)
            .ToString()).NewCard(cardInfo);
    }

    [Authorize(Roles = "NfcClient")]
    public async Task CurrentCard(Card card)
    {
        _logger.LogDebug("Currently placed card is: {Card}", card);
        CardInfo cardInfo = null;
        var posIdNfcClient = _connectionService.GetPointOfSaleId(Context.ConnectionId);
        // TODO posIdNfcClient might be null!
        var account = await _accountRepository.GetAccountInformationByCardIdAsync(card.Id);

        var accountInfo = _mapper.Map<AccountInfo>(account);
        cardInfo = new CardInfo
        {
            Id = card.Id,
            Account = accountInfo
        };

        if (account == null)
        {
            if (card.Id != null)
            {
                _logger.LogWarning("CurrentCard: The card placed is not associated with any account!");
            }

            await Clients.OthersInGroup(posIdNfcClient.ToString()).RemovedCard();
        }
        else
        {
            await Clients.OthersInGroup(posIdNfcClient.ToString()).NewCard(cardInfo);
        }

        _connectionService.PublishResponseFromNfc(Context.ConnectionId, cardInfo);
    }

    [Authorize(Roles = "NfcClient")]
    public async Task RemovedCard()
    {
        _logger.LogDebug("Card was removed");
        await Clients
            .OthersInGroup(_connectionService.GetPointOfSaleId(Context.ConnectionId).ToString())
            .RemovedCard();
    }

    public override async Task OnConnectedAsync()
    {
        await Clients.Caller.JoinRequired("Connected to PaymentBackend. Please join a PointOfSale!");
        await base.OnConnectedAsync();
        await LogPosStatesAsync();
    }

    public override async Task OnDisconnectedAsync(Exception exception)
    {
        var valueTuple = _connectionService.RemoveConnection(Context.ConnectionId);
        if (valueTuple.HasValue)
        {
            await Clients.OthersInGroup(valueTuple.ToString()).DeviceLeftPos(new PointOfSaleDevice
            {
                PosId = valueTuple.Value.posId,
                Type = valueTuple.Value.posClientType
            });
            await Groups.RemoveFromGroupAsync(Context.ConnectionId, valueTuple.Value.posId.ToString());

            await Clients.Group("EventSubscriber").NewEvent(
                new SubscriptionNotificationPointOfSaleInfo
                {
                    EventClass = SubscriptionEventClasses.PointOfSaleInfo,
                    EventType = SubscriptionEventTypes.PosDeviceLeft,
                    Content = await _pointOfSaleService.GetPointOfSaleByIdAsync(valueTuple.Value.posId)
                });
        }
        else
        {
            // seems to be an GeneralSubscriber, which is not registered in _connectionService
            await Groups.RemoveFromGroupAsync(Context.ConnectionId, "EventSubscriber");
        }

        await base.OnDisconnectedAsync(exception);
        await LogPosStatesAsync();
    }

    private async Task LogPosStatesAsync()
    {
        var countPosReady = (await _pointOfSaleService.GetPointsOfSaleAsync(new SieveModel {PageSize = int.MaxValue}))
            .Results
            .Count(pos => pos.IsReady);
        var countPosUnready = (await _pointOfSaleService.GetPointsOfSaleAsync(new SieveModel {PageSize = int.MaxValue}))
            .Results
            .Count(pos => !pos.IsReady);
        _logger.LogInformation("Currently fully available PoS: {CountPosReady}", countPosReady);
        _logger.LogInformation("Currently uncompleted PoS: {CountPosUnready}", countPosUnready);
    }
}
