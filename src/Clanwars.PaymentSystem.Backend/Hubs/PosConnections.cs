#nullable enable
namespace Clanwars.PaymentSystem.Backend.Hubs;

public class PosConnections
{
    public string? Cashdesk { get; set; }
    public string? NfcClient { get; set; }
    public string? CustomerPanel { get; set; }

    public bool RequiredConnectionsReady()
    {
        return Cashdesk != null && NfcClient != null;
    }
}