﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Clanwars.PaymentSystem.Backend.Migrations
{
    public partial class SplitAccountColumnToEmailFirstAndLastName : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Username",
                table: "Accounts",
                newName: "Email");

            migrationBuilder.RenameIndex(
                name: "IX_Accounts_Username",
                table: "Accounts",
                newName: "IX_Accounts_Email");

            migrationBuilder.AddColumn<string>(
                name: "FirstName",
                table: "Accounts",
                type: "longtext",
                nullable: false,
                collation: "utf8mb4_swedish_ci");

            migrationBuilder.AddColumn<string>(
                name: "LastName",
                table: "Accounts",
                type: "longtext",
                nullable: false,
                collation: "utf8mb4_swedish_ci");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FirstName",
                table: "Accounts");

            migrationBuilder.DropColumn(
                name: "LastName",
                table: "Accounts");

            migrationBuilder.RenameColumn(
                name: "Email",
                table: "Accounts",
                newName: "Username");

            migrationBuilder.RenameIndex(
                name: "IX_Accounts_Email",
                table: "Accounts",
                newName: "IX_Accounts_Username");
        }
    }
}
