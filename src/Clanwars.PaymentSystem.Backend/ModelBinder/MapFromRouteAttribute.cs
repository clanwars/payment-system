using System;

namespace Clanwars.PaymentSystem.Backend.ModelBinder;

[AttributeUsage(AttributeTargets.Property)]
public class MapFromRouteAttribute : Attribute
{
}
