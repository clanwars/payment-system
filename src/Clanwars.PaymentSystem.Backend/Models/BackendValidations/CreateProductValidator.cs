using System;
using System.Linq;
using Clanwars.PaymentSystem.Backend.Repositories;
using Clanwars.PaymentSystem.Lib.Messages.Product;
using FluentValidation;

namespace Clanwars.PaymentSystem.Backend.Models.BackendValidations;

public class CreateProductValidator : AbstractValidator<CreateProductMessage>
{
    private readonly IProductRepository _productRepository;
    private readonly IProductTypeRepository _productTypeRepository;

    public CreateProductValidator(
        IProductRepository productRepository,
        IProductTypeRepository productTypeRepository
    )
    {
        _productRepository = productRepository;
        _productTypeRepository = productTypeRepository;

        RuleFor(cpm => cpm.Name)
            .Must(BeUniqueProductName())
            .WithMessage("A product with the name '{PropertyValue}' already exists.");

        RuleFor(upr => upr.ProductTypeId)
            .Must(BeValidProductType())
            .WithMessage("The product type '{PropertyValue}' does not exist.");
    }

    private Func<string, bool> BeUniqueProductName()
    {
        return productName =>
        {
            var product = _productRepository
                .GetProducts()
                .FirstOrDefault(product => product.Name == productName);
            return product is null;
        };
    }

    private Func<Guid, bool> BeValidProductType()
    {
        return productTypeId =>
        {
            var productType = _productTypeRepository
                .GetProductTypes()
                .FirstOrDefault(pt => pt.Id.Equals(productTypeId));
            return productType is not null;
        };
    }
}
