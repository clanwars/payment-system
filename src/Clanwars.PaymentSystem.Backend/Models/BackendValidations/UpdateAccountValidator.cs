using System;
using System.Linq;
using Clanwars.PaymentSystem.Backend.Repositories;
using FluentValidation;

namespace Clanwars.PaymentSystem.Backend.Models.BackendValidations;

public class UpdateAccountValidator : AbstractValidator<UpdateAccountRequest>
{
    private readonly IAccountRepository _accountRepository;

    public UpdateAccountValidator(
        IAccountRepository accountRepository
    )
    {
        _accountRepository = accountRepository;

        RuleFor(uar => uar.Email)
            .Must(BeUniqueUsername())
            .WithMessage("The username must be unique");

        When(cam => cam.CardId != null, () =>
        {
            RuleFor(uar => uar.CardId)
                .Must(BeUniqueCardId())
                .WithMessage("The cardId must be unique");
        });
    }

    private Func<UpdateAccountRequest, string, bool> BeUniqueUsername()
    {
        return (aur, username) =>
        {
            var account = _accountRepository
                .GetAccounts()
                .Where(a => a.Id != aur.AccountId)
                .FirstOrDefault(a => a.Email == username);
            return account is null;
        };
    }

    private Func<UpdateAccountRequest, string, bool> BeUniqueCardId()
    {
        return (aur, cardId) =>
        {
            var account = _accountRepository
                .GetAccounts()
                .Where(a => a.Id != aur.AccountId)
                .FirstOrDefault(a => a.CardId == cardId);
            return account is null;
        };
    }
}
