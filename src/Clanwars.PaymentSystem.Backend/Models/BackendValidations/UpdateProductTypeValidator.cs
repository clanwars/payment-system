using System;
using System.Linq;
using Clanwars.PaymentSystem.Backend.Repositories;
using FluentValidation;

namespace Clanwars.PaymentSystem.Backend.Models.BackendValidations;

public class UpdateProductTypeValidator : AbstractValidator<UpdateProductTypeRequest>
{
    private readonly IProductTypeRepository _productTypeRepository;

    public UpdateProductTypeValidator(
        IProductTypeRepository productTypeRepository
    )
    {
        _productTypeRepository = productTypeRepository;

        RuleFor(uptr => uptr.Name)
            .Must(BeUniqueProductTypeName())
            .WithMessage("A product type with the name '{PropertyValue}' already exists.");
    }

    private Func<UpdateProductTypeRequest, string, bool> BeUniqueProductTypeName()
    {
        return (message, productTypeName) =>
        {
            var productType = _productTypeRepository
                .GetProductTypes()
                .Where(pt => pt.Id != message.ProductTypeId)
                .FirstOrDefault(pt => pt.Name == productTypeName);
            return productType is null;
        };
    }
}
