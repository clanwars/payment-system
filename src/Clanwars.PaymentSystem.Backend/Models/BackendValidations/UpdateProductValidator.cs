using System;
using System.Linq;
using Clanwars.PaymentSystem.Backend.Repositories;
using FluentValidation;

namespace Clanwars.PaymentSystem.Backend.Models.BackendValidations;

public class UpdateProductValidator : AbstractValidator<UpdateProductRequest>
{
    private readonly IProductRepository _productRepository;
    private readonly IProductTypeRepository _productTypeRepository;

    public UpdateProductValidator(
        IProductRepository productRepository,
        IProductTypeRepository productTypeRepository
    )
    {
        _productRepository = productRepository;
        _productTypeRepository = productTypeRepository;

        RuleFor(upr => upr.Name)
            .Must(BeUniqueProductName())
            .WithMessage("A product with the name '{PropertyValue}' already exists.");

        RuleFor(upr => upr.ProductTypeId)
            .Must(BeValidProductType())
            .WithMessage("The product type '{PropertyValue}' does not exist.");
    }

    private Func<UpdateProductRequest, string, bool> BeUniqueProductName()
    {
        return (message, productName) =>
        {
            var product = _productRepository
                .GetProducts()
                .Where(product => product.Id != message.ProductId)
                .FirstOrDefault(product => product.Name == productName);
            return product is null;
        };
    }

    private Func<Guid?, bool> BeValidProductType()
    {
        return productTypeId =>
        {
            if (productTypeId is null)
            {
                return true;
            }

            var productType = _productTypeRepository
                .GetProductTypes()
                .FirstOrDefault(pt => pt.Id.Equals(productTypeId));
            return productType is not null;
        };
    }
}
