using System.Linq;
using AutoMapper;
using Clanwars.PaymentSystem.Backend.Entities;
using Clanwars.PaymentSystem.Lib.Messages;
using Clanwars.PaymentSystem.Lib.Messages.Account;

namespace Clanwars.PaymentSystem.Backend.Models.Mappings;

public class AccountMappingProfile : Profile
{
    public AccountMappingProfile()
    {
        CreateMap<Account, CardInfo>()
            .ForMember(dest => dest.Id,
                opts => opts.MapFrom(src => src.CardId))
            .ForMember(dest => dest.Account,
                opts => opts.MapFrom(src => src));

        CreateMap<Account, AccountInfo>();

        CreateMap<CreateAccountMessage, Account>()
            .ForMember(dest => dest.Id, opts => opts.Ignore())
            .ForMember(dest => dest.Balance, opts => opts.Ignore());

        CreateMap<UpdateAccountRequest, Account>()
            .ForMember(dest => dest.Id, opts => opts.Ignore())
            .ForMember(dest => dest.Balance, opts => opts.Ignore());
    }
}
