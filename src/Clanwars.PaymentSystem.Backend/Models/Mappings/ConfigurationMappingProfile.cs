using AutoMapper;
using Clanwars.PaymentSystem.Backend.Options;
using Clanwars.PaymentSystem.Lib.Messages.Configuration;

namespace Clanwars.PaymentSystem.Backend.Models.Mappings;

public class ConfigurationMappingProfile : Profile
{
    public ConfigurationMappingProfile()
    {
        CreateMap<AuthOptions, ConfigurationAuthResponse>()
            .ForMember(dst => dst.ClientId, opt => opt.MapFrom(src => src.FrontendClientId))
            .ForMember(dst => dst.IdpUrl, opt => opt.MapFrom(src => src.RealmDomain))
            ;
    }
}
