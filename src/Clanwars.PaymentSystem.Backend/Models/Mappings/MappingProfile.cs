using AutoMapper;
using Clanwars.PaymentSystem.Lib;

namespace Clanwars.PaymentSystem.Backend.Models.Mappings;

public class MappingProfile : Profile
{
    public MappingProfile()
    {
        CreateMap(typeof(PagedResult<>), typeof(PagedResult<>));
    }
}
