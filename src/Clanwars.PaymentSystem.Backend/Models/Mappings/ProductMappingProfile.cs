using AutoMapper;
using Clanwars.PaymentSystem.Backend.Entities;
using Clanwars.PaymentSystem.Lib.Messages.Product;

namespace Clanwars.PaymentSystem.Backend.Models.Mappings;

public class ProductMappingProfile : Profile
{
    public ProductMappingProfile()
    {
        CreateMap<Product, ProductResponse>()
            .ForMember(dest => dest.Image, opts =>
            {
                opts.Condition(p => !string.IsNullOrWhiteSpace(p.Image));
                opts.MapFrom(p => $"/api/v1/Products/{p.Id}/image");
            });

        CreateMap<CreateProductMessage, Product>()
            .ForMember(dest => dest.Id, opts => opts.Ignore())
            .ForMember(dest => dest.ProductType, opts => opts.Ignore());

        CreateMap<UpdateProductMessage, Product>()
            .ForAllMembers(o => o.Condition((src, dst, value) => { return value != null; }));
    }
}
