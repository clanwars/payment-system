using AutoMapper;
using Clanwars.PaymentSystem.Backend.Entities;
using Clanwars.PaymentSystem.Lib.Messages.ProductType;

namespace Clanwars.PaymentSystem.Backend.Models.Mappings;

public class ProductTypeMappingProfile : Profile
{
    public ProductTypeMappingProfile()
    {
        CreateMap<ProductType, ProductTypeResponse>()
            .ForMember(dest => dest.Image, opts =>
            {
                opts.Condition(p => !string.IsNullOrWhiteSpace(p.Image));
                opts.MapFrom(p => $"/api/v1/ProductTypes/{p.Id}/image");
            });

        CreateMap<CreateProductTypeMessage, ProductType>()
            .ForMember(dest => dest.Id, opts => opts.Ignore());

        CreateMap<UpdateProductTypeMessage, ProductType>()
            .ForMember(dest => dest.Id, opts => opts.Ignore());
    }
}
