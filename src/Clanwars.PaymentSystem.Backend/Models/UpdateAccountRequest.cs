using System;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using Clanwars.PaymentSystem.Backend.ModelBinder;
using Clanwars.PaymentSystem.Lib.Messages.Account;

namespace Clanwars.PaymentSystem.Backend.Models;

public record UpdateAccountRequest : UpdateAccountMessage
{
    [JsonIgnore]
    [MapFromRoute]
    [Required]
    public Guid AccountId { get; set; }
}
