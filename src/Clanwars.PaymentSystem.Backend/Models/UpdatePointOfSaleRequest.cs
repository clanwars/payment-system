using System;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using Clanwars.PaymentSystem.Backend.ModelBinder;
using Clanwars.PaymentSystem.Lib.Messages.PointOfSale;

namespace Clanwars.PaymentSystem.Backend.Models;

public record UpdatePointOfSaleRequest : UpdatePointOfSaleMessage
{
    [JsonIgnore]
    [MapFromRoute]
    [Required]
    public Guid PosId { get; set; }
}
