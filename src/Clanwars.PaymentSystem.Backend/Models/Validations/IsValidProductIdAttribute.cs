using System;
using System.ComponentModel.DataAnnotations;
using Clanwars.PaymentSystem.Backend.Repositories;

namespace Clanwars.PaymentSystem.Backend.Models.Validations;

public class IsValidProductIdAttribute : ValidationAttribute
{
    protected override ValidationResult IsValid(object value, ValidationContext validationContext)
    {
        var productId = (Guid) value;
        var productRepository = (IProductRepository) validationContext.GetService(typeof(IProductRepository));
        var product = productRepository?.GetProductByIdAsync(productId).Result;
        return product == null ? new ValidationResult(GetErrorMessage(productId)) : ValidationResult.Success;
    }

    private static string GetErrorMessage(Guid productId)
    {
        return $"An product with the id {productId} does not exist.";
    }
}
