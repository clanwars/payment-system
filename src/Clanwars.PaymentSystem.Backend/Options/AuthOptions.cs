namespace Clanwars.PaymentSystem.Backend.Options;

public class AuthOptions
{
    public const string OptionsKey = "Auth";
    public string RealmDomain { get; set; }
    public string IssuerDomain { get; set; }

    public string FrontendClientId { get; set; }
}
