using System;
using System.Linq;
using System.Threading.Tasks;
using Clanwars.PaymentSystem.Backend.Entities;
using Clanwars.PaymentSystem.Lib;
using Sieve.Models;

namespace Clanwars.PaymentSystem.Backend.Repositories;

public interface IAccountRepository
{
    IQueryable<Account> GetAccounts();
    Task<PagedResult<Account>> GetAccountsAsync(SieveModel sieveModel);
    Task<Account> GetAccountByEmailAsync(string email);
    Task<Account> GetAccountInformationByIdAsync(Guid accountId);
    Task<Account> GetAccountInformationByCardIdAsync(string cardId);
    Task CreateAccountAsync(Account account);
    Task UpdateAccountAsync(Account account);
    Task DeleteAccountAsync(Account account);
}
