using System;
using System.Linq;
using System.Threading.Tasks;
using Clanwars.PaymentSystem.Backend.Entities;
using Clanwars.PaymentSystem.Lib;
using Clanwars.PaymentSystem.Lib.Messages.PointOfSale;
using Sieve.Models;

namespace Clanwars.PaymentSystem.Backend.Repositories;

public interface IPointOfSaleRepository
{
    IQueryable<PointOfSale> GetPointsOfSale();

    Task<PagedResult<PointOfSale>> GetPointsOfSaleAsync(SieveModel sieveModel);
    Task<PointOfSale> GetPointOfSaleByIdAsync(Guid posId);
    Task<PointOfSale> GetPointOfSaleByNameAsync(string posName);
    Task CreatePointOfSaleAsync(PointOfSale newPointOfSale);
    Task UpdatePointOfSaleAsync(PointOfSale updatePointOfSale);
}
