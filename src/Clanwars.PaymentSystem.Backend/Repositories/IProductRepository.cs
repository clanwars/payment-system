using System;
using System.Linq;
using System.Threading.Tasks;
using Clanwars.PaymentSystem.Backend.Entities;
using Clanwars.PaymentSystem.Lib;
using Sieve.Models;

namespace Clanwars.PaymentSystem.Backend.Repositories;

public interface IProductRepository
{
    IQueryable<Product> GetProducts();
    Task<PagedResult<Product>> GetProductsAsync(SieveModel sieveModel);
    Task<Product> GetProductByIdAsync(Guid productId);
    Task<Product> GetProductByNameAsync(string productName);
    Task CreateProductAsync(Product newProduct);

    Task UpdateProductAsync(Product updateProduct);
    
}
