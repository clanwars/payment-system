using System;
using System.Linq;
using System.Threading.Tasks;
using Clanwars.PaymentSystem.Backend.Entities;
using Clanwars.PaymentSystem.Lib;
using Sieve.Models;

namespace Clanwars.PaymentSystem.Backend.Repositories;

public interface IProductTypeRepository
{
    IQueryable<ProductType> GetProductTypes();
    Task<PagedResult<ProductType>> GetProductTypesAsync(SieveModel sieveModel);
    Task<ProductType> GetProductTypeByIdAsync(Guid productTypeId);
    Task<ProductType> GetProductTypeByNameAsync(string productTypeName);
    Task CreateProductTypeAsync(ProductType createProductType);
    Task UpdateProductTypeAsync(ProductType updateProductType);
}
