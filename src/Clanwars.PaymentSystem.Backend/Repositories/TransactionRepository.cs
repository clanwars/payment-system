using System;
using System.Linq;
using System.Threading.Tasks;
using Clanwars.PaymentSystem.Backend.Context;
using Clanwars.PaymentSystem.Backend.Entities;
using Clanwars.PaymentSystem.Backend.Helpers.Sieve;
using Clanwars.PaymentSystem.Lib;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Sieve.Models;
using Sieve.Services;

namespace Clanwars.PaymentSystem.Backend.Repositories;

public class TransactionRepository : ITransactionRepository
{
    private readonly PaymentContext _context;
    private readonly ISieveProcessor _sieveProcessor;
    private readonly SieveOptions _sieveOptions;

    public TransactionRepository(
        PaymentContext context,
        ISieveProcessor sieveProcessor,
        IOptions<SieveOptions> sieveOptions
    )
    {
        _context = context;
        _sieveProcessor = sieveProcessor;
        _sieveOptions = sieveOptions.Value;
    }

    public IQueryable<Transaction> GetTransactions()
    {
        var result = _context
            .Transactions
            .Include(t => t.PointOfSale)
            .Include(t => t.Account)
            .Include(t => (t as PurchaseTransaction).Entries)
            .ThenInclude(e => e.Product)
            .ThenInclude(p => p.ProductType);
        return result;
    }

    public async Task<PagedResult<Transaction>> GetTransactionsAsync(SieveModel sieveModel)
    {
        var result = GetTransactions()
            .AsNoTrackingWithIdentityResolution();
        return await _sieveProcessor.GetPagedAsync(result, _sieveOptions, sieveModel);
    }

    public async Task<Transaction> GetTransactionByIdAsync(Guid transactionId)
    {
        var result = GetTransactions()
            .Where(transaction => transaction.Id == transactionId);

        return await result.SingleOrDefaultAsync();
    }

    public async Task<bool> WriteNewTransactionAsync(Transaction transaction)
    {
        var dbTransaction = await _context.Database.BeginTransactionAsync();
        try
        {
            _context.Add(transaction);
            await _context.SaveChangesAsync();

            // Update Account Balance
            await RecalculateAccountBalance(transaction.AccountId);

            await dbTransaction.CommitAsync();
        }
        catch (DbUpdateException e)
        {
            Console.WriteLine(e);
            await dbTransaction.RollbackAsync();
            return false;
        }

        return true;
    }

    public async Task<bool> CancelTransactionAsync(Transaction transaction)
    {
        transaction.Cancelled = DateTime.Now;
        var result = await _context.SaveChangesAsync();

        await RecalculateAccountBalance(transaction.AccountId);

        return result > 0;
    }

    private async Task RecalculateAccountBalance(Guid accountId)
    {
        var accountInfo = _context.Accounts.SingleOrDefault(account => account.Id == accountId);
        if (accountInfo == null) return;
        var newAccountBalance = _context.Transactions.Where(t => t.AccountId == accountInfo.Id && t.Cancelled == null).AsEnumerable().Sum(
            t =>
            {
                if (t is PurchaseTransaction)
                {
                    return t.Sum * -1M;
                }

                return t.Sum;
            });
        accountInfo.Balance = newAccountBalance;
        await _context.SaveChangesAsync();
    }
}
