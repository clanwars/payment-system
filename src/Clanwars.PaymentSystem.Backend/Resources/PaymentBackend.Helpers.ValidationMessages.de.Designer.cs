﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Clanwars.PaymentSystem.Backend.Resources {
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class PaymentBackend_Helpers_ValidationMessages_de {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal PaymentBackend_Helpers_ValidationMessages_de() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("PaymentBackend.Resources.PaymentBackend.Helpers.ValidationMessages.de", typeof(PaymentBackend_Helpers_ValidationMessages_de).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Das Feld {0} darf nicht leer sein..
        /// </summary>
        internal static string The__0__field_is_required_ {
            get {
                return ResourceManager.GetString("The {0} field is required.", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to .
        /// </summary>
        internal static string The_field__0__must_be_a_string_or_array_type_with_a_maximum_length_of___1___ {
            get {
                return ResourceManager.GetString("The field {0} must be a string or array type with a maximum length of \'{1}\'.", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to .
        /// </summary>
        internal static string The_field__0__must_be_a_string_or_array_type_with_a_minimum_length_of___1___ {
            get {
                return ResourceManager.GetString("The field {0} must be a string or array type with a minimum length of \'{1}\'.", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to .
        /// </summary>
        internal static string The_field__0__must_be_a_string_with_a_minimum_length_of__2__and_a_maximum_length_of__1__ {
            get {
                return ResourceManager.GetString("The field {0} must be a string with a minimum length of {2} and a maximum length " +
                        "of {1}.", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Der Wert von {0} muss zwischen {1} und {2} liegen..
        /// </summary>
        internal static string The_field__0__must_be_between__1__and__2__ {
            get {
                return ResourceManager.GetString("The field {0} must be between {1} and {2}.", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Das Passwort darf nicht leer sein..
        /// </summary>
        internal static string The_Password_field_is_required_ {
            get {
                return ResourceManager.GetString("The Password field is required.", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Der Benutzername darf nicht leer sein..
        /// </summary>
        internal static string The_Username_field_is_required_ {
            get {
                return ResourceManager.GetString("The Username field is required.", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Der Request darf nicht leer sein..
        /// </summary>
        internal static string There_must_be_a_request_body_ {
            get {
                return ResourceManager.GetString("There must be a request body.", resourceCulture);
            }
        }
    }
}
