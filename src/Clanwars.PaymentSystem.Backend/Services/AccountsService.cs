using System;
using System.Threading.Tasks;
using Clanwars.PaymentSystem.Backend.Entities;
using Clanwars.PaymentSystem.Backend.Repositories;
using Clanwars.PaymentSystem.Lib;
using Sieve.Models;

namespace Clanwars.PaymentSystem.Backend.Services;

class AccountsService : IAccountsService
{
    private readonly IAccountRepository _accountRepository;

    public AccountsService(
        IAccountRepository accountRepository
    )
    {
        _accountRepository = accountRepository;
    }

    public Task<PagedResult<Account>> GetAsync(SieveModel sieveModel)
    {
        return _accountRepository.GetAccountsAsync(sieveModel);
    }

    public Task<Account> GetByIdAsync(Guid accountId)
    {
        return _accountRepository.GetAccountInformationByIdAsync(accountId);
    }

    public Task<Account> GetByEmailAsync(string email)
    {
        return _accountRepository.GetAccountByEmailAsync(email);
    }

    public Task CreateAsync(Account account)
    {
        return _accountRepository.CreateAccountAsync(account);
    }

    public Task UpdateAsync(Account updateAccount)
    {
        return _accountRepository.UpdateAccountAsync(updateAccount);
    }

    public Task DeleteAsync(Account account)
    {
        return _accountRepository.DeleteAccountAsync(account);
    }
}
