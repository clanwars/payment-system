using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Clanwars.PaymentSystem.Backend.Exceptions;
using Clanwars.PaymentSystem.Backend.Hubs;
using Clanwars.PaymentSystem.Lib.Enums;
using Clanwars.PaymentSystem.Lib.Messages.Account;
using Clanwars.PaymentSystem.Lib.Messages.PointOfSale;
using Microsoft.AspNetCore.SignalR;

namespace Clanwars.PaymentSystem.Backend.Services;

public class ConnectionService : IConnectionService
{
    private readonly ConcurrentDictionary<string, CardInfo> _nfcGetCardCalls = new();

    private readonly IHubContext<PaymentHub, IPaymentHub> _paymentHubContext;
    private readonly Dictionary<Guid, PosConnections> _posConnections = new();

    public ConnectionService(IHubContext<PaymentHub, IPaymentHub> paymentHubContext)
    {
        _paymentHubContext = paymentHubContext;
    }

    public string? GetConnection(Guid posId, PosClientType clientType)
    {
        var connections = _posConnections
            .SingleOrDefault(c => c.Key == posId)
            .Value;
        if (connections == null)
        {
            return null;
        }

        return clientType switch
        {
            PosClientType.CashDesk => connections.Cashdesk,
            PosClientType.NfcReader => connections.NfcClient,
            PosClientType.CustomerPanel => connections.CustomerPanel,
            _ => null
        };
    }

    public bool SetConnection(Guid posId, PosClientType clientType, string connectionId)
    {
        if (ConnectionIdIsAlreadyRegistered(connectionId))
        {
            return false;
        }

        if (!_posConnections.ContainsKey(posId))
        {
            _posConnections.Add(posId, new PosConnections());
        }

        switch (clientType)
        {
            case PosClientType.CashDesk:
                if (_posConnections[posId].Cashdesk != null)
                {
                    throw new CashDeskClientAlreadyRegisteredForPointOfSaleException();
                }

                _posConnections[posId].Cashdesk = connectionId;
                break;
            case PosClientType.NfcReader:
                if (_posConnections[posId].NfcClient != null)
                {
                    throw new NfcClientAlreadyRegisteredForPointOfSaleException();
                }

                _posConnections[posId].NfcClient = connectionId;
                break;
            case PosClientType.CustomerPanel:
                if (_posConnections[posId].CustomerPanel != null)
                {
                    throw new CustomerPanelClientAlreadyRegisteredForPointOfSaleException();
                }

                _posConnections[posId].CustomerPanel = connectionId;
                break;
            default:
                return false;
        }

        return true;
    }

    public (Guid, PosClientType)? RemoveConnection(string connectionId)
    {
        var posConnections = _posConnections.Where(c => c.Value.Cashdesk == connectionId || c.Value.NfcClient == connectionId || c.Value.CustomerPanel == connectionId).ToList();
        if (!posConnections.Any())
        {
            return null;
        }

        PosClientType? clientType = null;
        if (posConnections.First().Value.Cashdesk == connectionId)
        {
            clientType = PosClientType.CashDesk;
            posConnections.First().Value.Cashdesk = null;
        }

        if (posConnections.First().Value.NfcClient == connectionId)
        {
            clientType = PosClientType.NfcReader;
            posConnections.First().Value.NfcClient = null;
        }

        if (posConnections.First().Value.CustomerPanel == connectionId)
        {
            clientType = PosClientType.CustomerPanel;
            posConnections.First().Value.CustomerPanel = null;
        }

        return (posConnections.First().Key, (PosClientType) clientType);
    }

    public Guid? GetPointOfSaleId(string connectionId)
    {
        var posConnections = _posConnections.Where(c => c.Value.Cashdesk == connectionId || c.Value.NfcClient == connectionId || c.Value.CustomerPanel == connectionId).ToList();
        if (!posConnections.Any())
        {
            return null;
        }

        return posConnections.First().Key;
    }

    public async Task<CardInfo> GetCardAsync(Guid posId)
    {
        if (!_posConnections.TryGetValue(posId, out var posConnections))
        {
            return null;
        }

        var connectionId = posConnections.NfcClient;

        if (connectionId == null)
        {
            // no nfc connection for this PoS this should be validated before here!
            return null;
        }

        if (!_nfcGetCardCalls.TryAdd(connectionId, null))
        {
            // already pending request!
            throw new NotImplementedException();
        }

        await _paymentHubContext.Clients.Client(connectionId).GetCard();

        // task erzeugen und call wegspeichern
        var ctSource = new CancellationTokenSource();
        var t = Task.Run(() =>
        {
            while (true)
            {
                if (!_nfcGetCardCalls.TryGetValue(connectionId, out var cardInfo))
                {
                    return null;
                }

                if (cardInfo != null)
                {
                    return cardInfo;
                }

                Thread.Sleep(100);
            }
        }, ctSource.Token);

        CardInfo card = null;
        try
        {
            card = await TimeoutAfter(t, new TimeSpan(0, 0, 15));
        }
        catch (TimeoutException)
        {
            ctSource.Cancel();
        }

        _nfcGetCardCalls.TryRemove(connectionId, out _);

        return card;
    }

    public void PublishResponseFromNfc(string connectionId, CardInfo cardInfo)
    {
        if (!_nfcGetCardCalls.ContainsKey(connectionId))
        {
            //result was not expected!?
            return;
        }

        if (cardInfo == null)
        {
            _nfcGetCardCalls.TryRemove(connectionId, out _);
            return;
        }

        _nfcGetCardCalls[connectionId] = cardInfo;
    }

    public bool PosIsReady(Guid posId)
    {
        return _posConnections.ContainsKey(posId) && _posConnections[posId].RequiredConnectionsReady();
    }

    private bool ConnectionIdIsAlreadyRegistered(string connectionId)
    {
        return _posConnections.Count(e => e.Value.Cashdesk == connectionId || e.Value.NfcClient == connectionId || e.Value.CustomerPanel == connectionId) > 0;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="task"></param>
    /// <param name="timeout"></param>
    /// <typeparam name="TResult"></typeparam>
    /// <returns></returns>
    /// <exception cref="TimeoutException"></exception>
    private static async Task<TResult> TimeoutAfter<TResult>(Task<TResult> task, TimeSpan timeout)
    {
        using (var timeoutCancellationTokenSource = new CancellationTokenSource())
        {
            var completedTask = await Task.WhenAny(task, Task.Delay(timeout, timeoutCancellationTokenSource.Token));
            if (completedTask != task) throw new TimeoutException("The operation has timed out.");
            timeoutCancellationTokenSource.Cancel();
            return await task; // Very important in order to propagate exceptions
        }
    }
}
