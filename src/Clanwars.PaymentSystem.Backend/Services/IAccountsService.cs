using System;
using System.Threading.Tasks;
using Clanwars.PaymentSystem.Backend.Entities;
using Clanwars.PaymentSystem.Lib;
using Sieve.Models;

namespace Clanwars.PaymentSystem.Backend.Services;

public interface IAccountsService
{
    Task<PagedResult<Account>> GetAsync(SieveModel sieveModel);
    Task<Account> GetByIdAsync(Guid accountId);
    Task<Account> GetByEmailAsync(string email);
    Task CreateAsync(Account account);
    Task UpdateAsync(Account updateAccount);
    Task DeleteAsync(Account account);
}
