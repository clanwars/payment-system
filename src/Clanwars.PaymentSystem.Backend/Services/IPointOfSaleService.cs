using System;
using System.Linq;
using System.Threading.Tasks;
using Clanwars.PaymentSystem.Backend.Entities;
using Clanwars.PaymentSystem.Lib;
using Clanwars.PaymentSystem.Lib.Messages.PointOfSale;
using Sieve.Models;

namespace Clanwars.PaymentSystem.Backend.Services;

public interface IPointOfSaleService
{
    IQueryable<PointOfSale> GetPointsOfSale();
    Task<PagedResult<PointOfSaleInfo>> GetPointsOfSaleAsync(SieveModel sieveModel);
    Task<PointOfSaleInfo> GetPointOfSaleByIdAsync(Guid posId);
    Task<PointOfSaleInfo> GetPointOfSaleByNameAsync(string posName);
    Task<PointOfSaleInfo> CreatePointOfSaleAsync(PointOfSale newPointOfSale);
    Task<PointOfSaleInfo> UpdatePointOfSaleAsync(PointOfSale updatePointOfSale);
}
