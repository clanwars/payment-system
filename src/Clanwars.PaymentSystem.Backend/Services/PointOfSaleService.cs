using System;
using System.Linq;
using System.Threading.Tasks;
using Clanwars.PaymentSystem.Backend.Entities;
using Clanwars.PaymentSystem.Backend.Repositories;
using Clanwars.PaymentSystem.Lib;
using Clanwars.PaymentSystem.Lib.Enums;
using Clanwars.PaymentSystem.Lib.Messages.PointOfSale;
using Sieve.Models;

namespace Clanwars.PaymentSystem.Backend.Services;

public class PointOfSaleService : IPointOfSaleService
{
    private readonly IConnectionService _connectionService;
    private readonly IPointOfSaleRepository _pointOfSaleRepository;

    public PointOfSaleService(
        IConnectionService connectionService,
        IPointOfSaleRepository pointOfSaleRepository
    )
    {
        _connectionService = connectionService;
        _pointOfSaleRepository = pointOfSaleRepository;
    }

    public IQueryable<PointOfSale> GetPointsOfSale()
    {
        return _pointOfSaleRepository.GetPointsOfSale();
    }

    public async Task<PagedResult<PointOfSaleInfo>> GetPointsOfSaleAsync(SieveModel sieveModel)
    {
        var poses = await _pointOfSaleRepository.GetPointsOfSaleAsync(sieveModel);

        var result = poses.Results.Select(TranslatePos).ToList();

        return new PagedResult<PointOfSaleInfo>
        {
            CurrentPage = poses.CurrentPage,
            PageCount = poses.PageCount,
            PageSize = poses.PageSize,
            Results = result,
            TotalCount = poses.TotalCount
        };
    }


    public async Task<PointOfSaleInfo> GetPointOfSaleByIdAsync(Guid posId)
    {
        var pos = await _pointOfSaleRepository.GetPointOfSaleByIdAsync(posId);
        return TranslatePos(pos);
    }

    public async Task<PointOfSaleInfo> GetPointOfSaleByNameAsync(string posName)
    {
        var pos = await _pointOfSaleRepository.GetPointOfSaleByNameAsync(posName);
        return TranslatePos(pos);
    }

    public async Task<PointOfSaleInfo> CreatePointOfSaleAsync(PointOfSale newPointOfSale)
    {
        await _pointOfSaleRepository.CreatePointOfSaleAsync(newPointOfSale);
        return TranslatePos(newPointOfSale);
    }

    public async Task<PointOfSaleInfo> UpdatePointOfSaleAsync(PointOfSale updatePointOfSale)
    {
        await _pointOfSaleRepository.UpdatePointOfSaleAsync(updatePointOfSale);
        return TranslatePos(updatePointOfSale);
    }

    private PointOfSaleInfo TranslatePos(PointOfSale pos)
    {
        return new PointOfSaleInfo
        {
            Id = pos.Id,
            Name = pos.Name,
            HasNfcClient = CheckPosConnection(pos.Id, PosClientType.NfcReader),
            HasCashDeskClient = CheckPosConnection(pos.Id, PosClientType.CashDesk)
        };
    }

    private bool CheckPosConnection(Guid posId, PosClientType desiredType)
    {
        return _connectionService.GetConnection(posId, desiredType) is not null;
    }
}
