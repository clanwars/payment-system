﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text.Json.Serialization;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Clanwars.PaymentSystem.Backend.Context;
using Clanwars.PaymentSystem.Backend.Filters;
using Clanwars.PaymentSystem.Backend.Helpers;
using Clanwars.PaymentSystem.Backend.Helpers.OpenApi;
using Clanwars.PaymentSystem.Backend.Helpers.Sieve;
using Clanwars.PaymentSystem.Backend.Hubs;
using Clanwars.PaymentSystem.Backend.ModelBinder;
using Clanwars.PaymentSystem.Backend.Options;
using Clanwars.PaymentSystem.Backend.Repositories;
using Clanwars.PaymentSystem.Backend.Services;
using Clanwars.PaymentSystem.Lib.Extensions;
using FluentValidation;
using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc.ModelBinding.Binders;
using Microsoft.AspNetCore.StaticFiles;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Primitives;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using Sieve.Models;
using Sieve.Services;
using ApplicationBuilderExtensions = Microsoft.AspNetCore.Builder.ApplicationBuilderExtensions;

namespace Clanwars.PaymentSystem.Backend;

public class Startup
{
    public Startup(IConfiguration configuration)
    {
        Configuration = configuration;
    }

    public IConfiguration Configuration { get; }

    // This method gets called by the runtime. Use this method to add services to the container.

    // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
    public void ConfigureServices(IServiceCollection services)
    {
        // configure strongly typed settings objects
        var authSettingsSection = Configuration.GetSection(AuthOptions.OptionsKey);
        services.Configure<AuthOptions>(authSettingsSection);

        // configure jwt authentication
        var authSettings = authSettingsSection.Get<AuthOptions>();

        services.AddScoped<ISieveProcessor, ApplicationSieveProcessor>();
        services.Configure<SieveOptions>(Configuration.GetSection("Sieve"));
        services.AddScoped<ISieveCustomSortMethods, SieveCustomSortMethods>();
        services.AddScoped<ISieveCustomFilterMethods, SieveCustomFilterMethods>();
        
        // Add framework services.
        services.AddDbContext<PaymentContext>(options =>
        {
            var dbConfig = Configuration.GetSection(DatabaseOptions.OptionsKey).Get<DatabaseOptions>();
            options.UseMySql(
                dbConfig.GetConnectionString(),
                new MariaDbServerVersion(new Version(10, 9, 2)),
                sqlOptions => { sqlOptions.UseMicrosoftJson(); });
        });

        services.AddLocalization(o => { o.ResourcesPath = "Resources"; });

        services.AddControllers(options =>
            {
                var bodyProvider = options.ModelBinderProviders.Single(provider => provider.GetType() == typeof(BodyModelBinderProvider)) as BodyModelBinderProvider;
                options.ModelBinderProviders.Insert(0, new MapToModelModelBinderProvider(bodyProvider!));

                options.Filters.Add(typeof(PagedResultResponseFilter));
            })
            .AddJsonOptions(options => JsonSerializerExtensions.JsonSerializerOptions(options.JsonSerializerOptions))
            .ConfigureApiBehaviorOptions(options =>
            {
                options.SuppressInferBindingSourcesForParameters = false; // not disable binding source inference
                options.SuppressMapClientErrors = false; // automatically create ProblemDetails
            })
            .AddDataAnnotationsLocalization(o =>
            {
                o.DataAnnotationLocalizerProvider =
                    (type, factory) => factory.Create(typeof(ValidationMessages));
            });

        services
            .AddFluentValidationAutoValidation()
            .AddValidatorsFromAssembly(Assembly.GetExecutingAssembly(), includeInternalTypes: true);

        services.AddSwaggerGen(c =>
        {
            c.SwaggerDoc("v1", new OpenApiInfo {Title = "Payment System API", Version = "v1"});

            // Set the comments path for the Swagger JSON and UI.
            var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
            var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
            c.IncludeXmlComments(xmlPath);
            c.AddSecurityDefinition("OAuth2", new OpenApiSecurityScheme
            {
                Type = SecuritySchemeType.OpenIdConnect,
                OpenIdConnectUrl = new Uri(authSettings.RealmDomain+ "/.well-known/openid-configuration"),
                Scheme = "bearer",
                In = ParameterLocation.Header,
                Description = "Clanwars Auth"
            });
            c.UseCommonFilters();
        });

        // Mapster configuration
        // var mapsterConfig = TypeAdapterConfig.GlobalSettings;
        // mapsterConfig.RequireExplicitMapping = true;
        // mapsterConfig.RequireDestinationMemberSource = true;
        // mapsterConfig.Scan(Assembly.GetExecutingAssembly());
        // mapsterConfig.Compile();

        //TODO uncomment when automapper is gone
        // services.AddSingleton(mapsterConfig);
        // services.AddScoped<IMapper, ServiceMapper>();

        services.AddAutoMapper(typeof(Startup).Assembly);

        services.AddCors();

        services.AddHealthChecks()
            .AddCheck("self", () => HealthCheckResult.Healthy("backend service is running"))
            .AddDbContextCheck<PaymentContext>("database-connection", HealthStatus.Unhealthy);

        services.AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(o =>
                {
                    o.Authority = authSettings.RealmDomain;
                    o.RequireHttpsMetadata = false;
                    o.SaveToken = true;
                    o.ClaimsIssuer = authSettings.IssuerDomain;
                    o.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuerSigningKey = true,
                        ValidateIssuer = true,
                        ValidateLifetime = true,
                        ValidateAudience = false,
                    };
                    // We have to hook the OnMessageReceived event in order to
                    // allow the JWT authentication handler to read the access
                    // token from the query string when a WebSocket or 
                    // Server-Sent Events request comes in.
                    o.Events = new JwtBearerEvents
                    {
                        OnMessageReceived = context =>
                        {
                            var accessToken = context.Request.Query["access_token"];

                            // If the request is for our hub...
                            var path = context.HttpContext.Request.Path;
                            if (!string.IsNullOrEmpty(accessToken) &&
                                (path.StartsWithSegments("/payment-hub")))
                            {
                                // Read the token out of the query string
                                context.Token = accessToken;
                            }

                            return Task.CompletedTask;
                        }
                    };
                }
            );
        services.AddAuthorization();

        services.AddSignalR(options => { options.EnableDetailedErrors = true; })
            .AddJsonProtocol(o => o.PayloadSerializerOptions.Converters.Add(new JsonStringEnumConverter()));

        services.AddScoped<IProductRepository, ProductRepository>();
        services.AddScoped<IProductTypeRepository, ProductTypeRepository>();
        services.AddScoped<IAccountRepository, AccountRepository>();
        services.AddScoped<ITransactionRepository, TransactionRepository>();
        services.AddScoped<IPointOfSaleRepository, PointOfSaleRepository>();
        services.AddScoped<IPointOfSaleService, PointOfSaleService>();
        services.AddScoped<IAccountsService, AccountsService>();
        services.AddSingleton<IConnectionService, ConnectionService>();
    }

    public void Configure(IApplicationBuilder app, IWebHostEnvironment env, PaymentContext dbContext)
    {
        var dbConfig = Configuration.GetSection(DatabaseOptions.OptionsKey).Get<DatabaseOptions>();
        if (dbConfig.AutoApplyMigrations)
        {
            using var scope = app.ApplicationServices.CreateScope();
            using var db = scope.ServiceProvider.GetRequiredService<PaymentContext>();
            db!.Database.Migrate();
        }

        IList<CultureInfo> supportedCultures = new List<CultureInfo>
        {
            new("en-US"),
            new("en"),
            new("de-DE"),
            new("de")
        };
        app.UseRequestLocalization(new RequestLocalizationOptions
        {
            DefaultRequestCulture = new RequestCulture("en-US"),
            SupportedCultures = supportedCultures,
            SupportedUICultures = supportedCultures
        });

        if (!env.IsDevelopment())
        {
            app.MapWhen(context => Regex.IsMatch(context.Request.Path.Value!, @"^/(?:admin|cashdesk)/?$", RegexOptions.IgnoreCase), applicationBuilder =>
                {
                    applicationBuilder.Run(context =>
                    {
                        var localeString = CultureInfo.CurrentCulture.IsNeutralCulture
                            ? CultureInfo.CurrentCulture.Name
                            : CultureInfo.CurrentCulture.Parent.Name;
                        var appMatch = Regex.Match(context.Request.Path.Value!, @"^/(admin|cashdesk)");
                        if (appMatch.Success)
                        {
                            context.Response.Headers.Location = new StringValues(appMatch.Captures.First().Value + "/" + localeString + "/");
                            context.Response.StatusCode = StatusCodes.Status302Found;
                        }
                        return Task.CompletedTask;
                    });
                });
        }

        // Enable middleware to serve generated Swagger as a JSON endpoint.
        app.UseSwagger();
        // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.), specifying the Swagger JSON endpoint.
        app.UseSwaggerUI(c =>
        {
            c.SwaggerEndpoint("/swagger/v1/swagger.json", "Payment System API v1");
            c.OAuthClientId("payment-system");
            c.OAuthUsePkce();
        });

        if (env.IsDevelopment())
        {
            app.UseDeveloperExceptionPage();
        }

        app.UseCors();
        app.UseAuthentication();

        app.UseRouting();
        app.UseAuthorization();
        app.UseEndpoints(endpoints =>
        {
            endpoints.MapControllers();
            endpoints.MapHub<PaymentHub>("/payment-hub");
            endpoints.MapHealthChecks("/health");
        });

        // serve static files from wwwroot
        app.UseDefaultFiles();
        var fileExtensionContentTypeProvider = new FileExtensionContentTypeProvider
        {
            Mappings = {[".webmanifest"] = "application/manifest+json"}
        };
        app.UseStaticFiles(new StaticFileOptions
        {
            ContentTypeProvider = fileExtensionContentTypeProvider
        });

        // map /cashdesk and /admin as SPA's from within wwwroot
        if (!env.IsDevelopment())
        {
            var supportedSpas = new List<string>() {"cashdesk/en", "cashdesk/de", "admin/en", "admin/de"};
            supportedSpas.ForEach(path =>
            {
                app.Map("/" + path, spaApp =>
                {
                    spaApp.UseSpa(spa =>
                    {
                        spa.Options.SourcePath = Path.Combine("wwwroot", path);
                        spa.Options.DefaultPageStaticFileOptions = new StaticFileOptions
                        {
                            FileProvider = new PhysicalFileProvider(Path.Combine(Directory.GetCurrentDirectory(),
                                "wwwroot", path)),
                            ContentTypeProvider = fileExtensionContentTypeProvider
                        };
                    });
                });
            });
        }
    }
}
