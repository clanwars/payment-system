# Roles
* User
  + can be `blocked`
* Admin
* NfcClient
* CashDesk

## Roles needed for REST calls and SignalR methods
| Action          | User | CashDesk | NfcClient | Admin |
|-----------------|:----:|:--------:|:----------:|:-----:|
| /Login          |   x  |     x    |      x     |   x   |
| /Accounts       |      |          |            |   x   |
| /Products       |      |     x    |            |   x   |
| /ProductTypes   |      |     x    |            |   x   |
| /Transactions   |      |     x    |            |   x   |
| /PointsOfSale   |      |     x    |            |   x   |
| -----           |      |          |            |       |
| JoinPointOfSale |      |     x    |      x     |       |
| NewCard         |      |          |      x     |       |
| CurrentCard     |      |          |      x     |       |
| RemovedCard     |      |          |      x     |       |
|                 |      |          |            |       |
|                 |      |          |            |       |
