namespace Clanwars.PaymentSystem.Lib.Enums;

public enum PosClientType
{
    CashDesk = 0,
    NfcReader = 1,
    CustomerPanel = 2,
}
