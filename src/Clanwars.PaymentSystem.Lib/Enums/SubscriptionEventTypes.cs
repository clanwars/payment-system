namespace Clanwars.PaymentSystem.Lib.Enums;

public enum SubscriptionEventTypes
{
    PosAdded,
    PosConfigured,
    PosDeviceLeft,
    PosDeviceJoined
}
