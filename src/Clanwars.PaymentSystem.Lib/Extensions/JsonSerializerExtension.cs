using System.Text.Json;
using System.Text.Json.Serialization;
using Clanwars.PaymentSystem.Lib.Converter.Json;

namespace Clanwars.PaymentSystem.Lib.Extensions;

public static partial class JsonSerializerExtensions
{
    private static JsonSerializerOptions? _serializerOptions;

    private static bool InitializeOptions()
    {
        if (_serializerOptions != null) return true;

        _serializerOptions = new JsonSerializerOptions(JsonSerializerDefaults.Web);
        return false;
    }

    public static JsonSerializerOptions JsonSerializerOptions()
    {
        if (InitializeOptions())
        {
            return _serializerOptions!;
        }
        return JsonSerializerOptions(_serializerOptions!);
    }

    public static JsonSerializerOptions JsonSerializerOptions(JsonSerializerOptions jsonSerializerOptions)
    {
        // standard JsonSerializerDefaults.Web settings
        jsonSerializerOptions.PropertyNameCaseInsensitive = true;
        jsonSerializerOptions.PropertyNamingPolicy = JsonNamingPolicy.CamelCase;
        jsonSerializerOptions.NumberHandling = JsonNumberHandling.AllowReadingFromString;
        jsonSerializerOptions.DefaultIgnoreCondition = JsonIgnoreCondition.Never;

        // additional settings
        jsonSerializerOptions.Converters.Add(new DateTimeToAtomStringConverter());
        jsonSerializerOptions.Converters.Add(new JsonStringEnumConverter());

        return jsonSerializerOptions;
    }

    public static T? DeserializeJsonString<T>(this string json)
    {
        InitializeOptions();
        return JsonSerializer.Deserialize<T>(json, _serializerOptions);
    }

    public static string SerializeToJsonString(this object data)
    {
        InitializeOptions();
        return JsonSerializer.Serialize(data, _serializerOptions);
    }
}

