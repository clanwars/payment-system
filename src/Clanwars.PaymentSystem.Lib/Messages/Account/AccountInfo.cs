using System;

namespace Clanwars.PaymentSystem.Lib.Messages.Account;

public record AccountInfo
{
    public Guid Id { get; set; }
    public string Email { get; set; }
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string CardId { get; set; }
    public decimal Balance { get; set; }
    public bool Locked { get; set; }
    public int Overdraft { get; set; }
    public float Discount { get; set; }

    public override string ToString()
    {
        return
            $"{nameof(Id)}: {Id}, {nameof(Email)}: {Email}, {nameof(FirstName)}: {FirstName}, {nameof(LastName)}: {LastName}, {nameof(CardId)}: {CardId}, {nameof(Balance)}: {Balance}, {nameof(Locked)}: {Locked}";
    }
}
