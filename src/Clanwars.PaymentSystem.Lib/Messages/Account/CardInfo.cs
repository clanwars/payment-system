namespace Clanwars.PaymentSystem.Lib.Messages.Account;

public record CardInfo
{
    public string Id { get; set; }
    public AccountInfo Account { get; set; }

    public override string ToString()
    {
        return $"{nameof(Id)}: {Id}, {nameof(Account)}: {Account}";
    }
}
