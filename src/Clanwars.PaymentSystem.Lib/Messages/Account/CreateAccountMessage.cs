using System.ComponentModel.DataAnnotations;

namespace Clanwars.PaymentSystem.Lib.Messages.Account;

public record CreateAccountMessage
{
    [Required(ErrorMessage = "The {0} field is required.")]
    [StringLength(128, MinimumLength = 1)]
    public string Email { get; set; }

    [Required(ErrorMessage = "The {0} field is required.")]
    [StringLength(128, MinimumLength = 1)]
    public string FirstName { get; set; }

    [Required(ErrorMessage = "The {0} field is required.")]
    [StringLength(128, MinimumLength = 1)]
    public string LastName { get; set; }

    [StringLength(255, MinimumLength = 1)]
    public string CardId { get; set; }

    [Range(0, int.MaxValue, ErrorMessage = "The field {0} must be between {1} and {2}.")]
    public int Overdraft { get; set; }

    [Range(0, 1, ErrorMessage = "The field {0} must be between {1} and {2}.")]
    public float Discount { get; set; }
}
