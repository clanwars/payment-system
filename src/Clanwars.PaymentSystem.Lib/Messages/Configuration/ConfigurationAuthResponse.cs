namespace Clanwars.PaymentSystem.Lib.Messages.Configuration;

public record ConfigurationAuthResponse
{
    public string IdpUrl { get; set; }
    public string ClientId { get; set; }
}
