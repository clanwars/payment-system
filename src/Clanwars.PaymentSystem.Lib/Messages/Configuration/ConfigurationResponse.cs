namespace Clanwars.PaymentSystem.Lib.Messages.Configuration;

public record ConfigurationResponse
{
    public ConfigurationAuthResponse Auth { get; set; }
}
