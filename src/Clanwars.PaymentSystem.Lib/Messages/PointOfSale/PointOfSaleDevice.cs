using System;
using System.ComponentModel.DataAnnotations;
using Clanwars.PaymentSystem.Lib.Enums;

namespace Clanwars.PaymentSystem.Lib.Messages.PointOfSale;

public record PointOfSaleDevice
{
    [Required(ErrorMessage = "The {0} field is required.")]
    public Guid PosId { get; set; }

    [Required(ErrorMessage = "The {0} field is required.")]
    public PosClientType Type { get; set; }

    public override string ToString()
    {
        return $"PosDevice({nameof(Type)}: {Type.ToString()}) {nameof(PosId)}: {PosId}";
    }
}
