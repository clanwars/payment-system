using System;
using Clanwars.PaymentSystem.Lib.Messages.Account;
using Sieve.Attributes;

namespace Clanwars.PaymentSystem.Lib.Messages.PointOfSale;

public record PointOfSaleInfo
{
    [Sieve(CanFilter = true, CanSort = true)]
    public Guid Id { get; set; }

    [Sieve(CanFilter = true, CanSort = true)]
    public string Name { get; set; }

    public bool HasNfcClient { get; set; }

    public bool HasCashDeskClient { get; set; }

    public bool IsReady => HasNfcClient && HasCashDeskClient;
}
