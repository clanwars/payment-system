using System;
using System.ComponentModel.DataAnnotations;

namespace Clanwars.PaymentSystem.Lib.Messages.Product;

public record CreateProductMessage
{
    [Required(ErrorMessage = "The {0} field is required.")]
    // [IsUniqueProduct]
    [MinLength(3, ErrorMessage = "The field {0} must be a string or array type with a minimum length of '{1}'.")]
    public string Name { get; set; }

    [Required(ErrorMessage = "The {0} field is required.")]
    [Range(0, int.MaxValue, ErrorMessage = "The field {0} must be between {1} and {2}.")]
    public decimal Price { get; set; }

    public string Description { get; set; }

    public string Image { get; set; }

    [Required(ErrorMessage = "The {0} field is required.")]
    public Guid ProductTypeId { get; set; }

    public int Order { get; set; }

    public bool Locked { get; set; }
}
