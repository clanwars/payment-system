using System.ComponentModel.DataAnnotations;

namespace Clanwars.PaymentSystem.Lib.Messages.ProductType;

public record CreateProductTypeMessage
{
    [Required(ErrorMessage = "The {0} field is required.")]
    public string Name { get; set; }

    public int Order { get; set; }

    public string Image { get; set; }

    public bool Locked { get; set; }
}
