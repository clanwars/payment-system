using System.ComponentModel.DataAnnotations;

namespace Clanwars.PaymentSystem.Lib.Messages.Transaction;

public record CreateChargeTransactionMessage : CreateTransactionBaseMessage
{
    [Required(ErrorMessage = "The {0} field is required.")]
    [Range(-100, 100, ErrorMessage = "The field {0} must be between {1} and {2}.")]
    public decimal Amount { get; set; }

    public override string ToString()
    {
        return $"{nameof(Amount)}: {Amount}, {nameof(AccountId)}: {AccountId}";
    }
}
