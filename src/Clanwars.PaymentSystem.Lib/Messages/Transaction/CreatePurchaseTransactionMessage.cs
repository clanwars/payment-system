using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Clanwars.PaymentSystem.Lib.Messages.Transaction;

public record CreatePurchaseTransactionMessage : CreateTransactionBaseMessage
{
    [Required(ErrorMessage = "The {0} field is required.")]
    [MinLength(1, ErrorMessage = "The field {0} must be a string or array type with a minimum length of '{1}'.")]
    public List<PurchaseEntry> Entries { get; set; }

    public override string ToString()
    {
        return $"{nameof(Entries)}: {Entries}, {nameof(AccountId)}: {AccountId}";
    }
}
