using System;
using System.ComponentModel.DataAnnotations;

namespace Clanwars.PaymentSystem.Lib.Messages.Transaction;

public record CreateTransactionBaseMessage
{
    [Required(ErrorMessage = "The {0} field is required.")]
    public Guid AccountId { get; set; }

    [Required(ErrorMessage = "The {0} field is required.")]
    public Guid PointOfSaleId { get; set; }

    public string Comment { get; set; }
}
