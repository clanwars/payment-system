using System;
using System.ComponentModel.DataAnnotations;

namespace Clanwars.PaymentSystem.Lib.Messages.Transaction;

public record PurchaseEntry
{
    [Required(ErrorMessage = "The {0} field is required.")]
    // [IsValidProductId]
    public Guid ProductId { get; set; }

    [Required(ErrorMessage = "The {0} field is required.")]
    [Range(0, 100, ErrorMessage = "The field {0} must be between {1} and {2}.")]
    public decimal Price { get; set; }

    public override string ToString()
    {
        return $"{nameof(ProductId)}: {ProductId}, {nameof(Price)}: {Price}";
    }
}
