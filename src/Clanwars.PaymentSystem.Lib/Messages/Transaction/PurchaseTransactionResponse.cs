using System.Collections.Generic;

namespace Clanwars.PaymentSystem.Lib.Messages.Transaction;

public record PurchaseTransactionResponse
{
    public List<PurchaseTransactionEntryResponse> Entries { get; set; }

}
