using System;
using System.Collections.Generic;
using Clanwars.PaymentSystem.Lib.Enums;
using Clanwars.PaymentSystem.Lib.Messages.Account;
using Clanwars.PaymentSystem.Lib.Messages.PointOfSale;

namespace Clanwars.PaymentSystem.Lib.Messages.Transaction;

public record TransactionResponse
{
    public Guid Id { get; set; }

    public AccountInfo Account { get; set; }

    public PointOfSaleResponse PointOfSale { get; set; }

    public decimal Sum { get; set; }

    public DateTime Timestamp { get; set; }

    public string Comment { get; set; }

    public DateTime? Cancelled { get; set; }

    public List<PurchaseTransactionEntryResponse> Entries { get; set; }

    public TransactionType TransactionType => Entries == null
        ? TransactionType.ChargeTransaction
        : TransactionType.PurchaseTransaction;
}
