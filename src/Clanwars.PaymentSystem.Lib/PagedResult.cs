using System.Collections.Generic;

namespace Clanwars.PaymentSystem.Lib;

public class PagedResult<T> : IPagedResult<T> where T : class
{
    public IList<T> Results { get; set; } = null!;

    public int CurrentPage { get; set; }
    public int PageCount { get; set; }
    public int PageSize { get; set; }
    public long TotalCount { get; set; }
}
