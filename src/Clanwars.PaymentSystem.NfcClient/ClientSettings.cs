using System;

namespace Clanwars.PaymentSystem.NfcClient;

public class ClientSettings
{
    public string ServerAddress { get; set; }
    public Guid PosId { get; set; }
    public int CardReaderId { get; set; }
}
