﻿using System.IO;
using System.Reflection;
using System.Threading;
using Clanwars.OidcTokenAuthenticationClient;
using Clanwars.PaymentSystem.NfcClient.Services;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace Clanwars.PaymentSystem.NfcClient;

internal class Program
{
    private static EventWaitHandle _ewh;

    public static void Main()
    {
        _ewh = new EventWaitHandle(false, EventResetMode.AutoReset);
        var serviceCollection = new ServiceCollection();
        ConfigureServices(serviceCollection);

        var serviceProvider = serviceCollection.BuildServiceProvider();

        var logger = serviceProvider.GetService<ILogger<Program>>();
        logger.LogDebug("Payment NFC Client is starting up...");

        var signalr = serviceProvider.GetService<IPaymentSignalrService>();

        var pcsc = serviceProvider.GetService<IPcScService>();
        pcsc.StartListening();

        _ewh.WaitOne();
        logger.LogWarning("jumped over waitOne");
    }

    private static void ConfigureServices(IServiceCollection services)
    {
        var config = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
            .AddUserSecrets(Assembly.GetExecutingAssembly(), true)
            .Build();

        services
            .AddOptions()
            .Configure<ClientSettings>(config)
            .Configure<OidcOptions>(config.GetSection("Oauth2"))
            .AddLogging(builder => builder.AddConsole())
            .Configure<LoggerFilterOptions>(options => options.MinLevel = LogLevel.Debug)
            .AddHttpClient()
            .AddSingleton<IPcScService, PcScService>()
            .AddSingleton<IPaymentSignalrService, PaymentSignalrService>()
            .AddSingleton<IOidcTokenAuthenticationClient, OidcTokenAuthenticationClient.OidcTokenAuthenticationClient>();
    }
}
