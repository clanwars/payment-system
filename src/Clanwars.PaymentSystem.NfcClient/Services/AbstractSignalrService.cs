using System;
using System.Threading.Tasks;
using Clanwars.OidcTokenAuthenticationClient;
using Clanwars.PaymentSystem.NfcClient.Models;
using Microsoft.AspNetCore.SignalR.Client;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace Clanwars.PaymentSystem.NfcClient.Services;

public abstract class AbstractSignalrService
{
    protected HubConnection Connection;
    protected readonly ClientSettings Config;
    protected readonly ILogger<AbstractSignalrService> Logger;
    protected readonly IServiceProvider Services;
    private readonly IOidcTokenAuthenticationClient _oidcTokenAuthenticationClient;

    protected AbstractSignalrService(IOptions<ClientSettings> config, ILogger<AbstractSignalrService> logger,
        IServiceProvider services, IOidcTokenAuthenticationClient oidcTokenAuthenticationClient)
    {
        Config = config.Value;
        Logger = logger;
        Services = services;
        _oidcTokenAuthenticationClient = oidcTokenAuthenticationClient;
    }

    protected void InitializeService()
    {
        Connection = new HubConnectionBuilder()
            .WithUrl(Config.ServerAddress,
                options => { options.AccessTokenProvider = () => _oidcTokenAuthenticationClient.GetCurrentJwtTokenAsync(); })
            .Build();
        Connection.Closed += async (error) =>
        {
            await Task.Delay(new Random().Next(0, 5) * 1000);
            await Connection.StartAsync();
        };
    }

    protected void RegisterHandler<T>(MethodTypes methodType, Action<T> handler)
    {
        Logger.LogDebug("registered a message for " + methodType);
        Connection.On(methodType.ToString(), handler);
    }

    protected void RegisterHandler(MethodTypes methodType, Action handler)
    {
        Logger.LogDebug("registered a message for " + methodType);
        Connection.On(methodType.ToString(), handler);
    }
}
