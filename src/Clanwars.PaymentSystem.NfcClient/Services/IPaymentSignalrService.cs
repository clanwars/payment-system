namespace Clanwars.PaymentSystem.NfcClient.Services;

public interface IPaymentSignalrService
{
    void SendCardInformation(string cardId);
    void SendRemovedCardInformation();
}