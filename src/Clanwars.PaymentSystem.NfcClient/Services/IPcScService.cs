namespace Clanwars.PaymentSystem.NfcClient.Services;

public interface IPcScService
{
    void StartListening();
    string GetCurrentCardId();
}