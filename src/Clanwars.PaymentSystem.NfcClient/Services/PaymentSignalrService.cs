using System;
using Clanwars.OidcTokenAuthenticationClient;
using Clanwars.PaymentSystem.Lib.Enums;
using Clanwars.PaymentSystem.Lib.Messages.Account;
using Clanwars.PaymentSystem.Lib.Messages.Hub;
using Clanwars.PaymentSystem.Lib.Messages.PointOfSale;
using Clanwars.PaymentSystem.NfcClient.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR.Client;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace Clanwars.PaymentSystem.NfcClient.Services;

public class PaymentSignalrService : AbstractSignalrService, IPaymentSignalrService
{
    public PaymentSignalrService(IOptions<ClientSettings> config,
        ILogger<PaymentSignalrService> logger, IServiceProvider services,
        IOidcTokenAuthenticationClient oidcTokenAuthenticationClient
        ) : base(config, logger,
        services, oidcTokenAuthenticationClient)
    {
        Logger.LogDebug("PaymentSignalrService is starting....");
        InitializeService();
        Logger.LogInformation("PaymentSignalrService started");
    }

    private new async void InitializeService()
    {
        base.InitializeService();
        Logger.LogDebug(Config.ServerAddress);
        Logger.LogDebug(Config.PosId.ToString());

        RegisterHandler<ProblemDetails>(MethodTypes.Error, HandleError);
        RegisterHandler<string>(MethodTypes.Welcome, ReceivedWelcome);
        RegisterHandler<string>(MethodTypes.JoinRequired, ReceivedJoinRequired);
        RegisterHandler<PointOfSaleInfo>(MethodTypes.NewCashDesk, ReceivedNewCashDesk);
        RegisterHandler<CardInfo>(MethodTypes.NewCard, ReceivedNewCard);
        RegisterHandler(MethodTypes.GetCard, ReceivedGetCard);

        await Connection.StartAsync();
    }

    private void HandleError(ProblemDetails errorMessage)
    {
        Logger.LogError("Received: Error");
        Logger.LogError($"{errorMessage.Type}: {errorMessage.Detail}");
    }

    private void ReceivedWelcome(string message)
    {
        Logger.LogDebug("Received Welcome message: " + message);
    }

    private void ReceivedJoinRequired(string m)
    {
        Logger.LogDebug("Received JoinRequired message");

        var pointOfSaleDevice = new PointOfSaleDevice
        {
            PosId = Config.PosId,
            Type = PosClientType.NfcReader
        };

        Logger.LogDebug("sending: " + pointOfSaleDevice);
        Connection.SendAsync(MethodTypes.JoinPointOfSale.ToString(), pointOfSaleDevice);
    }

    private void ReceivedNewCashDesk(PointOfSaleInfo pointOfSale)
    {
        Logger.LogDebug(pointOfSale.ToString());
    }

    private void ReceivedNewCard(CardInfo cardInfo)
    {
        Logger.LogDebug("Received CardInfo: " + cardInfo);
    }

    private void ReceivedGetCard()
    {
        Logger.LogDebug("received GetCard");
        var pcScService = Services.GetRequiredService<IPcScService>();
        var currentCardId = pcScService.GetCurrentCardId();
        SendCurrentCardInformation(currentCardId);
    }

    public void SendCardInformation(string cardId)
    {
        var card = new Card
        {
            Id = cardId
        };
        Connection.SendAsync(MethodTypes.NewCard.ToString(), card);
    }

    public void SendRemovedCardInformation()
    {
        Connection.SendAsync(MethodTypes.RemovedCard.ToString());
    }

    private void SendCurrentCardInformation(string cardId)
    {
        var card = new Card
        {
            Id = cardId
        };

        Connection.SendAsync(MethodTypes.CurrentCard.ToString(), card);
        Logger.LogDebug("sent current card information");
    }
}
